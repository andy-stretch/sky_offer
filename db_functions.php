<?php

function last_game_play($party_id,$promo_id)
{
	global $db,$table;
	$rowqr = $db->from($table['competition_game_data'])->where('promo_id', $promo_id)->where('party_id', $party_id)->order_by('last_played', 'desc')->fetch_first();
	return $rowqr;
}


function all_game_play($party_id,$promo_id)
{
	global $db,$table;
	$db->from($table['competition_game_data']);
			$db->where('promo_id', $promo_id);
			$db->where('party_id', $party_id);
			$db->order_by('last_played', 'desc');
	$rowqr = $db->fetch();
	return $rowqr;
}

function record_game_play($party_id,$promo_id,$score)
{
	global $db,$table;
	$last_game_play = "insert into {$table[TABLE_PREFIX]}{$table['competition_game_data']} 
		( `party_id`, `promo_id`, `last_played`, `total_play`,`score`)  values ('$party_id','$promo_id',now(),'1','$score')
		 ON DUPLICATE KEY UPDATE  `last_played`=now(),`total_play`=total_play+1 ";
	$db->query($last_game_play)->execute();

}

function check_last_play($party_id,$promo_id,$min_time)
{
	$rowqr=last_game_play($party_id,$promo_id);
	$return_array = array();
	if(empty($rowqr['last_played']))
		$return_array['result']=true;
	elseif($rowqr['last_played']<$min_time)
		$return_array['result']=true;
	else
	{
		$return_array['result']=false;
		$return_array['last_played']=$rowqr['last_played'];
	}
	return $return_array;
}


