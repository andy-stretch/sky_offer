<?php
if(empty($_SESSION['user_token']))
{
    $user_token             = get_rand_id(10);
    $_SESSION['user_token'] = $user_token; 
}
?>
<script language="javascript">
function show_hide_divs()
{
    $('.upload_text').hide(100);
    $(".hidden").show(50);
	<?php if(!empty($errors['email']))
	echo '$(".error").hide(100);';
	?>
}

	function register_upload()
	{ 	//any select change on the dropdown triggers this code
		var token_value = '<?php echo $_SESSION['user_token'];?>';
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL;?>post_upload.php" , 
			data : {'file_name':'<?php echo $filename ;?>','promoid':'<?php echo $promoid;?>','user_token':token_value},
			dataType:'json',
			success: function(data) //we're calling the response json array 'stime' (sesstion time)
			{
				if(data['result']=='success')
                {
                   show_hide_divs();
                }
                if(data['result']=='redirect')
                {
					// redirect to index page
                    window.location.href = "<?php echo SITE_URL;?>index.php";
                }
			}

		});
	}
</script>
