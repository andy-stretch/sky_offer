<?php 

$email_header = '
<div style="font-family: Arial, Helvetica, sans-serif; font-size:13px; width: 100% !important; height: 100%; background: #fff; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; " >
  <table width="580" align="center">
    <tr>
      <td style="display: block !important; clear: both !important; margin: 0 auto !important; max-width: 580px !important;"><!-- Message start -->
        <table style=" width: 100% !important; border-collapse: collapse;">
          <tr>
            <td style="">
            	<table width="100%" style=" width: 100% !important; border-collapse: collapse;">
                	<tr>
                    	<td><img src="'.SITE_URL.("images/emailheader_".$custband.".jpg?v2").'"  style="display: block;"></td>
                        <td align="right" style="color:#808080; font-family:Arial, Helvetica, sans-serif">
                        	<br /> <br />
                            <a href="#" style="text-decoration:none; font-family:Arial, Helvetica, sans-serif; color:'.$band_color.'; font-size:16px;"></a>
                        </td>
                    </tr>
                </table>
            </td>
          </tr>
          <tr>
          	<td></td>
          </tr>
          <tr>
            <td  style="background: white; padding: 30px 0;"><h2 style="margin-bottom: 20px;  color:'. $band_color.'; line-height: 1.25; font-size:26px; font-weight:normal;" align="center">Your [ETYPE] entry
</h2>
              <div style="font-size: 14px; color: #808080; font-weight: normal; padding:0px 10px 0px 10px; margin-bottom: 20px; line-height:20px;">
              <div>Hello '.$user_name.',<br><br></div>
              ';