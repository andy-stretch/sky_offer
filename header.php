<!DOCTYPE html>
<html lang="en-US">
<head>
<title>
<?php if(!empty($comp['promotion_title']))
   echo $comp['promotion_title'];?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="ROBOTS" CONTENT="NOARCHIVE">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!-- Responsive and mobile friendly stuff -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

<!-- Stylesheets -->
<link rel="stylesheet" href="<?php echo CDN_URL;?><?php echo DIR;?>css/general.css?v=8" media="all" type="text/css">
<link rel="stylesheet" href="<?php echo CDN_URL;?><?php echo DIR;?>css/Band<?php echo $custband;?>.css" media="all" type="text/css">
	<?php if($comp['require_upload']==1) 
		echo '<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>'; ?>
<script>
	document.write('<script src="<?php echo CDN_URL;?>common/js/jquery.js"><\/script>');
	document.write('<script src="<?php echo CDN_URL;?>common/js/html5.js"><\/script>');
</script>

<script type="text/javascript">
skyTags.queue.push(['set', {
    config: {
        adapters: {
            adobeAnalytics: {
                maps: {
                    populators: {
                        /*Development Report Suite*/
                        /*account: function() { return 'mobskyappstretchdev'; }*/
                        account: function() { return 'bskybmobskyappstretchprod'; }
                    }
                }
            }
        }
    }
}]);

$(document).ready(function() {
	var data = {
		page: {
			name: "<?php echo $track_pagename;?>",
			breadcrumb: [ "skyapp", "stretch","comp" ]
		}
	};
	skyTags.queue.push(['set', data]);
});

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93102600-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93102600-3');
</script>

</head>
<body>
<div class="wraper">
