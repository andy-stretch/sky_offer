<?php  
	require('local_config.php');
	require(ROOT.'config/sky_connect.php');
	require(ROOT.'common/db/DB_manager.php');
	$track_pagename = 'compconfirm';
	
	$db = new DB_manager(HOST, DBU, DBPASS, DB);
	$db->set_table_prefix('sky_');
	$db->debug=0;
	$booking_rand=$_SESSION['booking_rand'];
	$promoid = $_SESSION['promoid'];
	$custband = $_SESSION['custband'];
	$user_answer = $_SESSION['user_answer'];
	$band_color = $BAND_COLORS[$custband];
	$checksum2= sha1($custband.$booking_rand.$sha_salt.date('Ymd'));
	$email = $_SESSION['email'];
	$user_name=$fname = $_SESSION['fname'];
	$lname = $_SESSION['lname'];
	
	if($_SESSION['checksum2']!=$checksum2)
	{
		header('Location:index.php');
		exit();
	}

	$comp = $db->from($table['competition'])->where('treat_id',$promoid)->fetch_first(); 
	$filename= $name.'-'.$custband.'-'.round(microtime(true));

?>
<?php require('header.php'); ?>
  <div class="topbaner">
    <div class="leftimage"><img src="<?php echo CDN_URL;?><?php echo DIR;?>content/<?=$comp['image'];?>" alt=""> </div>
    <div class="rightparttext">
      <div class="textsection">
        <p> <img class="star-img" src="<?php echo CDN_URL;?><?php echo DIR;?>images/star<?=$custband;?>.png"> </p>
        <div class="bigtext <?php if($comp['require_upload']==1) echo 'hidden'; ?>">You're in it to win it!</div>
			<?php if($comp['require_upload']==1) { ?>
		  <div class="hidden success"><br>Your file is uploaded successfully <br></div>
		  <div class="bigtext upload_text">  Upload your file</div>
		          <div class="subheadertext">Upload your file to compete your entry.</div>

		  <?php }
		  
		  ?>
         <p class="body <?php if($comp['require_upload']==1) echo 'hidden'; ?>"><?php 
		 
		 $band_entries_text = $BAND_ENTRIES_DISPLAY[$custband];
		 if(!empty($comp['question']) && empty($comp['answer_set']))
			$band_entries_text = $BAND_ENTRIES_DISPLAY_COMP[$custband];
		$comp_type="prize draw";
					 if(!empty($comp['question']))
			$comp_type = "competition";
 
		 echo $confirm_page_text = "Thank you for entering our ".$comp_type.": &quot;".$comp['promotion_title']."&quot;.".$band_entries_text."<br><br>We'll let you know by email if you're a winner.";?></p>


          <?php  
			
			if(count($errors) > 0 && $caller=='Submit')

			{

				echo '<div class="error" id="err_div"><ul>';

				foreach($errors as $error)

					echo "<li>$error</li>";

				echo '</ul></div>';

			}

		?>
        <div class="bodytext">
          <?php
		    $comp['email_content']=htmlspecialchars_decode($comp['email_content']);
			$term_tags = array('[CLOSING_DATE]','[START_DATE]','[DRAW DATE]','[NUMBER_WINNERS]','[NOTIFICATION_DATE]');
	$replace_tags = array(date('H:i \o\n j F, Y',strtotime($comp['end_date'])),date('H:i \o\n j F, Y',strtotime($comp['start_date'])),date('H:i \o\n j F, Y',strtotime($comp['drawtime'])),$comp['winners'],date('d F, Y',strtotime($comp['drawtime'])+(7*60*60*24)));		
	$comp['terms']=str_replace($term_tags,$replace_tags, $comp['terms']);
			 $band_color = $BAND_COLORS[$custband];

			$email_constants = array('[YOUTUBE]','[BOOKING_ID]','[FILM_NAME]','[IMAGE]','[USER_EMAIL]','[UNIQUE_DATE]','[UNIQUE_TIME]','[BRAND]','[TERMS]','[FNAME]','[LNAME],[ACTION_ID]');

			$email_constants_value = array($comp['analytics_id'],$booking_rand,

			$comp['promotion_title'],

			SITE_URL.$comp['image'],
			$email,
			date('d-m-Y',strtotime($comp['start_date'])),
			date('H:ia',strtotime($comp['start_date'])),
			$comp['brand'],
			$comp['terms'],
			$fname,
			$lname,
			$booking_rand
			);

			 $email_content=  str_replace($email_constants,$email_constants_value,$comp['email_content']);
			 if($comp['require_upload']==1){ 
				 $filename= $booking_rand.'-'.$custband.'-'.round(microtime(true));
				 $foldername= $comp['upload_dir'].'/'.preg_replace("/[^A-Za-z0-9- ]/", '',substr($user_answer,0,50));
			 
			 ?>
			<div class="card-body upload_text" id="upload-widget"><a name="C_UPLOAD" id="C_UPLOAD"></a>
                        <div id="widget">
        </div>
                    
                        </div>
                        

                <script>
                
               var myWidget = cloudinary.createUploadWidget({
                      cloudName: 'dnd68seoj', 
                      uploadPreset: 'wo1uwnop',
                      inlineContainer: "#widget",
                      publicId: '<?php echo $filename; ?>',
                      folder: '<?php echo $foldername; ?>',
                      
                         sources: [
                            "local"
                        ],
                        googleApiKey: "<image_search_google_api_key>",
                        showAdvancedOptions: false,
                        show_powered_by: false,
                        cropping: false,
                        multiple: false,
                        defaultSource: "local",
                        
                        
                    },
                      
                      (error, result) => { 
                        if (!error && result && result.event === "success") { 
                          console.log('Done! Here is the image info: ', result.info);
						  register_upload();
                          //alert("FILE HAS BEEN UPLOADED!")
                          //window.location = "index.php";
                          
                        }
                      }
                    )
                 myWidget.open();
                
                </script>  
			<?php
			 }
			 ?>
        </div>
        <div <?php if($comp['require_upload']==1) echo ' class="hidden"' ?>>
          <form action="all_comps.php" method="post" name="frm_back">
            <div class="buttondiv bottom-button">
              <input type="submit" class="btn_big"  name="btn_back" value="All exclusive prizes" />
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>
<?php require('upload_handler.php');?>
<script language="javascript">

$(document).ready(function() {

var data = {
    page: {
        name: "comp_success",
        breadcrumb: [ "skyapp", "stretch", "" ]
    }
};

	skyTags.queue.push(['set', data]);
});

function toggletc()
{
	$(".termcondition p").fadeToggle(1000);
}

<?  if(count($errors) > 0 && $caller=='Submit') { ?>
$( "#err_div" ).fadeOut(20000);
<? } ?>
</script>
<? 
if($comp['require_upload']==1)
{
	// we are not sending email if promotion requires file upload
	// we can do something else in future here.
}
else
{
	require('email/header.php');
	require('email/footer.php');
	$email_signoff="
	<br><br>
	Your Sky VIP Team";
	$from="noreply-admin@skyticketit.com";
	$etype = 'prize draw';
	if(!empty($comp['question']) )
		$etype = 'competition';
	$subject = "You've entered Sky VIP's $etype";
	
	$email_header = str_replace('[ETYPE]',$etype,$email_header);

	// Sending Email using API
	$email_content =  $email_header.$confirm_page_text.$email_content.$email_signoff.$email_footer;
	$from_name = 'Sky VIP';
	require(ROOT . 'common/Send_Email_API.php');
	$post = array(
	'to' => $email,  // if we send via CSV file we do not needs this line
	'from' => $from,
	'fromName' => $from_name,
	'subject' => $subject,
	'bodyHtml' => $email_content,
	'bodyText' => strip_tags($email_content),
	'channel' =>  str_replace('/','_', rtrim (DIR, '/')),      // sky/offer/ will produce sky_offer
	'isTransactional' => true
	);
	// if we send via CSV file we do not needs to send to in above field
	$result = send_email_api($post,$csv_file_name=NULL); 
}

if(0)
{
require(ROOT.'phpmailer/class.smtp.php');
require(ROOT.'phpmailer/class.phpmailer.php');

 $mail = new PHPMailer();
 
 $mail->isSMTP();
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->SMTPDebug = 0;
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	//Set the hostname of the mail server
	$mail->Host = "smtp.elasticemail.com";
	//Set the SMTP port number - likely to be 25, 465 or 587
	$mail->Port = '2525';
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;
	//Username to use for SMTP authentication
	$mail->Username = "webteam@stretchcomms.com";
	//Password to use for SMTP authentication
	$mail->Password = "5c0086c3-5b6d-45f6-a41b-86ac75268f92";
	$mail->addCustomHeader('IsTransactional', true);
		
  $mail->IsHTML(true);
  	//Set an alternative reply-to address
	$mail->addReplyTo($from, 'Sky VIP');
	//Set who the message is to be sent to
	
	$mail->setFrom($from, 'Sky VIP');
	
	$mail->Subject= $subject;
	$mail->msgHTML( $email_header.$confirm_page_text.$email_content.$email_signoff.$email_footer);
	$mail->AltBody = '';
	$mail->AddAddress($email);
	
	$mail->send();
}
//  unset($_SESSION['title'],$_SESSION['lname'],$_SESSION['fname'],$_SESSION['email'],$_SESSION['requested_tickets'],$_SESSION['booking_rand'],$_SESSION['checksum']);


