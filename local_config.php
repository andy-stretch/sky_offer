<?php
session_start();
//SITE Directory
define('SITE_URL','https://www.skyticketit.com/sky/offer/');

define('CDN_URL','https://1973584292.rsc.cdn77.org/');

//SITE Directory
define('DIR','sky/offer/');
// Document root directory
define('ROOT',$_SERVER['DOCUMENT_ROOT'].'/'); // /home/mediapromotions/public_html
// full path to site directory
define('SITE_ROOT',$_SERVER['DOCUMENT_ROOT'].'/'.DIR); 
 require(ROOT.'common/lib/Error_handler.php');
define('CATEGORY','comp'); 
$history_cat='competition';
$table['TABLE_PREFIX'] ="sky_";
$table['prefix'] = 'sky_';
$table['users'] = 'users';
$table['admin'] = 'admin';
$table['competition'] = 'competition';
$table['competition_data'] = 'competition_data';
$table['competition_game_data'] = 'competition_game_results';
$table['history'] = 'history';
$table['history_update'] = 'history_update';
define('DEF_VOLUME',1);
define('UPLOAD_IMAGE_SIZE',200000); // 200kb
$BANDS = array('A','B','C','D','E');//,'F','G','H','I','J');;
$BAND_COLORS = array('A'=>"#6E787B",'B'=>"#9F6802",'C'=>"#6E6C89",'D'=>"#252525",'E'=>"#CC901F",'F'=>"#CC901F",'G'=>"#CC901F",'P'=>"#CC901F",'U'=>"#CC901F",'X'=>"#CC901F");
$BAND_ENTRIES = array('A'=>1,'B'=>2,'C'=>3,'D'=>4,'E'=>1);
$BAND_ENTRIES_DISPLAY = array('A'=>"",
								'B'=>"<br><br>As you are a valued Sky customer with Sky VIP Gold membership, we've automatically added one more entry for you!",
								'C'=>"<br><br>As you are a valued Sky customer with Sky VIP Platinum membership, we've automatically added two more entries for you!",
							  	'D'=>"<br><br>As you are a valued Sky customer with Sky VIP Diamond membership, we've automatically added three more entries for you!",
							  	'E'=>"<br><br>As you are a valued Sky customer with Sky VIP Diamond membership, we've automatically added one more entries for you!");

$BAND_ENTRIES_DISPLAY_COMP = array('A'=>"",
								'B'=>"",
								'C'=>"",
							  	'D'=>"",
							  	'E'=>"");
$TODAY = date('Y-m-d H:i:s');
$TODAY_DATE = date('Y-m-d');	
$SUB_CATEGORY =array('sport','movies','ents','arts','kids'); 
$BAND_E_CPNs = array('13268806531377410604623','CX3647715S');
$DATE_DISPLAY_FORMAT =  'j M Y \a\t g:ia';

		/*	'D'=>"<br><br>As you are a Sky VIP Diamond member, we've automatically added three more entries to the draw for you!");  */
function get_sha1($Party_Id,$Cust_Band,$category,$date='')
{
	if(empty($date))
		$date=date('Ymd');
	if(empty($category))
		$category = CATEGORY;
	$Party_Id = (string)$Party_Id;
	$Cust_Band = (string)$Cust_Band;
	$Promo_Id = (string)$Promo_Id;
	$Salt_Key = 'uWpfKq3jhy1S3ctT8JXdF4nr0QHtAaTa';
	
	return hash('sha256',$Salt_Key . '$' . $Party_Id . '$' . $date. '$' . $Cust_Band.'$'.$category);
	
//	$Key . '$' . $Party_Id . '$' . date('Ymd') . '$' . $Cust_Band .'$'. $Promo_Id);
}
	function checkphone($phone)
	{
		$phone=str_replace(' ','',trim($phone));
		if(substr($phone,0,3)!='353' && substr($phone,0,2)!='44')
			return false;
		if(!is_numeric($phone))
			return false;
		$len = strlen($phone);
		if($len<10 || $len>14)
			return false;
		return true;
		
	}
