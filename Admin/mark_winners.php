<?php 
//
	session_start();
	require('../local_config.php');
	include(ROOT.'config/foxtel_connect.php');
	
	require("com_function.php");
	check_login();
	
	require(ROOT.'common/xss_safe.php');
	$xss = new xssSafe();
	
	require(ROOT.'common/db/DB_manager.php');
	//$db = new DB_manager(HOST, DBU, DBPASS, DB);
	$db        = new DB_manager(HOST, DBU, DBPASS, DB_HISTORY);
	$db->debug =1;
	
	$rand_num=mt_rand(); 
	$msg = array();
	if(!empty($_SESSION['action_token']) && $_POST['action_token']==$_SESSION['action_token'])
	{
		$action = $xss->clean_input($_POST['action']);
		$actiondate = $xss->clean_input($_POST['actiondate']);
		$group_id =  get_rand_id(10);;
		if(empty($action))
		{
			$msg['error'] = "Please enter action.";
		}
		elseif(empty($actiondate))
		{
			$msg['error'] = "Please enter action date.";
		}
		elseif($_FILES['winner_csv']['name'] !='')
		{
			if($_FILES["winner_csv"]["error"]==UPLOAD_ERR_OK)
			{
				$temp = explode(".", $_FILES["winner_csv"]["name"]);
				$extension = end($temp);
				$extension = strtoupper($extension);
				if($extension != 'CSV' && $extension != 'TXT')
					$msg['error'] = "The file is invalid. Only CSV or TXT file is accepted.";
				else
				{
					if($fp=fopen($_FILES["winner_csv"]["tmp_name"],"r"))
					{
						$lines = count($trimmed );
						$insert_batch_size = 500;
						$return['col_missmatch'] = $return['rec_found']=$return['total_rows'] = 0;
						$temp_count =0;
						
						while($str = fgets($fp))
						{
							if($return['total_rows']<1)
							{	
								$return['total_rows']++;
								continue;
							}
							$return['total_rows']++;							
							$str = explode(",",trim($str));
							if(count($str)!=2 )
							{	
								$return['col_missmatch']++;
								continue;
							}
							$return['rec_found']++;
							if($temp_count==0)
							{
								$insert_user_sql = "insert into `{$table['prefix']}{$table['history_update']}`
								( `action_id`, `action`, `action_date`, `rec_id`, `upload_date`, `upload_by`,group_id,executed)
								values ";
							}
							else {
								$insert_user_sql .=",";
							}
							$insert_user_sql .=" ('$str[1]','$action','$actiondate','$str[0]',now(),'$_SESSION[adminuser]','$group_id',-1) ";
							$temp_count++;

							if($temp_count>=$insert_batch_size)
							{
								$db->query($insert_user_sql)->execute();								
								$temp_count=0;
							}							
						}
						if($temp_count>0)
						{
							$db->query($insert_user_sql)->execute();
						}
						$data_db = DB;
						$history = DB_HISTORY;

						//$match_sql = "select D.id from $data_db.`{$table['prefix']}{$table['competition_data']}` D,  $history.`{$table['prefix']}{$table['history_update']}` H where H.rec_id=D.id and H.action_id=D.Booking_id and H.group_id='$group_id'";
						//$db->query($match_sql)->execute();	

						$msg['success'] = " Total records found on file : $return[total_rows] ";
						$msg['success'] .= "</li><li> Column mismatch : $return[col_missmatch] ";
						if($return['rec_found']>0)	
						{
							$msg['success'] .= "</li><li> records uploaded : $return[rec_found] ";							
							$confirm_update_sql = "update  $history.`{$table['prefix']}{$table['history_update']}` U, $data_db.`{$table['prefix']}{$table['competition_data']}` D set U.`promo_id`=D.treat_id, U.`party_id`=D.partyid, executed=0 WHERE U.executed=-1 and U.group_id='$group_id' and D.booking_id=U.action_id and D.id=U.rec_id	";
							$db->query($confirm_update_sql)->execute();	
							$msg['added_update'] = $db->affected_rows. " record added for update";
						
							$get_from_history = "update `{$table['prefix']}{$table['history_update']}` U, `{$table['prefix']}{$table['history']}` H 
							set U.`category_id`=H.`category_id`, U.`promo_title`=H.`promo_title`, U.`promo_date`=H.`promo_date`, U.`location`=H.`location`, U.`volume`=H.`volume`, U.`original_band`=H.`original_band`, U.`original_action_date`=H.`original_action_date`, U.`direct_ticket_link`=H.`direct_ticket_link`
							  WHERE U.executed=0 and U.group_id='$group_id' and H.action_id = U.action_id and H.party_id=U.party_id and H.promo_id=U.promo_id";
							$db->query($get_from_history)->execute();	
							$msg['from_history'] = $db->affected_rows. " record found on history";

							$_SESSION['action_token']=NULL;
							require('mark_winner_confirm.php');
						exit(0);}
					}
					else
						$msg['error'] = 'can not open the uploaded file';							
				}
			
			}
			else
				$msg['error']="Error to upload unique codes : ".$_FILES["winner_csv"]["error"];
		}	
	}
?><?php	
	require("header.php"); 
?>
<div class="content">
    <h1 style="padding-left:140px;">Update History record (winners)</h1>
    <?php if(!empty($msg)){?>
  <div style="background:#FFBFC1; color:#D70005;margin:0px 50px; padding:0px; 50px;">
    <li><?php echo implode('</li>
	<li>',$msg);?></li>
  </div>
    <?php } ?>
    <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1"><form action="mark_winners.php" method="post" name="frm_random_winners" enctype="multipart/form-data"  id="frm_random_winners">
  <?php if(0){ ?> 
      <tr>
        <td bgcolor="#8090AB"><strong>Competition:*</strong></td>
        <td bgcolor="#93A5C4"><select name="comp_id" id="comp_id">
         <?php 
		 $rows = $db->from($table['competition'])->order_by('start_date','desc')->fetch(); 
		foreach($rows as $c)
		{
			

		 //$comp_sql = "select * from  order by start_date";
		 //$comp_result = mysql_query($comp_sql) or die(mysql_error());
		 //while($c = mysql_fetch_assoc($comp_result))
		// {
			 $selected = '';
			 if($c['treat_id']==$comp_id)
			 {	$selected='selected';
			 	$comp=$c;
			 }
		 ?>
         <option value=<?php echo '"'.$c['treat_id'].'" '.$selected;?>><?php echo $xss->clean_input($c['promotion_title']);?></option>
         <?php } ?>
         </select></td>
      </tr>
	<?php } ?>
	  <tr>
        <td bgcolor="#8090AB">Winner data (csv)</td>
        <td bgcolor="#93A5C4"><input name="winner_csv" type="file" id="winner_csv" />
          <?php if(1){ ?> 
          <a target="_blank" href="winner_csv_sample.csv">download sample</a>
          <? } ?>
          </td>
      </tr>
      <tr>
        <td width="18%" bgcolor="#8090AB"><strong>Set Action:</strong></td>
        <td width="82%" bgcolor="#93A5C4"><input name="action" list="action_list" type="text" id="action" value="<?php echo $action;?>" /></td>
			<datalist id="action_list">
			<option>requested</option>
			<option>received</option>			
			<option>played</option>
			<option>drawn</option>
			</datalist>
      </tr>
	  <tr>
         <td bgcolor="#8090AB">Action Date:</td>
         <td bgcolor="#93A5C4"><input name="actiondate" type="text" id="actiondate" value="<?php echo $xss->clean_input($post_input['actiondate']);?>" size="25" />
           <input type="button" value="..." id="btn_actiondate" /></td>
       </tr>
	   <script type="text/javascript">//<![CDATA[

		var cal = Calendar.setup({
			onSelect: function(cal) { cal.hide() },
			showTime: true
		});
		cal.manageFields("btn_actiondate", "actiondate", "%Y-%m-%d %H:%M");
		//]]></script>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4"><input type="submit" value="Upload Winners" /></td>
      </tr>
     	<input type="hidden" name="action_token" value="<?php echo $_SESSION['action_token']=get_rand_id(10);?>" />
      </form>
</table>
<p>&nbsp;</p>
  <p>&nbsp; </p>
    <!-- end .content --></div>
<?php   require("footer.php");  ?>