<? 
session_start();
header('X-Frame-Options: SAMEORIGIN');
ini_set( 'session.cookie_httponly', 1 );
ini_set( 'session.cookie_secure', 1 );

define('BASEPATH','10');
//define('APPPATH',$_SERVER['DOCUMENT_ROOT'].'/sky/booking/application/');
require('../local_config.php');
$msg = "";

$code_sent = false;

spl_autoload_register(function($class)
{
	// project-specific namespace prefix
	$prefix   = 'MessageBird\\';
	// base directory for the namespace prefix
	$base_dir =  ROOT.'sky/booking/application/third_party/';
	// does the class use the namespace prefix?
	$len      = strlen($prefix);
	if (strncmp($prefix, $class, $len) !== 0) {

	} //strncmp($prefix, $class, $len) !== 0
	// separators with directory separators in the relative class name, append
	// with .php
	$class = str_replace('\\', '/', $class);
	$file  = $base_dir . $class . '.php';
	// if the file exists, require it
	if (file_exists($file)) {
		require $file;
	} //file_exists($file)
});
$output=array();
//var_dump($_SESSION);
if($_POST['user_token']==$_SESSION['USER_TOKEN'])
{
	require('../../booking/application/config/database.php');
	require('../../booking/application/config/config.php');
	
	require(ROOT.'common/db/DB_manager.php');
	$db = new DB_manager($db['default']['hostname'], $db['default']['username'], $db['default']['password'], $db['default']['database']);
	$db->debug = 1;
	
	$admin_user = $db->from($config['db_tables']['admin'])->where('username',$_POST['uname'])->fetch_first(); 
	
	if($admin_user)
		if($admin_user['attempt']<5)
		{
			$login_code= "".substr(hexdec( sha1($TODAY_DATE.$admin_user['email_id']) ),0,7)*100000;
			//$login_code= "".substr(hexdec( sha1($TODAY_DATE.$admin_user['email_id']) ),0,7)*100000;;
		
			if($admin_user['level_num']==7)
			{
				$output['errors']='you do not have access to this page';
			}
			else
			{
				$email_template_dir = $config['EMAIL_TEMPLATE_DIR'];
				
				$email_body = file_get_contents('../../booking/application/views/'.$email_template_dir.'login_code.php');
				$email_body = str_replace('<?=$login_code;?>',$login_code,$email_body);
				$text_only = strip_tags($email_body);
				$ret = send_email($admin_user['email_id'],'Your authorisation code',$email_body);

				if(!empty($admin_user['mobile']))
				{	$ret=send_sms($admin_user['mobile'],$text_only);
				}
				$user_data = array('last_code'=>$date_ptz);
				$db->where(array('id'=>$admin_user['id']))->limit(1)->update($config['db_tables']['admin'],$user_data);
				$code_sent = true;
				$output['success']="Login code is sent to your email and mobile";
			}
		}
		else
			$output['errors']='Your account is locked';
	else	
		$output['errors']='Your account is locked';
}
require(ROOT.'common/functions.php');
$output['user_token'] =$_SESSION['USER_TOKEN']=my_random_string(10);
echo json_encode($output);


	
function send_sms($mobile,$smsbody)
{
	global $config;
	$MessageBird = new \MessageBird\Client($config['smsbird_key']); // Set your own API access key here.
	$Message             = new \MessageBird\Objects\Message();
	$Message->originator = $config['SMS_FROM'];
	
	$Message->recipients = array($mobile);
	$Message->body       = $smsbody;
	
	try {
		$MessageResult = $MessageBird->messages->create($Message);
		return true;
	
	} catch (\MessageBird\Exceptions\AuthenticateException $e) {
		// That means that your accessKey is unknown
		return false;
	
	} catch (\MessageBird\Exceptions\BalanceException $e) {
		// That means that you are out of credits, so do something about it.
		return false;
	
	} catch (\Exception $e) {
		return false;
	}

}



function send_email($email,$subject,$body)
{
global $config;
$from = $config['FROM_EMAIL'];
require(ROOT.'phpmailer/class.smtp.php');
require(ROOT.'phpmailer/class.phpmailer.php');

 $mail = new PHPMailer();
 
 $mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = "smtp.elasticemail.com";
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = '2525';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication
$mail->Username = "webteam@stretchcomms.com";
//Password to use for SMTP authentication
$mail->Password = "5c0086c3-5b6d-45f6-a41b-86ac75268f92";
$mail->addCustomHeader('IsTransactional', true);
	
$mail->IsHTML(true);
  //Set an alternative reply-to address
$mail->addReplyTo($from, 'Foxtel First');
//Set who the message is to be sent to
try
{
	$mail->setFrom($from, 'Foxtel First');
	$mail->Subject= $subject;
	$mail->msgHTML( $body);
	$mail->AltBody = '';
	$mail->AddAddress($email);

	return $mail->send();
} 
	catch (phpmailerException $e) 
	{
		  return false;
		 //Pretty error messages from PHPMailer
	} catch (Exception $e) {
		  return false;
		//Boring error messages from anything else!
	}
}