<?php 
//
	session_start();
	require('../local_config.php');
	include(ROOT.'config/sky_connect.php');
	
	require("com_function.php");
	check_login();
	
	require(ROOT.'common/xss_safe.php');
	$xss = new xssSafe();
	
	require(ROOT.'common/db/DB_manager.php');
	$db = new DB_manager(HOST, DBU, DBPASS, DB);
	$db->set_table_prefix('sky_');
	$db->debug =1;
	
	$rand_num=mt_rand(); 
	$rand_id= str_shuffle(sha1('$sec12etk3yfor'.$rand_num));
	$errors = '';
	if(!empty($_SESSION['action_token']) && $_POST['action_token']==$_SESSION['action_token'])
	{
		$comp_id = $_POST['comp_id'];
		$winners = $_POST['winners'];
		
		$comp     = $db->from($table['competition'])->where('treat_id', $comp_id)->fetch_first();
	  if(empty($comp_id) || !$comp)
	  	$errors[] = 'Please select competition';
	  if(empty($winners))
	  	$errors[] = 'Please enter #Winners';
		
	  if(empty($errors))
	  {
//		  $db->select('Title,First_Name,Last_Name,Address_1,Address_2,Address_3,Town,Postcode,Day_Phone,Email_Address,treat_id,Registration_Date,partyid,custband,Answer');
 		  $db->select('Title,First_Name,Last_Name,REPLACE(Address_1,",","/"),REPLACE(Address_2,",","/"),REPLACE(Address_3,",","/"),REPLACE(Town,",","/"),REPLACE(Postcode,",","/"),Day_Phone,Email_Address,treat_id,Registration_Date,partyid,custband,Answer,id,booking_id,email_read');
 
		  $db->from( $table['competition_data'] );
		  if($comp['require_upload']==1)
		  {
			  $db->where("email_read is NOT NULL");
			  $db->where("email_read !=",'');
		  }
		  $db->where('treat_id',$comp_id);
		  $db->limit($winners);
		  $db->order_by('rand()');
		  $result = $db->fetch(); 
		
		  if(!$result)
		  	$errors[] ='No User Records found...';
		else
		{
			
			$winner_data = $file_name = "SKY-Comp-$comp_id-$winners.csv";
			header("Content-type: application/text");
			header("Content-Disposition: attachment; filename=$file_name");
			header("Pragma: no-cache");
			header("Expires: 0");
			echo 'Title,First Name,Last Name,Address_1,Address_2,Address_3,Town,Postcode,Day_Phone,Email_Address,Treat_Id,Registration_Date,PartyId,CustBand,Answer,id,Booking_id,File_name
';
			$winner_count = 0 ;
			foreach($result as $row)
			{
				echo implode(',',$row);
				echo '
';
				$winner_count++;
			}
			if($winner_count<501)
				$winner_data = json_encode($result);

			$admin_log_array = array('username'=>$_SESSION['adminuser'],'pagename'=>__FILE__,'action_title'=>"$winners Winners drawn for $comp_id ",'action_detail'=>$winner_data,'datetime'=>$TODAY,'ip'=>$_SERVER['REMOTE_ADDR']); 
			log_action($admin_log_array);
			
			exit();			
		}
	  }
	}
?><?php	
	require("header.php"); 
?>
<div class="content">
    <h1 style="padding-left:140px;">Search User</h1>
    <?php if(!empty($errors)){?>
  <div style="background:#FFBFC1; color:#D70005;margin:0px 50px; padding:0px; 50px;">
    <li><?php echo implode('</li>
	<li>',$errors);?></li>
  </div>
    <?php } ?>
    <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1"><form action="random_winners.php" method="post" name="frm_random_winners" id="frm_random_winners">
    <tr>
      <td colspan="2"><strong>Please enter customer name: </strong></td></tr>
      <tr>
        <td bgcolor="#8090AB"><strong>Competition:*</strong></td>
        <td bgcolor="#93A5C4"><select name="comp_id" id="comp_id">
         <?php 
		 $rows = $db->from($table['competition'])->order_by('start_date','desc')->fetch(); 
		foreach($rows as $c)
		{
			

		 //$comp_sql = "select * from  order by start_date";
		 //$comp_result = mysql_query($comp_sql) or die(mysql_error());
		 //while($c = mysql_fetch_assoc($comp_result))
		// {
			 $selected = '';
			 if($c['treat_id']==$comp_id)
			 {	$selected='selected';
			 	$comp=$c;
			 }
		 ?>
         <option value=<?php echo '"'.$c['treat_id'].'" '.$selected;?>><?php echo $xss->clean_input($c['promotion_title']);?></option>
         <?php } ?>
         </select></td>
      </tr>
      <tr>
        <td width="18%" bgcolor="#8090AB"><strong>#Winners:*</strong></td>
        <td width="82%" bgcolor="#93A5C4"><input name="winners" type="text" id="winners" value="<?php echo $winners;?>" /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4"><input type="submit" value="Download Winners" /></td>
      </tr>
     	<input type="hidden" name="action_token" value="<?php echo $_SESSION['action_token']=$rand_id;?>" />
      </form>
</table>
<p>&nbsp;</p>
<?php if(!empty($result)){?>
  <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1">
      <tr>
        <td colspan="5"><strong>Search Result</strong></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB"><strong>First Name</strong></td>
        <td bgcolor="#8090AB"><strong>Last Name</strong></td>
        <td bgcolor="#8090AB"><strong>Email</strong></td>
        <td bgcolor="#8090AB"><strong>Band</strong></td>
        <td bgcolor="#8090AB"><strong>Action</strong></td>
    </tr>
      <?php 
	  foreach($result as $row)
	  { ?>
      <tr>
        <td width="14%" bgcolor="#CCCCCC"><?php echo $row['First_Name'];?></td>
        <td width="16%" bgcolor="#CCCCCC"><?php echo $row['Last_name'];?></td>
        <td width="33%" bgcolor="#CCCCCC"><?php echo $row['Email_Address'];?></td>
        <td width="11%" bgcolor="#CCCCCC"><?php echo $row['custband'];?></td>
        <td width="26%" bgcolor="#CCCCCC">View/Mark as Winner</td>
      </tr>
      <?php } ?>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    
  </table>
  <?php } ?>
  <p>&nbsp; </p>
    <!-- end .content --></div>
<?php   require("footer.php"); 

  // INSERT INTO sky_history (`party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`,`original_band`,`original_action_date`,`direct_ticket_link`) values (?,?,?,?,now(),?,?,?,?,?,now(),?)  ON DUPLICATE KEY UPDATE party_id = values(party_id),category_id = values(category_id),promo_id = values(promo_id),promo_title = values(promo_title),action_date = now(),promo_date = values(promo_date),location= values(location),action = values(action),volume = values(volume),action_id=values(action_id) 
  ?>