<?php 
//
	session_start();
	require('../local_config.php');
	include(ROOT.'config/sky_connect.php');
	
	require("com_function.php");
	check_login();
	
	require(ROOT.'common/xss_safe.php');
	$xss = new xssSafe();
	
	require(ROOT.'common/db/DB_manager.php');
	$db = new DB_manager(HOST, DBU, DBPASS, DB);
	$db->set_table_prefix('sky_');
	$db->debug =1;
	
	$rand_num=mt_rand(); 
	$rand_id= str_shuffle(sha1('$sec12etk3yfor'.$rand_num));
	$errors = '';
	if(!empty($_SESSION['action_token']) && $_POST['action_token']==$_SESSION['action_token'])
	{
		$comp_id = $_POST['comp_id'];
		$search_in = $_POST['search_in'];
		$search = $_POST['search'];
		
	  if(empty($comp_id))
	  	$errors[] = 'Please select competition';
	  if(empty($search_in))
	  	$errors[] = 'Please select Search In option ';
	  if(empty($search))
	  	$errors[] = 'Please enter search keyword';
	  if(empty($errors))
	  {
		  $db->from( $table['competition_data'] );
		  $db->where('treat_id',$comp_id);
		  switch($search_in)
		  {
			  case 'F':
				$db->like('first_name',$search); 	
			  	break;
			case 'L':
				$db->like('last_name',$search);
				break;
			case 'E':
				$db->like('email_address',$search);
				break;
			case 'P':
				$db->like('partyid',$search);
				break;
		  }
		  $result = $db->fetch(); 
		  if(!$result)
		  	$errors[] ='No User Records found...';
	  }
	}
?><?php	
	require("header.php"); 
?>
<div class="content">
    <h1 style="padding-left:140px;">Search User</h1>
    <?php if(!empty($errors)){?>
  <div style="background:#FFBFC1; color:#D70005;margin:0px 50px; padding:0px; 50px;">
    <li><?php echo implode('</li>
	<li>',$errors);?></li>
  </div>
    <?php } ?>
    <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1"><form action="search_user.php" method="post" name="frm_search_user" id="frm_search_user">
    <tr>
      <td colspan="2"><strong>Please enter customer name: </strong></td></tr>
      <tr>
        <td bgcolor="#8090AB"><strong>Competition:*</strong></td>
        <td bgcolor="#93A5C4"><select name="comp_id" id="comp_id">
         <?php 
		 $rows = $db->from($table['competition'])->order_by('start_date','desc')->fetch(); 
		foreach($rows as $c)
		{
			

		 //$comp_sql = "select * from  order by start_date";
		 //$comp_result = mysql_query($comp_sql) or die(mysql_error());
		 //while($c = mysql_fetch_assoc($comp_result))
		// {
			 $selected = '';
			 if($c['treat_id']==$comp_id)
			 {	$selected='selected';
			 	$comp=$c;
			 }
		 ?>
         <option value=<?php echo '"'.$c['treat_id'].'" '.$selected;?>><?php echo $xss->clean_input($c['promotion_title']);?></option>
         <?php } ?>
         </select></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB"><strong>Search In:*</strong></td>
        <td bgcolor="#93A5C4"><label><input type="radio" name="search_in" value="F" <?php if($search_in=='F') echo 'checked';?> /> First Name</label>
        	<label><input type="radio" name="search_in" value="L"  <?php if($search_in=='L') echo 'checked';?>  /> Last Name</label>
            <label><input type="radio" name="search_in" value="E" <?php if($search_in=='E') echo 'checked';?>  /> Email </label>
        <label><input type="radio" name="search_in" value="P" <?php if($search_in=='P') echo 'checked';?>  /> Party ID</label></td>
      </tr>
      <tr>
        <td width="18%" bgcolor="#8090AB"><strong>Search Word:*</strong></td>
        <td width="82%" bgcolor="#93A5C4"><input name="search" type="text" id="search" value="<?php echo $search;?>" /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4"><input type="submit" value="Search User" /></td>
      </tr>
     	<input type="hidden" name="action_token" value="<?php echo $_SESSION['action_token']=$rand_id;?>" />
      </form>
</table>
<p>&nbsp;</p>
<?php if(!empty($result)){?>
  <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1">
      <tr>
        <td colspan="6"><strong>Search Result</strong></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB"><strong>First Name</strong></td>
        <td bgcolor="#8090AB"><strong>Last Name</strong></td>
         <td bgcolor="#8090AB"><strong>Party ID</strong></td>
       <td bgcolor="#8090AB"><strong>Email</strong></td>
       <td bgcolor="#8090AB"><strong>Band</strong></td>
        <td bgcolor="#8090AB"><strong>Answer</strong></td>
        <td bgcolor="#8090AB"><strong>Action</strong></td>
    </tr>
    <form action="edit_user.php" method="post" name="frm_edit_user" id="frm_edit_user">
      <?php 
	  foreach($result as $row)
	  { ?>
      <tr>
        <td width="14%" bgcolor="#CCCCCC"><?php echo $row['First_Name'];?></td>
        <td width="16%" bgcolor="#CCCCCC"><?php echo $row['Last_name'];?></td>
        <td width="33%" bgcolor="#CCCCCC"><?php echo $row['partyid'];?></td>
        <td width="33%" bgcolor="#CCCCCC"><?php echo $row['Email_Address'];?></td>
        <td width="11%" bgcolor="#CCCCCC"><?php echo $row['custband'];?></td>
        <td width="11%" bgcolor="#CCCCCC"><?php echo $row['answer'];?></td>
        <td width="26%" bgcolor="#CCCCCC"><a href="#" onclick="return call_edit_user(<?php echo $row['Id'];?>);">View/Mark as Winner</a></td>
      </tr>
      <?php } ?>
      <input type="hidden" name="user_id" id="user_id" value="" />
      <input type="hidden" name="comp_id" id="comp_id" value="<?php echo $comp_id;?>" />
      <input type="hidden" name="action_token" value="<?php echo $_SESSION['action_token']=$rand_id;?>" />
      </form>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    
  </table>
  <?php } ?>
  <p>&nbsp; </p>
    <!-- end .content --></div>
<?php   require("footer.php"); 

  // INSERT INTO sky_history (`party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`,`original_band`,`original_action_date`,`direct_ticket_link`) values (?,?,?,?,now(),?,?,?,?,?,now(),?)  ON DUPLICATE KEY UPDATE party_id = values(party_id),category_id = values(category_id),promo_id = values(promo_id),promo_title = values(promo_title),action_date = now(),promo_date = values(promo_date),location= values(location),action = values(action),volume = values(volume),action_id=values(action_id) 
  ?>
  <script language="javascript">
  function call_edit_user(uid)
  {
	  document.frm_edit_user.user_id.value= uid;
	  document.frm_edit_user.submit();
  		return false;
  }
  </script>