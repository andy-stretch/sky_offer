<?

function create_table($table_name)
{
	$create_sql = "
	CREATE TABLE IF NOT EXISTS `$table_name` (
  `Id` int(11) NOT NULL auto_increment,
 `Title` varchar(255) default NULL,
 `First_Name` varchar(255) default NULL,
 `Last_name` varchar(255) default NULL,
 `Address_1` varchar(255) default NULL,
 `Address_2` varchar(255) default NULL,
 `Address_3` varchar(255) default NULL,
 `Town` varchar(255) default NULL,
 `Postcode` varchar(20) default NULL,
 `Day_Phone` varchar(50) default NULL,
 `Mobile_Phone` varchar(50) default NULL,
 `Email_Address` varchar(255) default NULL,
 `Date_Of_Birth` date default NULL,
 `Gender` varchar(20) default NULL,
 `NI_Email_Permission` varchar(20) default NULL,
 `NI_SMS_Permission` varchar(20) default NULL,
 `NI_Post_and_Phone_Permission` varchar(20) default NULL,
 `Third_Party_Post_and_Phone_Permission` varchar(20) default NULL,
 `Buy_Times` varchar(50) default NULL,
 `Buy_Sat_Times` varchar(50) default NULL,
 `Buy_Sun_Times` varchar(50) default NULL,
 `Code` varchar(20) default NULL,
 `Recency_Date` date default NULL,
 `Partner_Permission` varchar(20) default NULL,
 `Answer` text,
 `Tickets` varchar(255) default NULL,
 `Booking_Id` varchar(255) default NULL,
 `Own_Ipad` varchar(255) default NULL,
 `Read_News_Online` varchar(255) default NULL,
 `IP_Address` varchar(255) default NULL,
 `OS` varchar(255) default NULL,
 `Registration_Date` datetime default NULL,
 `custom1` varchar(255) NOT NULL,
 `custom2` varchar(255) NOT NULL,
 `custom3` varchar(255) NOT NULL,
 PRIMARY KEY  (`Id`)
) ENGINE=MyISAM ";

	mysql_query($create_sql);
	if(mysql_error() != "")
		return false;
	return true;
}

function table_exists($table_name)
{
	$tmp_sql = "select 1 from $table_name limit 1";
	mysql_query($tmp_sql) ; 
	$my_error = mysql_error();
	if(!empty($my_error))
	{	
		echo " -- Empty ";
		return false;
	}
	return true;	
}

function check_login($redirect=true)
{
	$key = "aA2Kkl34rsSm7AFs";
	$logged_in = true;
	
	if($_SESSION['isAdmin'] != '' && $_SESSION['Ltm'] !='')
	{
		$tme = ($_SESSION['Ltm']*1)/10 ;
		
		$tme = strrev("".$tme);
		if($_SESSION['isAdmin'] != md5($tme.$key))
			$logged_in =false;
	}
	else
		$logged_in =false;
	if($redirect!=true)
		return $logged_in;
	if(!$logged_in )
	{
			header("Location:login.php");
			exit();
	}
}

function check_login_cnt()
{
	$key = "aS7eT5GouXd2nv0L";
	$logged_in = true;
	
	if($_SESSION['iscNtAdmin'] != '' && $_SESSION['Lctm'] !='')
	{
		$tme = ($_SESSION['Lctm']*1)/10 ;
		
		$tme = strrev("".$tme);
		if($_SESSION['iscNtAdmin'] != md5($tme.$key))
			$logged_in =false;
	}
	else
		$logged_in =false;
	
	if(!$logged_in )
		return false;
	return true;
}
	function get_browser_info ($agent) {
	// Get the name the browser calls itself and what version
		$browser_info['Name'] = strtok($agent, "/");
		$browser_info['Version'] = strtok(" ");
	
	 $ua = $agent;
		$checker = array(
		  'iphone'=>preg_match('/iPhone/', $ua),
		   'iPod'=>preg_match('/iPodWWWW/', $ua),
			'iPad'=>preg_match('/iPad/', $ua),
		  'blackberry'=>preg_match('/BlackBerry/', $ua),
		  'android'=>preg_match('/Android/', $ua),
		);
		$browser_info['Platform']='';
		if ($checker['iphone'])
			$browser_info['Platform']='Iphone';
		if ($checker['iPod'])
			$browser_info['Platform']='iPod';
		if ($checker['iPad'])
			$browser_info['Platform']='iPad';
		if ($checker['blackberry'])
			$browser_info['Platform']='BlackBerry';
		if ($checker['android'])
			$browser_info['Platform']='Android';
		if($browser_info['Platform']=='')
		{
			$OSList = array
			(
					// Match user agent string with operating systems
					'Windows 3.11' => 'Win16',
					'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)',
					'Windows 98' => '(Windows 98)|(Win98)',
					'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
					'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
					'Windows Server 2003' => '(Windows NT 5.2)',
					'Windows Vista' => '(Windows NT 6.0)',
					'Windows 7' => '(Windows NT 7.0)|(Windows NT 6.1)',
					'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
					'Windows ME' => 'Windows ME',
					'Open BSD' => 'OpenBSD',
					'Sun OS' => 'SunOS',
					'Linux' => '(Linux)|(X11)',
					'Mac OS' => '(Mac_PowerPC)|(Macintosh)',
					'QNX' => 'QNX',
					'BeOS' => 'BeOS',
					'OS/2' => 'OS/2',
					'Search Bot'=>'(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)'
			);
		
			// Loop through the array of user agents and matching operating systems
			
			foreach($OSList as $CurrOS=>$Match)
			{
					// Find a match
					if (@eregi($Match,$agent))
					{
							// We found the correct match
							break;
					}
			} 
			$browser_info['Platform']=$CurrOS;
		}
		 if($browser_info['Platform']=='')
			 $browser_info['Platform']='UnKnown';
		return $browser_info; 
	 
	} 
	
	
	
function UploadImage($source, $target)
{
	if (!file_exists($source))
		return false;
	if (move_uploaded_file($source, $target)) 
		return true;
	return false;
	
	$sourceImg = @imagecreatefromstring(@file_get_contents($source));
	if ($sourceImg === false)
		return false;
	
	$width = imagesx($sourceImg);
	$height = imagesy($sourceImg);
	$targetImg = imagecreatetruecolor($width, $height);
	imagecopy($targetImg, $sourceImg, 0, 0, 0, 0, $width, $height);
	imagedestroy($sourceImg);
	imagepng($targetImg, $target);
	imagedestroy($targetImg);
	return true;
}

function log_action($data_array)
{
	if(!defined('BASEPATH'))
		define('BASEPATH',$_SERVER['DOCUMENT_ROOT']);

	require_once('../../booking/application/config/database.php');
	require_once('../../booking/application/config/config.php');
	
	$db2 = new DB_manager($db['default']['hostname'], $db['default']['username'], $db['default']['password'], $db['default']['database']);
	$db2->debug = 0;
	foreach($data_array as $k=>$v)
		$data_array[$k] = $db2->escape($v);
	
	$db2->insert($config['db_tables']['admin_actions'],$data_array);
}


function date_diff_decimal($date1='',$date2='')
{
	if(empty($date1) || empty($date1))
		return false;
	
	$datetime1 = new DateTime($date1);
	$datetime2 = new DateTime($date2);

	$epoch1 = $datetime1->getTimestamp();
	$epoch2 = $datetime2->getTimestamp();

	$diff = $epoch1 - $epoch2;

	return number_format( $diff / (3600*24), 2 );
}
