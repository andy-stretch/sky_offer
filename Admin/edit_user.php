<?php 
//
	session_start();
	require('../local_config.php');
	include(ROOT.'config/sky_connect.php');
	
	require("com_function.php");
	check_login();
	
	require(ROOT.'common/xss_safe.php');
	$xss = new xssSafe();
	
	require(ROOT.'common/db/DB_manager.php');
	$db = new DB_manager(HOST, DBU, DBPASS, DB);
	$db->set_table_prefix('sky_');
	$db->debug =1;
	
	$today = date('Y-m-d H:i:s');
	
	$rand_num=mt_rand(); 
	$rand_id= str_shuffle(sha1('$sec12etk3yfor'.$rand_num));
	$errors = $success ='';
	if(!empty($_SESSION['action_token']) && $_POST['action_token']==$_SESSION['action_token'])
	{
		$user_id = $_POST['user_id'];
		$comp_id = $_POST['comp_id'];
		$update_history = 0;
		if($_POST['update']=='winner')
		{
			$winner = $_POST['winner'];
			$user_data=array('selection_date'=>date('Y-m-d H:i:s'),'winner'=>$winner);
			$db->where('id',$user_id);
			$db->where('treat_id',$comp_id);
			$db->limit(1);
			$db->update( $table['competition_data'],$user_data);
			$success = 'Winner record updated';
			if($db->affected_rows)
				$update_history = 1;
		}
		$db->select('Title,First_Name,Last_Name,Address_1,Address_2,Address_3,Town,Postcode,Day_Phone,Email_Address,treat_id,Registration_Date,partyid,custband,winner,partyid,Booking_Id');
		$db->from( $table['competition_data'] );
		$db->where('treat_id',$comp_id);
		$db->where('id',$user_id);
		$db->limit(1);
		
		$result = $db->fetch(); 
		if($update_history)
		{	
			$win_action = 'requested';
			if($winner)
				$win_action = 'received';
			$comp     = $db->from($table['competition'])->where('treat_id', $comp_id)->fetch_first();
			$sql = sprintf("INSERT INTO sky_%s (`party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`,`original_band`,`original_action_date`)  values('%s','%s','%s','%s',now(),'%s','%s','%s','%s','%s','%s',now()) ", $table['history'], $result[0]['partyid'], strtolower(CATEGORY), $comp_id, $comp['promotion_title'],$comp['end_date'],'' , $win_action, DEF_VOLUME, $result[0]['Booking_Id'], $result[0]['custband']);
			$sql .= ' ON DUPLICATE KEY UPDATE party_id = values(party_id),category_id = values(category_id),promo_id = values(promo_id),promo_title = values(promo_title),action_date = now(),promo_date = values(promo_date),location= values(location),action = values(action),volume = values(volume),action_id=values(action_id)';
			unset($db);
			$db        = new DB_manager(HOST, DBU, DBPASS, DB_HISTORY);
			$db->debug = 1;
			$db->query($sql)->execute();
			
			$admin_log_array = array('username'=>$_SESSION['adminuser'],'pagename'=>__FILE__,'action_title'=>"$success :$user_id",'action_detail'=>"treat_id : $treat_id, Rec_id: $user_id",'datetime'=>$TODAY,'ip'=>$_SERVER['REMOTE_ADDR']); 
			log_action($admin_log_array);
			
			if(0){
			$db = new DB_manager(HOST, DBU, DBPASS, DB_HISTORY);
			$db->set_table_prefix('sky_');
			$db->debug = 1;
			$user_data=array('action'=>'received','volume'=>1);
			$db->where('action_id',$result[0]['Booking_Id']);
			$db->where('party_id',$result[0]['partyid']);
			$db->where('promo_id',$comp_id);
			$db->limit(1);
			}
		}
	}
	else
	{
		header('Location:search_user.php');
		exit();
	}
?><?php	
	require("header.php"); 
?>
<div class="content">
    <h1 style="padding-left:140px;">Search User</h1>
    <?php if(!empty($errors)){?>
  <div style="background:#FFBFC1; color:#D70005;margin:0px 50px; padding:0px; 50px;">
    <li><?php echo implode('</li>
	<li>',$errors);?></li>
  </div>
    <?php } 
	if(!empty($success))	echo 
  '<div style="background:#D2FDB9; color:#006600;margin:0px 50px; padding:0px; 50px;">'.$success.'</div>';
  
	if(!empty($result)){?>
  <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1">
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      
      <?php 
	  foreach($result as $row)
	  { ?>
      <tr>
        <td bgcolor="#8090AB"><strong>First Name</strong></td>
        <td bgcolor="#CCCCCC"><?php echo $row['First_Name'];?></td>
    </tr>
      <tr>
        <td bgcolor="#8090AB"><strong>Last Name</strong></td>
        <td bgcolor="#CCCCCC"><?php echo $row['Last_Name'];?></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB"><strong>Email</strong></td>
        <td bgcolor="#CCCCCC"><?php echo $row['Email_Address'];?></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB"><strong>Band</strong></td>
        <td bgcolor="#CCCCCC"><?php echo $row['custband'];?></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB"><strong>Address</strong></td>
        <td bgcolor="#CCCCCC"><?php echo $row['Address_1'] ;
								if(!empty($row['Address_2']))
									echo ', ' .$row['Address_2'] ;
								if(!empty($row['3ddress_3']))
									echo  ', '.$row['3ddress_3']  ;
								if(!empty($row['Postcode']))
									echo '<br>'.$row['Postcode']. ', '. $row['Town'];
				?></td>
      </tr>
      <form name="frm_edit_user" id="frm_edit_user" action="edit_user.php" method="post">
      <tr>
        <td bgcolor="#8090AB"><strong>Winner</strong></td>
        <td bgcolor="#CCCCCC"><label><input name="winner" type="radio" value="1" <?php if($row['winner']==1) echo 'checked';?> /> Yes</label> &nbsp; &nbsp; <label><input name="winner" type="radio" value="0"  <?php if($row['winner']!=1) echo 'checked';?>  /> No</label></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#CCCCCC"><input type="submit" value="Update Winner Status" /></td>
      </tr>
      <input type="hidden" value="winner" name="update" />
      <input type="hidden" name="user_id" value="<?php echo $user_id;?>" />
      <input type="hidden" name="comp_id" id="comp_id" value="<?php echo $comp_id;?>" />
      	<input type="hidden" name="action_token" value="<?php echo $_SESSION['action_token']=$rand_id;?>" />
      <tr>
        <td width="19%" bgcolor="#CCCCCC">&nbsp;</td>
        <td width="81%" bgcolor="#CCCCCC">&nbsp;</td>
      </tr>
      </form>
      <?php } ?>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    
  </table>
  <?php } ?>
  <p>&nbsp; </p>
    <!-- end .content --></div>
<?php   require("footer.php"); 

  // INSERT INTO sky_history (`party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`,`original_band`,`original_action_date`,`direct_ticket_link`) values (?,?,?,?,now(),?,?,?,?,?,now(),?)  ON DUPLICATE KEY UPDATE party_id = values(party_id),category_id = values(category_id),promo_id = values(promo_id),promo_title = values(promo_title),action_date = now(),promo_date = values(promo_date),location= values(location),action = values(action),volume = values(volume),action_id=values(action_id) 
  ?>