<?php 
//
	require('../local_config.php');
	include(ROOT.'config/sky_connect.php');
	
	require("com_function.php");
	check_login();
	
	require(ROOT.'common/xss_safe.php');
	$xss = new xssSafe();
	
	require(ROOT.'common/db/DB_manager.php');
	$db = new DB_manager(HOST, DBU, DBPASS, DB);
	$db->set_table_prefix('sky_');
	$db->debug =1;
	
	$rand_num=mt_rand(); 
	$rand_id= str_shuffle(sha1('$sec12etk3yfor'.$rand_num));
	$errors = '';
	 
	 if(!empty($_SESSION['action_token']) && $_POST['action_token']==$_SESSION['action_token'])
	 {
		 $comp_id = $_POST['comp_id'];
		  if(empty($comp_id))
			$errors[] = 'Please select competition';
	 }
?><?php	
	require("header.php"); 
?>
<div class="content">
    <h1 style="padding-left:140px;">Competition Entries</h1>
    <?php if(!empty($errors)){?>
  <div style="background:#FFBFC1; color:#D70005;margin:0px 50px; padding:0px; 50px;">
    <li><?php echo implode('</li>
	<li>',$errors);?></li>
  </div>
    <?php } ?>
    <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1"><form action="comp_stat.php" method="post" name="frm_comp_stat" id="frm_comp_stat">
      <tr id="comp_list">
        <td bgcolor="#8090AB"><strong>Competition:*</strong></td>
        <td bgcolor="#93A5C4"><select name="comp_id" id="comp_id">
         <?php 
		 $comp='';
		 $rows = $db->from($table['competition'])->order_by('start_date','desc')->fetch(); 
		foreach($rows as $c)
		{
			

		 //$comp_sql = "select * from  order by start_date";
		 //$comp_result = mysql_query($comp_sql) or die(mysql_error());
		 //while($c = mysql_fetch_assoc($comp_result))
		// {
			 $selected = '';
			 
			 if($c['treat_id']==$comp_id)
			 {	$selected='selected';
			 	$comp=$c;
			 }
		 ?>
         <option value=<?php echo '"'.$c['treat_id'].'" '.$selected;?>><?php echo $xss->clean_input($c['promotion_title']);?></option>
         <?php } ?>
         </select></td>
      </tr>
	  <tr id="comp_search" class="hidden">
        <td width="18%" bgcolor="#8090AB">Search Competition:</td>
        <td width="82%" bgcolor="#93A5C4"><input name="search_word" id="search_word" value="" /></td>
      </tr>
      <tr>
        <td width="18%" bgcolor="#8090AB">&nbsp;</td>
        <td width="82%" bgcolor="#93A5C4"><a id="search_link" href="javascript:void(0);">Search Comp</a></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4"><input type="submit" value="Show Numbers" /></td>
      </tr>
     	<input type="hidden" name="action_token" value="<?php echo $_SESSION['action_token']=$rand_id;?>" />
      </form>
</table>
<p>&nbsp;</p>
<?php 
	if(!empty($comp_id)) 
	{?>
  <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1">
		<tr>
        <td colspan="7" bgcolor="#8090AB"><strong>Status for <?php echo $comp['promotion_title'];?></strong></td>
      </tr>
      <tr>
        <td width="14%" bgcolor="#CCCCCC"><strong>Start Date</strong></td>
        <td width="14%" bgcolor="#CCCCCC" colspan="2"><strong><?php echo $comp['start_date'];?></strong></td>
        <td width="14%" bgcolor="#CCCCCC"><strong>End Date</strong></td>
        <td width="14%" bgcolor="#CCCCCC" colspan="2"><strong><?php echo $comp['end_date'];?></strong></td>
				<td width="14%" bgcolor="#CCCCCC"><?php 
					echo 'Running for : <strong>'.date_diff_decimal($comp['end_date'],$comp['start_date']); ?> Days</strong> </td>
    </tr>
    <?php if($comp['require_upload']==1) {?>
    <tr>
    <td colspan=6>With Upload</td>
    </tr>
    <tr>
        <td width="14%" bgcolor="#8090AB"><strong>Unique Entries</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band A</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band B</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band C</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band D</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band E</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Total</strong></td>
    </tr>
      <?php 
		  
	  { ?>
      <tr>
        <td width="18%" bgcolor="#CCCCCC"><?php 
      $db->select('distinct(partyid)');
      
		  $db->from( $table['competition_data'] );
      $db->where("(email_read is NOT NULL and email_read !='')");
      $db->where('treat_id',$comp_id);
		  //$result = $db->fetch();
		  
		  $db->execute();
		  echo $db->affected_rows; 
		//	foreach($result as $row)
		//		echo $row['total'];
		$result = NULL;
		?></td>
        <?php 
	  	  $db->select('custband,COUNT(distinct(partyid)) as count');
      $db->from( $table['competition_data'] );
      $db->where("(email_read is NOT NULL and email_read !='')");
		  $db->where('treat_id',$comp_id);
		  $db->group_by('custband');
      $db->order_by('custband');
      $result = $db->fetch();
      var_dump($db);
      $band_counts = array('A'=>0,'B'=>0,'C'=>0,'D'=>0,'E'=>0);
		  $all_bands = array_keys($band_counts);
		  $total = 0;
		  foreach($result as $row)
		  	$band_counts[$row['custband']]=$row['count'];
		  foreach($band_counts as $b=>$count)
		  {
			  $total += ($count*$BAND_ENTRIES[$b]);
		?>
        <td width="13%" bgcolor="#CCCCCC"><?php echo $count;?></td>
        <?php } ?>
        <td width="13%" bgcolor="#CCCCCC">
        <?php
		  $db->select('DISTINCT partyid');
		  //$db->distinct('partyid');
      $db->from( $table['competition_data'] );
      $db->where("(email_read is NOT NULL and email_read !='')");
		  $db->where('treat_id',$comp_id);
		  $db->execute();
		  //echo $db->affected_rows;
		  echo $total;
		  //foreach($result as $row)
		  //	echo $row['total'];
		?>
        </td>
      </tr>
      <?php } ?>

    <tr>
    <td colspan=6>Without Upload</td>
    </tr>
    <tr>
        <td width="14%" bgcolor="#8090AB"><strong>Unique Entries</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band A</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band B</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band C</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band D</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band E</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Total</strong></td>
    </tr>
      <?php 
		  
	  { ?>
      <tr>
        <td width="18%" bgcolor="#CCCCCC"><?php 
      $db->select('distinct(partyid)');
      
		  $db->from( $table['competition_data'] );
      $db->where("(email_read is NULL or email_read='')");
      $db->where('treat_id',$comp_id);
		  //$result = $db->fetch();
		 
		  $db->execute();
		  echo $db->affected_rows; 
		//	foreach($result as $row)
		//		echo $row['total'];
		$result = NULL;
		?></td>
        <?php 
	  	  $db->select('custband,COUNT(distinct(partyid)) as count');
      $db->from( $table['competition_data'] );
      $db->where("(email_read is NULL or email_read='')");
		  $db->where('treat_id',$comp_id);
		  $db->group_by('custband');
		  $db->order_by('custband');
      $result = $db->fetch();
      $band_counts = array('A'=>0,'B'=>0,'C'=>0,'D'=>0,'E'=>0);
		  $all_bands = array_keys($band_counts);
		  $total = 0;
		  foreach($result as $row)
		  	$band_counts[$row['custband']]=$row['count'];
		  foreach($band_counts as $b=>$count)
		  {
			  $total += ($count*$BAND_ENTRIES[$b]);
		?>
        <td width="13%" bgcolor="#CCCCCC"><?php echo $count;?></td>
        <?php } ?>
        <td width="13%" bgcolor="#CCCCCC">
        <?php
		  $db->select('DISTINCT partyid');
		  //$db->distinct('partyid');
      $db->from( $table['competition_data'] );
      $db->where("(email_read is NULL or email_read='')");
		  $db->where('treat_id',$comp_id);
		  $db->execute();
		  //echo $db->affected_rows;
		  echo $total;
		  //foreach($result as $row)
		  //	echo $row['total'];
		?>
        </td>
      </tr>
      <?php } ?>

    <tr>
    <td colspan=6>Total  </td>
    </tr>

    <?php } ?>
      <tr>
        <td width="14%" bgcolor="#8090AB"><strong>Unique Entries</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band A</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band B</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band C</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band D</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Band E</strong></td>
        <td width="14%" bgcolor="#8090AB"><strong>Total</strong></td>
    </tr>
      <?php 
	    
	  { ?>
      <tr>
        <td width="18%" bgcolor="#CCCCCC"><?php 
		  $db->select('distinct(partyid)');
		  $db->from( $table['competition_data'] );
		  $db->where('treat_id',$comp_id);
		  //$result = $db->fetch();
		  
		  $db->execute();
		  echo $db->affected_rows; 
		//	foreach($result as $row)
		//		echo $row['total'];
		$result = NULL;
		?></td>
        <?php 
	  	  $db->select('custband,COUNT(distinct(partyid)) as count');
		  $db->from( $table['competition_data'] );
		  $db->where('treat_id',$comp_id);
		  $db->group_by('custband');
		  $db->order_by('custband');
		  $result = $db->fetch();
		  $band_counts = array('A'=>0,'B'=>0,'C'=>0,'D'=>0,'E'=>0);
		  $all_bands = array_keys($band_counts);
		  $total = 0;
		  foreach($result as $row)
		  	$band_counts[$row['custband']]=$row['count'];
		  foreach($band_counts as $b=>$count)
		  {
			  $total += ($count*$BAND_ENTRIES[$b]);
		?>
        <td width="13%" bgcolor="#CCCCCC"><?php echo $count;?></td>
        <?php } ?>
        <td width="13%" bgcolor="#CCCCCC">
        <?php
		  $db->select('DISTINCT partyid');
		  //$db->distinct('partyid');
		  $db->from( $table['competition_data'] );
		  $db->where('treat_id',$comp_id);
		  $db->execute();
		  //echo $db->affected_rows;
		  echo $total;
		  //foreach($result as $row)
		  //	echo $row['total'];
		?>
        </td>
      </tr>
      <?php } ?>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    
  </table>
  <?php } ?>
  <p>&nbsp; </p>
    <!-- end .content --></div>
<?php   require("footer.php"); 

  // INSERT INTO sky_history (`party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`,`original_band`,`original_action_date`,`direct_ticket_link`) values (?,?,?,?,now(),?,?,?,?,?,now(),?)  ON DUPLICATE KEY UPDATE party_id = values(party_id),category_id = values(category_id),promo_id = values(promo_id),promo_title = values(promo_title),action_date = now(),promo_date = values(promo_date),location= values(location),action = values(action),volume = values(volume),action_id=values(action_id) 
  ?>
  <script language="javascript">
         $('#search_link').on("click",  function(e)
        {
            $('#comp_list').toggle();
            $('#comp_search').toggle();
            $("#search_word").attr("autocomplete", "on");
        });
        $("#search_word").autocomplete({
                select: function( event, ui ) {
                    $("#comp_id").val(ui.item.id);
                   // $('form#frmflmlist').submit();
                },
                source: "<?php echo 'comp_search.php';?>",
                minLength: 3,
                delay: 1000
            });
</script>
