<?php 
	if(empty($msg['success']))
	{
		session_start();
		require('../local_config.php');
		include(ROOT.'config/foxtel_connect.php');
		
		require("com_function.php");
		check_login();
		
		require(ROOT.'common/xss_safe.php');
		$xss = new xssSafe();
		
		require(ROOT.'common/db/DB_manager.php');
		$db = new DB_manager(HOST, DBU, DBPASS, DB_HISTORY);
		$db->set_table_prefix($table['prefix']);
		$db->debug =1;
		
	}
	 
	if(!empty($_SESSION['action_token']) && $_POST['action_token']==$_SESSION['action_token'])
	{
		$group_id = $xss->clean_input($_POST['group_id']);

		if(empty($group_id))
		{
			header('Location:mark_winners.php');
			exit();
		}
		//$history = DB_HISTORY;
		//$data_db = DB;						
		$update_history  = "insert into `{$table['prefix']}{$table['history']}` (`party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`, `original_band`, `original_action_date`, `direct_ticket_link` )
		select `party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`, `original_band`, `original_action_date`, `direct_ticket_link` from  `{$table['prefix']}{$table['history_update']}` U
		where U.group_id='$group_id' and executed=0
		ON DUPLICATE KEY UPDATE action_date = U.`action_date`,action =U.action	";
		$db->query($update_history)->execute();	
		$msg['updated'] = ($db->affected_rows). "  added to history";


			 $update_history = " update `{$table['prefix']}{$table['history_update']}` U, `{$table['prefix']}{$table['history']}` H set  U.executed=1  WHERE U.executed=0 and U.group_id='$group_id' and H.action_id = U.action_id and H.party_id=U.party_id and H.promo_id=U.promo_id";
			$db->query($update_history)->execute();	
			$msg['update_count'] = $db->affected_rows. " record executed "; 
	}
?><?php	
	require("header.php"); 
?>
<div class="content">
    <h1 style="padding-left:140px;">Update History record (winners)</h1>
    <?php if(!empty($msg)){?>
  <div style="background:#FFBFC1; color:#D70005;margin:0px 50px; padding:0px; 50px;">
    <li><?php echo implode('</li>
	<li>',$msg);?></li>
  </div>
	<?php }
	if(empty($msg['updated']))
	{
	?>
    <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1">
	<form action="mark_winner_confirm.php" method="post" name="frm_random_winners" id="frm_random_winners">
   <tr>
        <td bgcolor="#8090AB">Confirm</td>
        <td bgcolor="#93A5C4">Please check the details about and then click confirm</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4"><input type="submit" value="Confirm to update history" />
		<input type="hidden" id="group_id" name="group_id" value="<?php echo $group_id;?>"/>
		</td>
      </tr>
     	<input type="hidden" name="action_token" value="<?php echo $_SESSION['action_token']=get_rand_id(10);?>" />
      </form>
</table>
	<?php } ?>
<p>&nbsp;</p>
  <p>&nbsp; </p>
    <!-- end .content --></div>
<?php   require("footer.php");  ?>