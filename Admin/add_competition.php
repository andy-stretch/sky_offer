<?php 
	session_start();
	require('../local_config.php');
	include(ROOT.'config/sky_connect.php');
	
	require("com_function.php");
	check_login();
	$u_token = $_POST['utoken'];
	$comp_id = $_POST['comp_id'];
	// dbConnect();
	$avl=1;
	
 	$msg = "";
	
	require(ROOT.'common/xss_safe.php');
	$xss = new xssSafe();
	
	require(ROOT.'common/db/DB_manager.php');
	$db = new DB_manager(HOST, DBU, DBPASS, DB);
	$db->set_table_prefix('sky_');
	
	if($_POST['add'] =="New")
	{
		unset($_SESSION['utoken']);
		if(empty($_POST['xcode']))
			$msg[] = "<li>Please enter Competition code";
		if(empty($_POST['promotion_title']))
			$msg[] = "<li>Invalid promotion title";
		
		if(empty($msg))
		{
			foreach($_POST['band_group'] as $key=>$val)
			{	
				$_POST['band_group'][$key] = $xss->clean_input($val);
			}
			$_POST['band_group'] = implode(',',$_POST['band_group']);
			$exclude_fields = array('terms','terms_full','email_content','band_group','question','answer_set','reminder_text');
			foreach($_POST as $key=>$val)
			{	
				if(!in_array($key,$exclude_fields))
					$post_input[$key]=$db->escape($xss->clean_input($val));
				else
					$post_input[$key]=$db->escape($val);
			}
			
			$year=date('Y');
			if(!is_dir("../content/$year"))
				mkdir("../content/$year");
			if($_FILES['header_image']['name'] !='')
			{
				if ($_FILES["header_image"]["size"] > UPLOAD_IMAGE_SIZE)
					$msg[] ='Header image file you are uploading is too big. It must be < '.(UPLOAD_IMAGE_SIZE/1000).' kb';
				elseif($_FILES["header_image"]["error"]==UPLOAD_ERR_OK)
				{
					$ext = explode(".", $_FILES['header_image']['name']);
					$post_input['header_image'] = $year."/".get_rand_id(12).'_'.time().".".array_pop($ext);
					if(UploadImage($_FILES["header_image"]["tmp_name"], "../content/".$post_input['header_image']))
						$_POST['header_image']="header_main.$ext";
					else	$msg[]="Error to move uploaded file, please check if this is valid image";
				}
				else
					$msg[]="Error to upload header image : ".$_FILES["pictures"]["error"];
			}
			if($_FILES['header_image_s']['name'] !='')
			{
				if ($_FILES["header_image_s"]["size"] > UPLOAD_IMAGE_SIZE)
					$msg[] ='Success header image file you are uploading is too big. It must be < '.(UPLOAD_IMAGE_SIZE/1000).' KB';
				elseif($_FILES["header_image_s"]["error"]==UPLOAD_ERR_OK)
				{
					$ext = explode(".", $_FILES['header_image_s']['name']);
					$post_input['header_image_s'] =   $year."/".get_rand_id(12).'_'.time().".".array_pop($ext);
					if(UploadImage($_FILES["header_image_s"]["tmp_name"], "../content/".$post_input['header_image_s']))
						$_POST['header_image_s']="header_main_S.$ext";
					else	$msg[]="Error to move uploaded file, please check if this is valid image";
				}
				else
					$msg[]="Error to upload header image : ".$_FILES["pictures"]["error"];
			}
			
			if($_FILES['tplus_header']['name'] !='')
			{
				if ($_FILES["tplus_header"]["size"] > UPLOAD_IMAGE_SIZE)
					$msg[] ='File must be less then '.(UPLOAD_IMAGE_SIZE/1000).' KB';
				elseif($_FILES["tplus_header"]["error"]==UPLOAD_ERR_OK)
				{
					$ext = explode(".", $_FILES['tplus_header']['name']);
					$post_input['tplus'] = $year."/".get_rand_id(12).'_'.time().".".array_pop($ext);
					if(UploadImage($_FILES["tplus_header"]["tmp_name"], "../content/".$post_input['tplus']))
						$_POST['tplus_header']="tplus.$ext";
					else	$msg[]="Error to move uploaded file (TPlus), please check if this is valid image";
				}
				else
					$msg[]="Error to upload Times+ header image : ".$_FILES["pictures"]["error"];
			}
			
			//$rows = $db->from($table['competition'])->where('id >',0)->order_by('start_date','desc')->fetch();
			if(empty($msg))
			{
				if(!isset($post_input['require_upload']))
				{
					$post_input['require_upload']=0;
					$post_input['upload_dir'] = '';
				}
				if(empty($post_input['reminder_email']))
				{
					$post_input['reminder_time']='0000-00-00';
					$post_input['reminder_email']=0;
					$post_input['reminder_text'] = '';
				}
        		$post_input['tags']=str_replace(',','|',$post_input['tags']);
				$competition_data = array(  'treat_id'=>$post_input['xcode'], 
										'promotion_title'=>$post_input['promotion_title'], 
										'sub_title'=>$post_input['sub_title'],
										'status'=>$post_input['status'],
										'sub_category'=>$post_input['sub_category'],
										'comingsoon'=>$post_input['comingsoon'], 
										'priority_date'=>$post_input['priority_date'], 
										'start_date'=>$post_input['stime'], 
										'end_date'=>$post_input['etime'],
										'drawtime'=>$post_input['drawtime'],
										'winners'=>$post_input['winners'],
										'runnersup'=>$post_input['runnersup'], 
										'image'=>$post_input['header_image'], 
										'image_success'=>$post_input['header_image_s'],
										'description'=>$post_input['description'], 
										'hourly'=>$post_input['hourly'],
										'show_address'=>$post_input['show_address'], 
										'opt_brand'=>$post_input['opt_brand'], 
										'optin'=>$post_input['optin'], 
										'terms'=>$post_input['terms'], 
										'terms_full'=>$post_input['terms_full'], 
										'analytics_id'=>$post_input['google_analytics'], 
										'own_table'=>$post_input['own_table'],
										'email_content'=>$post_input['email_content'],
										'band_group'=>$post_input['band_group'],
										'question'=>$post_input['question'],
                    					'answer_set'=>$post_input['answer_set']	,
                    					'answer_required'=>$post_input['answer_required'],
                    					'tags'=>$post_input['tags'],
                    					'require_upload'=>$post_input['require_upload'],
										'upload_dir'=>$post_input['upload_dir'],
					                    'game_file'=>$post_input['game_file'],
										'win_score'=>$post_input['win_score'],
										'game_message'=>$post_input['game_message'],
					                    'cooling_time'=>$post_input['cooling_time'],
					                    'reminder_email'=>$post_input['reminder_email'],
										  'reminder_time'=>$post_input['reminder_time'],
										  'reminder_text'=>$post_input['reminder_text']	
                    );
        
				$db->insert($table['competition'],$competition_data);
				
				if(!empty($_POST['own_table']) && !empty($_POST['own_table']) && !table_exists($_POST['own_table']))
					create_table($_POST['table']);
				$msg = 'Competition added successfully...';
				
				$admin_log_array = array('username'=>$_SESSION['adminuser'],'pagename'=>__FILE__,'action_title'=>'Competition Added','action_detail'=>serialize($competition_data),'datetime'=>$TODAY,'ip'=>$_SERVER['REMOTE_ADDR']); 
			log_action($admin_log_array);
			
			}
			else
				$msg = implode(" ",$msg);
		}
		else
			$msg = implode(" ",$msg);
	}

?><? require("header.php"); ?>
<link rel="stylesheet" media="screen" type="text/css" href="/common/css/colorpicker.css" />
<script type="text/javascript" src="/common/js/colorpicker.js"></script>
<link href="<?=('../../booking/css/jquery.tagsinput-revisited.css');?>" rel="stylesheet">
<script src="<?=('../../booking/js/jquery.tagsinput-revisited.js');?>"></script>


  <div class="content">
    <h1 style="padding-left:140px;">&nbsp;</h1>
    <table width="90%" border="0" align="center" cellpadding="2" cellspacing="1">
      <?
	  if($msg!="") {?>
      <tr>
        <td colspan="2" style="color:#FF0000;"><?=$msg;?></td>
        </tr>
       <? } 
	    { 
	   ?>
       <form action="add_competition.php" method="post" enctype="multipart/form-data" name="frm2" id="frm2">
       <tr>
         <td bgcolor="#8090AB">Name of Competition</td>
         <td bgcolor="#93A5C4"><input name="promotion_title" type="text" id="promotion_title" size="45" value="<?php echo $xss->clean_input($post_input['promotion_title']);?>" /></td>
       </tr>
       <tr>
         <td width="23%" bgcolor="#8090AB">Competitions Code</td>
         <td width="77%" bgcolor="#93A5C4"><input name="xcode" type="text" id="xcode" value="<?php echo $xss->clean_input($post_input['xcode']);?>" size="25" /></td>
       </tr>
      <tr>
        <td bgcolor="#8090AB">Promotion Subtitle</td>
        <td bgcolor="#93A5C4"><textarea name="sub_title" class="ckeditor"  id="sub_title"><?php echo $xss->clean_input($post_input['sub_title']);?></textarea></td>
        </tr>
        <tr>
        <td bgcolor="#8090AB">Access to Tiers</td>
        <td bgcolor="#93A5C4"><?php
		$comp_bands = explode(',',$_POST['band_group']); 
        foreach($BANDS as $band)
		{
			$checked = '';
			if(in_array($band,$comp_bands) || empty($_POST['band_group']))
				$checked = 'checked';
			echo '<input name="band_group[]" id="band_group" type="checkbox" '.$checked.' value="'.$band.'"> '.$band.'  &nbsp;' ;
		}
		?></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
        </tr>
     
      <tr>
        <td bgcolor="#8090AB">Auto Redirect competition (NO)</td>
        <td bgcolor="#93A5C4"><label>
          <input type="radio" value="1" name="hourly" id="hourly" <? if($post_input['hourly']==1) echo 'checked';?> />
          Yes </label>
&nbsp; &nbsp;
<label>
  <input type="radio" value="0" name="hourly" id="hourly" <? if($post_input['hourly']==0) echo 'checked';?> />
  No</label></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Redirect URL</td>
        <td bgcolor="#93A5C4"><input name="own_table" type="text" id="own_table" size="45" value="<?php echo $xss->clean_input($post_input['own_table']);?>" />
        <br>
        https://www.skyticketit.com/sky/offer/comp/@partyid@/@custband@/@offerchecksum@/[TREAT_ID]<br>
https://www.skyticketit.com/sky/athome/code/@partyid@/@custband@/@offerchecksum@/[TREAT_ID]<br>
https://www.skyticketit.com/sky/athomev2/code/@partyid@/@custband@/@offerchecksum@/[TREAT_ID]<br>
https://www.skyticketit.com/sky/promo/code/@partyid@/@custband@/@offerchecksum@/[TREAT_ID]</td>
        </tr>
        <?
		// if(!table_exists($table))
		
		{
		?>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4"><label>
          <input type="checkbox" name="create_table" id="create_table" value="yes" />Create table if not exists.</label></td>
        </tr>
       <? } ?>
       
        <tr>
        	<td bgcolor="#8090AB">Show Upload Fields</td>
        	<td bgcolor="#93A5C4"><label>
        		<input type="radio" value="1" name="require_upload" id="require_upload" <? if($comp['require_upload']==1) echo 'checked';?> />
        		Yes </label>
	&nbsp; &nbsp;
	<label>
		<input type="radio" value="0" name="require_upload" id="require_upload2" <? if($comp['require_upload']==0) echo 'checked';?> />
		No</label></td>
        	</tr>
        <tr>
        	<td bgcolor="#8090AB">Upload Dir</td>
        	<td bgcolor="#93A5C4"><input name="upload_dir" type="text" id="upload_dir" value="<?php echo $xss->clean_input($comp['upload_dir']);?>" size="25" /></td>
        	</tr>
        <tr>
        <td bgcolor="#8090AB">Show Address Fields</td>
        <td bgcolor="#93A5C4"><label>
          <input type="radio" value="1" name="show_address" id="show_address" <? if($post_input['show_address']==1) echo 'checked';?> />
          Yes </label>
&nbsp; &nbsp;
<label>
  <input type="radio" value="0" name="show_address" id="show_address" <? if($post_input['show_address']==0) echo 'checked';?> />
  No</label></td>
      </tr>
      
       <tr>
         <td bgcolor="#8090AB">Status :</td>
         <td bgcolor="#93A5C4"><label>
          <input type="radio" value="live" name="status" id="status" <? if($post_input['status']=='live') echo 'checked';?> />
          Live </label>
&nbsp; &nbsp;
<label>
  <input type="radio" value="preview" name="status" id="status" <? if($post_input['status']=='preview' || empty($post_input['status'])) echo 'checked';?> />
  Preview</label></td>
       </tr>
       <tr>
		<td bgcolor="#8090AB">Category :</td>
		<td bgcolor="#93A5C4"><label>
			<?php
			foreach($SUB_CATEGORY as $category){
			?>
			<input type="radio" value="<?php echo $category;?>" name="sub_category" <? if($comp['sub_category']==$category) echo 'checked';?> />
			<?php echo $category;?> </label>
			&nbsp; &nbsp;
			<?php
			}
			?>
		</td>
		</tr>
       <tr>
         <td bgcolor="#8090AB">Coming soon Date:</td>
         <td bgcolor="#93A5C4"><input name="comingsoon" type="text" id="comingsoon" value="<?php echo $xss->clean_input($post_input['comingsoon']);?>" size="25" />
           <input type="button" value="..." id="btn_comingsoon" /></td>
       </tr>
       <tr>
         <td bgcolor="#8090AB">Priority Date:</td>
         <td bgcolor="#93A5C4"><input name="priority_date" type="text" id="priority_date" value="<?php echo $xss->clean_input($post_input['priority_date']);?>" size="25" />
           <input type="button" value="..." id="btn_priority" /></td>
       </tr>
       <tr>
         <td bgcolor="#8090AB">Start Time:</td>
         <td bgcolor="#93A5C4"><input name="stime" type="text" id="stime" value="<?php echo $xss->clean_input($post_input['start_date']);?>" size="25" /> <input type="button" value="..." id="btn_from" /></td>
       </tr>
       <tr>
         <td bgcolor="#8090AB">End Time:</td>
         <td bgcolor="#93A5C4"><input name="etime" type="text" id="etime" value="<?php echo $xss->clean_input($post_input['end_date']);?>" size="25" /> <input type="button" value="..."  id="btn_to" /></td>
       </tr>
        
       	<tr>
       		<td bgcolor="#8090AB">Draw Time (non functional):</td>
       		<td bgcolor="#93A5C4"><input name="drawtime" type="text" id="drawtime" value="<?php echo $xss->clean_input($post_input['drawtime']);?>" size="25" />
       			<input type="button" value="..." id="btn_draw" /></td>
       		</tr>
		<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("btn_from", "stime", "%Y-%m-%d %H:%M");
      cal.manageFields("btn_to", "etime", "%Y-%m-%d %H:%M");
	  cal.manageFields("btn_draw", "drawtime", "%Y-%m-%d %H:%M");
	   cal.manageFields("btn_comingsoon", "comingsoon", "%Y-%m-%d %H:%M");
	    cal.manageFields("btn_priority", "priority_date", "%Y-%m-%d %H:%M");
    //]]></script>
       	<tr>
       		<td bgcolor="#8090AB">Winners (non functional):</td>
       		<td bgcolor="#93A5C4"><input name="winners" type="text" id="winners" value="<?php echo $xss->clean_input($post_input['winners']);?>" size="25" /></td>
       		</tr>
       	<tr>
       		<td bgcolor="#8090AB">Runnersup (non functional)</td>
       		<td bgcolor="#93A5C4"><input name="runnersup" type="text" id="runnersup" value="<?php echo $xss->clean_input($post_input['runnersup']);?>" size="25" /></td>
       		</tr>
       	<tr>
         <td bgcolor="#8090AB">&nbsp;</td>
         <td bgcolor="#93A5C4">&nbsp;</td>
       </tr>
      <tr>
        <td bgcolor="#8090AB">Opt In (add [TICK] for tickbox)</td>
        <td bgcolor="#93A5C4"><textarea rows="5" cols="50" name="optin" type="text" id="optin" ><?php echo $xss->clean_input($post_input['optin']);?></textarea></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Terms And Conditions</td>
        <td bgcolor="#93A5C4"><textarea name="terms" class="ckeditor"  id="terms"><?php echo $xss->clean_input($post_input['terms']);?></textarea>
          <br />
          Allowed tags are [CLOSING_DATE], [START_DATE], [DRAW DATE], [NUMBER_WINNERS]</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Terms And Conditions (can leave blank)</td>
        <td bgcolor="#93A5C4"><textarea name="terms_full" class="ckeditor"  id="terms_full"><?php echo $xss->clean_input($post_input['terms_full']);?></textarea>
          <br />
          Allowed tags are [CLOSING_DATE], [LIVE_DATE], [DRAW DATE], [NUMBER_WINNERS]</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
        </tr>
      <tr>
        <td bgcolor="#8090AB">Email Include (only add if you want to include additional messaging in  bounceback email)</td>
        <td bgcolor="#93A5C4"><textarea name="email_content" class="ckeditor"  id="email_content"><?php echo $xss->clean_input($post_input['email_content']);?></textarea>
		  <br>
[YOUTUBE],[BOOKING_ID],[FILM_NAME],[IMAGE],[USER_EMAIL],[UNIQUE_DATE],[UNIQUE_TIME],[BRAND],[TERMS],[FNAME],[LNAME],[ACTION_ID]
		  </td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Main Image</td>
        <td bgcolor="#93A5C4"><input name="header_image" type="file" id="header_image" />
          <? if(!empty($post_input['image'])){ ?> 
          <a target="_blank" href="../content/<?php echo $xss->clean_input($post_input['image']);?>">View Current Header</a>
          <? } ?>
          </td>
      </tr>
      <tr>
      	<td bgcolor="#8090AB">Success page<br />
      		Header 
      		Image (leave blank)</td>
      	<td bgcolor="#93A5C4"><input name="header_image_s" type="file" id="header_image_s" />
      		<? if(!empty($post_input['image_success'])){ ?>
      		<a target="_blank" href="../content/<?php echo $xss->clean_input($post_input['image_success']);?>">View Current Header</a>
      		<? } ?></td>
      </tr>
       
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      
      <tr>
        <td bgcolor="#8090AB">Competition Question (if required)</td>
        <td bgcolor="#93A5C4"><textarea name="question" cols="50" id="question"><?php echo $xss->clean_input($post_input['question']);?></textarea></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Answers (blank for freetext)</td>
        <td bgcolor="#93A5C4"><textarea name="answer_set" cols="50"  id="answer_set"><?php echo $xss->clean_input($post_input['answer_set']);?></textarea>
          <br />
          Example: Answer1 | Answer 2 | Answer 3
          <br />Example: Answer1 && Answer 2 && Answer 3</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Required</td>
        <td bgcolor="#93A5C4"><label>
          <input type="radio" name="answer_required" id="answer_required0" value="yes" <?php if($post_input['answer_required']=='yes') echo 'checked';?>  >
          Yes </label>
&nbsp; &nbsp;
<label>
  <input type="radio" name="answer_required" id="answer_required1" value="no" <?php if($post_input['answer_required']=='no') echo 'checked';?> >
  No</label></td>
      	</tr>

      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Youtube URL : </td>
        <td bgcolor="#93A5C4"><input type="text" name="google_analytics" id="google_analytics" value="<?php echo $xss->clean_input($post_input['analytics_id']);?>" /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Tags : </td>
        <td bgcolor="#FFFFFF"><input type="text" name="tags" id="tags" value="<?php $tags = $post_input['tags']; echo str_replace('|',',',$tags); ?>" /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Game File: </td>
        <td bgcolor="#93A5C4"><input type="text" name="game_file" id="game_file" value="<?php echo $xss->clean_input($post_input['game_file']);?>" /></td>
      </tr>
      <tr>
      	<td bgcolor="#8090AB">Winning Score : </td>
      	<td bgcolor="#FFFFFF"><input type="text" name="win_score" id="win_score" value="<?php echo $xss->clean_input($post_input['win_score']);?>" /></td>
      	</tr>
      <tr>
      	<td bgcolor="#8090AB">Game Message</td>
      	<td bgcolor="#93A5C4"><textarea name="game_message" class="ckeditor"  id="game_message"><?php echo $xss->clean_input($post_input['game_message']);?></textarea>
      		<br />
      		Allowed tags are [SCORE], [TOTAL_SCORE]</td>
      	</tr>
      <tr>
      	<td bgcolor="#8090AB">Cooling period before retry: </td>
      	<td bgcolor="#93A5C4"><input type="text" name="cooling_time" id="cooking_time" value="<?php echo $xss->clean_input($post_input['cooling_time']);?>" /></td>
      	</tr>
      	<tr>
      	<td bgcolor="#8090AB">Reminder email</td>
      	<td bgcolor="#93A5C4"><label>
      		<input type="radio" value="on" name="reminder_email" id="reminder_email" <? if($post_input['reminder_email']=='on') echo 'checked';?> />ON </label>
      		&nbsp; &nbsp;
      		<label>
      			<input type="radio" value="off" name="reminder_email" id="reminder_email2" <? if($post_input['show_address']=='off') echo 'checked';?> />	OFF </label></td>
      	</tr>
      <tr>
      	<td bgcolor="#8090AB">Reminder time</td>
      	<td bgcolor="#93A5C4"><input name="reminder_time" type="text" id="reminder_time" value="<?php echo $xss->clean_input($post_input['reminder_time']);?>" size="25" />
      		<input type="button" value="..." id="btn_rtime" /></td>
      	</tr>
      <tr>
      	<td bgcolor="#8090AB">Reminder Template</td>
      	<td bgcolor="#93A5C4"><textarea name="reminder_text" class="ckeditor"  id="reminder_text"><?php echo $xss->clean_input($post_input['reminder_text']);?></textarea>
      		<br />
      		Allowed tags are [FIRST_NAME], [ISSUE_CODE] </td>
      	</tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4"><input name="button1" type="submit" id="button1" value="Update Settings" /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <input type="hidden" name="add" value="New" />
      <input type="hidden" name="utoken" id="utoken" value="<?php echo $utoken;?>" />
      <input type="hidden" name="comp_id" id="comp_id" value="<?php echo $xss->clean_input($comp_id);?>" />
      </form>
		<?php } 
		$_SESSION['utoken']=$utoken;
		?>
    </table>
    <p>&nbsp; </p>
    <!-- end .content --></div>
  <? require("footer.php"); ?>
  <script language="javascript">
  $('#header_bg_color').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
		$(el).val('#'+hex);
		$(el).ColorPickerHide();
	},
	onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	}
})
.bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
});

      ClassicEditor
      .create( document.querySelector( '#sub_title' ) )
      .then( editor => {
          console.log( editor );
      } )
      .catch( error => {
          console.error( error );
      } );

      ClassicEditor
      .create( document.querySelector( '#terms' ) )
      .then( editor => {
          console.log( editor );
      } )
      .catch( error => {
          console.error( error );
      } );

      ClassicEditor
      .create( document.querySelector( '#terms_full' ) )
      .then( editor => {
          console.log( editor );
      } )
      .catch( error => {
          console.error( error );
      } );

      ClassicEditor
      .create( document.querySelector( '#email_content' ) )
      .then( editor => {
          console.log( editor );
      } )
      .catch( error => {
          console.error( error );
      } );
		ClassicEditor
      .create( document.querySelector( '#reminder_text' ) )
      .then( editor => {
          console.log( editor );
      } )
      .catch( error => {
          console.error( error );
      } );
      $('#tags').tagsInput();
  </script>
