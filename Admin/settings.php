<? 
	session_start();
	require('../local_config.php');
	include(ROOT.'config/connect.php');
	
	require("com_function.php");
	check_login();
	
	dbConnect();
	$avl=1;
	
 	$msg = "";
	
	if($_POST['UpDa'] =="Te")
	{
		if(empty($_POST['table'])) 
			$msg[] = "<li>Please enter Table 
			name";
		if(empty($_POST['Code']))
			$msg[] = "<li>Please enter Competition code";
		
		
		if(empty($msg))
		{
			if($_FILES['header_image']['name'] !='')
			{
				if ($_FILES["header_image"]["size"] > 1024000)
					$msg ='File must be less then 1MB';
				elseif($_FILES["header_image"]["error"]==UPLOAD_ERR_OK)
				{
					$ext = explode(".", $_FILES['header_image']['name']);
					$ext = array_pop($ext);
					if(UploadImage($_FILES["header_image"]["tmp_name"], "../content/header_main.".$ext))
						$_POST['header_image']="header_main.$ext";
					else	$msg="Error to move uploaded file, please check if this is valid image";
				}
				else
					$msg="Error to upload header image : ".$_FILES["pictures"]["error"];
			}
			if($_FILES['header_image_s']['name'] !='')
			{
				if ($_FILES["header_image_s"]["size"] > 1024000)
					$msg ='File must be less then 1MB';
				elseif($_FILES["header_image_s"]["error"]==UPLOAD_ERR_OK)
				{
					$ext = explode(".", $_FILES['header_image_s']['name']);
					$ext = array_pop($ext);
					if(UploadImage($_FILES["header_image_s"]["tmp_name"], "../content/header_main_S.".$ext))
						$_POST['header_image_s']="header_main_S.$ext";
					else	$msg="Error to move uploaded file, please check if this is valid image";
				}
				else
					$msg="Error to upload header image : ".$_FILES["pictures"]["error"];
			}
			
			if($_FILES['tplus_header']['name'] !='')
			{
				if ($_FILES["tplus_header"]["size"] > 1024000)
					$msg ='File must be less then 1MB';
				elseif($_FILES["tplus_header"]["error"]==UPLOAD_ERR_OK)
				{
					$ext = explode(".", $_FILES['tplus_header']['name']);
					$ext = array_pop($ext);
					if(UploadImage($_FILES["tplus_header"]["tmp_name"], "../content/tplus.".$ext))
						$_POST['tplus_header']="tplus.$ext";
					else	$msg="Error to move uploaded file (TPlus), please check if this is valid image";
				}
				else
					$msg="Error to upload Times+ header image : ".$_FILES["pictures"]["error"];
			}
			
			$fp = fopen("../content/gb.php","w");
			fwrite($fp,'<?php');
			$exclude = array("create_table","button1","UpDa");
			
			$editor = array("terms","terms_full","email_content");
			
			foreach($_POST as $ke => $val)
			{
				
				if(@in_array($ke ,$exclude))
					continue;
				if(!in_array($ke ,$editor))
					$val = strip_tags($val,'<br>,<strong>,<p>,<div>,<br />,<b>');	
				$val = stripslashes($val);
				
				$tmp_string = "	
		\$$ke  = '". str_replace("'","\'",$val)."';";
				fwrite($fp,$tmp_string);
			}
			fclose($fp);
			
			if(!empty($_POST['create_table']) && !empty($_POST['table']) && !table_exists($_POST['table']))
			{
				create_table($_POST['table']);
			}
			
		}
		else
			$msg = implode(" ",$msg);
	}

?><? require("header.php"); ?>
<link rel="stylesheet" media="screen" type="text/css" href="/common/css/colorpicker.css" />
<script type="text/javascript" src="/common/js/colorpicker.js"></script>

  <div class="content">
    <h1 style="padding-left:140px;">
    <? 
	$file_permission =is_writable("../content/gb.php");
	if(!$file_permission) { 
	?>
	Permission Error. Please contact webteam@stretchcomms.com

<? } 
?>
</h1>
    <? if($file_permission)  { 
	require("../content/gb.php");
	?>
    <table width="90%" border="0" align="center" cellpadding="2" cellspacing="1">
      <form action="settings.php" method="post" enctype="multipart/form-data" name="frm2" id="frm2">
      <?
	  if($msg!="") {?>
      <tr>
        <td colspan="2" style="color:#FF0000;"><?=$msg;?></td>
        </tr>
       <? } ?>
       <tr>
         <td bgcolor="#8090AB">Name of Competition</td>
         <td bgcolor="#93A5C4"><input name="name_of_competition" type="text" id="name_of_competition" size="45" value="<?=stripslashes(htmlspecialchars($name_of_competition)) ;?>" /></td>
       </tr>
       <tr>
        <td width="23%" bgcolor="#8090AB">Competitions Code</td>
        <td width="77%" bgcolor="#93A5C4"><input name="Code" type="text" id="Code" value="<?=stripslashes(htmlspecialchars($Code));?>" size="25" /></td>
        </tr>
      <tr>
        <td bgcolor="#8090AB">Page Title</td>
        <td bgcolor="#93A5C4"><input name="page_title" type="text" id="page_title" size="45" value="<?=stripslashes(htmlspecialchars($page_title));?>" /></td>
        </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
        </tr>
     
      <tr>
        <td bgcolor="#8090AB">Hourly competition</td>
        <td bgcolor="#93A5C4"><label>
          <input type="radio" value="1" name="is_hourly_comp" id="is_hourly_comp" <? if($is_hourly_comp==1) echo 'checked';?> />
          Yes </label>
&nbsp; &nbsp;
<label>
  <input type="radio" value="0" name="is_hourly_comp" id="is_hourly_comp" <? if($is_hourly_comp==0) echo 'checked';?> />
  No</label></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Database Table</td>
        <td bgcolor="#93A5C4"><input name="table" type="text" id="table" size="45" value="<?=stripslashes(htmlspecialchars($table));?>" /></td>
        </tr>
        <?
		// if(!table_exists($table))
		
		{
		?>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4"><label>
          <input type="checkbox" name="create_table" id="create_table" value="yes" />
          Create table if not exists.</label></td>
        </tr>
       <? } ?>
       
        <tr>
        <td bgcolor="#8090AB">Show Address Fields</td>
        <td bgcolor="#93A5C4"><label>
          <input type="radio" value="1" name="show_address" id="show_address" <? if($show_address==1) echo 'checked';?> />
          Yes </label>
&nbsp; &nbsp;
<label>
  <input type="radio" value="0" name="show_address" id="show_address" <? if($show_address==0) echo 'checked';?> />
  No</label></td>
      </tr>
      
       <tr>
         <td bgcolor="#8090AB">&nbsp;</td>
         <td bgcolor="#93A5C4">&nbsp;</td>
       </tr>
       <tr>
         <td bgcolor="#8090AB">Start Time:</td>
         <td bgcolor="#93A5C4"><input name="stime" type="text" id="stime" value="<?=stripslashes(htmlspecialchars($stime));?>" size="25" /> <input type="button" value="..." id="btn_from" /></td>
       </tr>
       <tr>
         <td bgcolor="#8090AB">End Time:</td>
         <td bgcolor="#93A5C4"><input name="etime" type="text" id="etime" value="<?=stripslashes(htmlspecialchars($etime));?>" size="25" /> <input type="button" value="..."  id="btn_to" /></td>
       </tr>
        <script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("btn_from", "stime", "%Y-%m-%d %H:%M");
      cal.manageFields("btn_to", "etime", "%Y-%m-%d %H:%M");
	  
    //]]></script>
       <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Tplus Member Link</td>
        <td bgcolor="#93A5C4">
        	<label><input type="radio" value="1" name="tplus" id="tplus" <? if($tplus==1) echo 'checked';?> /> Yes </label> &nbsp; &nbsp;
            <label><input type="radio" value="0" name="tplus" id="tplus" <? if($tplus==0) echo 'checked';?> /> No</label> 
        </td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Tplus Link</td>
        <td bgcolor="#93A5C4"><input name="tplus_link" type="text" id="tplus_link" value="<?=stripslashes(htmlspecialchars($tplus_link));?>" size="45"  /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Opt In Live</td>
        <td bgcolor="#93A5C4"><label>
          <input type="radio" name="optin_live" id="optin_live" value="1" <? if($optin_live==1) echo 'checked';?> />
          Yes </label>
&nbsp; &nbsp;
<label>
  <input type="radio" name="optin_live" id="optin_live" value="0" <? if($optin_live==0) echo 'checked';?> />
  No</label></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Opt In Brand</td>
        <td bgcolor="#93A5C4"><input name="optin" type="text" id="optin" size="45" value="<?=stripslashes(htmlspecialchars($optin));?>" /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Brand Privacy Policy Link</td>
        <td bgcolor="#93A5C4"><input name="brand_pplink" type="text" id="brand_pplink" value="<?=stripslashes(htmlspecialchars($brand_pplink));?>" size="45"  /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Terms And Conditions</td>
        <td bgcolor="#93A5C4"><textarea name="terms" class="ckeditor"  id="terms"><?=stripslashes(htmlspecialchars($terms));?>
        </textarea></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Terms And Conditions FULL</td>
        <td bgcolor="#93A5C4"><textarea name="terms_full" class="ckeditor"  id="terms_full"><?=stripslashes(htmlspecialchars($terms_full));?></textarea></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
        </tr>
      <tr>
        <td bgcolor="#8090AB">Email Template</td>
        <td bgcolor="#93A5C4"><textarea name="email_content" class="ckeditor"  id="email_content"><?=stripslashes(htmlspecialchars($email_content));?></textarea></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Header Image</td>
        <td bgcolor="#93A5C4"><input name="header_image" type="file" id="header_image" />
          <? if(!empty($header_image)){ ?> 
          <a target="_blank" href="../content/<?=htmlspecialchars(strip_tags($header_image));?>">View Current Header</a>
          <? } ?>
          </td>
      </tr>
      <tr>
      	<td bgcolor="#8090AB">Success page<br />
      		Header 
      		Image</td>
      	<td bgcolor="#93A5C4"><input name="header_image_s" type="file" id="header_image_s" />
      		<? if(!empty($header_image_s)){ ?>
      		<a target="_blank" href="../content/<?=htmlspecialchars(strip_tags($header_image_s));?>">View Current Header</a>
      		<? } ?></td>
      	</tr>
      <tr>
      	<td bgcolor="#8090AB">Times+ Banner</td>
      	<td bgcolor="#93A5C4"><input name="tplus_header" type="file" id="tplus_header" />
      		<? if(!empty($tplus_header)){ ?>
      		<a target="_blank" href="../content/<?=htmlspecialchars(strip_tags($tplus_header));?>">View Current Header</a>
      		<? } ?></td>
      	</tr>
      <tr>
      	<td bgcolor="#8090AB">Header Background Color</td>
      	<td bgcolor="#93A5C4"><input name="header_bg_color" class="color" type="text" id="header_bg_color" value="<?=stripslashes(htmlspecialchars($header_bg_color));?>" /></td>
      	</tr>
      <tr>
        <td bgcolor="#8090AB">Header Large Text</td>
        <td bgcolor="#93A5C4"><textarea name="header_large_text" id="header_large_text"><?=stripslashes(htmlspecialchars($header_large_text));?></textarea></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Header Small Text</td>
        <td bgcolor="#93A5C4"><textarea name="header_small_text" id="header_small_text"><?=stripslashes(htmlspecialchars($header_small_text));?></textarea></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">Analytics Code : </td>
        <td bgcolor="#93A5C4"><input type="text" name="google_analytics" id="google_analytics" value="<?=stripslashes(htmlspecialchars($google_analytics));?>" /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4"><input name="button1" type="submit" id="button1" value="Update Settings" /></td>
      </tr>
      <tr>
        <td bgcolor="#8090AB">&nbsp;</td>
        <td bgcolor="#93A5C4">&nbsp;</td>
      </tr>
      <input type="hidden" name="UpDa" value="Te" />
      <input type="hidden" name="header_image" value="<?=htmlspecialchars(strip_tags($header_image));?>" />
      <input type="hidden" name="header_image_s" value="<?=htmlspecialchars(strip_tags($header_image));?>" />
      <input type="hidden" name="tplus_header" value="<?=htmlspecialchars(strip_tags($tplus_header));?>" />
      </form>
    </table>
    <? } ?>
    <p>&nbsp; </p>
    <!-- end .content --></div>
  <? require("footer.php"); ?>
  <script language="javascript">
  $('#header_bg_color').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
		$(el).val('#'+hex);
		$(el).ColorPickerHide();
	},
	onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	}
})
.bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
});
  </script>