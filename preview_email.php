<?  
	require('local_config.php');
	require(ROOT.'config/sky_connect.php');
	require(ROOT.'common/db/DB_manager.php');
	
	require("Admin/com_function.php");
//	check_login();
	$db = new DB_manager(HOST, DBU, DBPASS, DB);
	$db->set_table_prefix('sky_');
	$db->debug=1;
	
	$booking_rand=mt_rand();
	
	require(ROOT . 'common/xss_safe.php');
	$xss       = new xssSafe();

	$custband  = $xss->clean_input($_REQUEST['custband']);
	$promoid   = $xss->clean_input($_REQUEST['promoid']);
	$band_color = $BAND_COLORS[$custband];
	$user_name='First  Last';
	$lname ='Last';
	
	$comp = $db->from($table['competition'])->where('treat_id',$promoid)->fetch_first(); 
		$comp['answer_set'] = trim($comp['answer_set']);
		$band_entries_text = $BAND_ENTRIES_DISPLAY[$custband];
		if(!empty($comp['question']) && empty($comp['answer_set']))
			$band_entries_text = $BAND_ENTRIES_DISPLAY_COMP[$custband];
			
			$comp_type="prize draw";
			if(!empty($comp['question']))
				$comp_type = "competition";
			  $confirm_page_text = "Thank you for entering our ".$comp_type.": &quot;".$comp['promotion_title']."&quot;.".$band_entries_text."<br><br>We'll let you know by email if you're a winner.";
			
			$term_tags = array('[CLOSING_DATE]','[START_DATE]','[DRAW DATE]','[NUMBER_WINNERS]','[NOTIFICATION_DATE]');
	$replace_tags = array(date('H:i \o\n j F, Y',strtotime($comp['end_date'])),date('H:i \o\n j F, Y',strtotime($comp['start_date'])),date('H:i \o\n j F, Y',strtotime($comp['drawtime'])),$comp['winners'],date('d F, Y',strtotime($comp['drawtime'])+(7*60*60*24)));		
			$comp['terms']=str_replace($term_tags,$replace_tags, $comp['terms']);
			 $band_color = $BAND_COLORS[$custband];

			$email_constants = array('[YOUTUBE]','[BOOKING_ID]','[FILM_NAME]','[IMAGE]','[USER_EMAIL]','[UNIQUE_DATE]','[UNIQUE_TIME]','[BRAND]','[TERMS]','[FNAME]','[LNAME]');

			$email_constants_value = array($booking_rand,

			$comp['promotion_title'],

			SITE_URL.$comp['image'],
			$email,
			date('d-m-Y',strtotime($comp['start_date'])),
			date('H:ia',strtotime($comp['start_date'])),
			$comp['brand'],
			$comp['terms'],
			$fname,
			$lname
			);

			 $email_content=  str_replace($email_constants,$email_constants_value,$comp['email_content']);
			
			
			$term_tags = array('[CLOSING_DATE]','[START_DATE]','[DRAW DATE]','[NUMBER_WINNERS]','[NOTIFICATION_DATE]');
	$replace_tags = array(date('H:i \o\n j F, Y',strtotime($comp['end_date'])),date('H:i \o\n j F, Y',strtotime($comp['start_date'])),date('H:i \o\n j F, Y',strtotime($comp['drawtime'])),$comp['winners'],date('d F, Y',strtotime($comp['drawtime'])+(7*60*60*24)));
	$comp['terms']=str_replace($term_tags,$replace_tags, $comp['terms']);
			
			$band_color = $BAND_COLORS[$custband];

			$email_constants = array('[YOUTUBE]','[BOOKING_ID]','[FILM_NAME]','[IMAGE]','[USER_EMAIL]','[UNIQUE_DATE]','[UNIQUE_TIME]','[BRAND]','[TERMS]','[FNAME]','[LNAME]');

			$email_constants_value = array($booking_rand,

			$comp['promotion_title'],

			SITE_URL.$comp['image'],
			$email,
			date('d-m-Y',strtotime($comp['start_date'])),
			date('H:ia',strtotime($comp['start_date'])),
			$comp['brand'],
			$comp['terms'],
			$fname,
			$lname
			);

			 $email_content=  str_replace($email_constants,$email_constants_value,$comp['email_content']);
			 
 require('email/header.php');
 require('email/footer.php');
 $email_header = str_replace('[ETYPE]',$comp_type,$email_header);
$email_signoff="
<br><br>
Your Sky VIP Team";
 $from="noreply-competitions@mp-staging.co.uk";
 $subject = "$fname, thanks for entering and good luck!";
 
 echo  $email_header.$confirm_page_text.$email_content.$email_signoff.$email_footer;
	
//  unset($_SESSION['title'],$_SESSION['lname'],$_SESSION['fname'],$_SESSION['email'],$_SESSION['requested_tickets'],$_SESSION['booking_rand'],$_SESSION['checksum']);


