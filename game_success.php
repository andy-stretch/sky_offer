<?php
require('local_config.php');
require(ROOT . 'config/sky_connect.php');
$track_pagename = 'compdetail';
$already_entered = false;
$errors          = array();
require(ROOT . 'common/xss_safe.php');
require(ROOT . 'common/db/DB_manager.php');
$db = new DB_manager(HOST, DBU, DBPASS, DB);
$db->set_table_prefix('sky_');
$db->debug = 0;

$comp = '';

$xss       = new xssSafe();
$checksum  = $_REQUEST['checksum'];
$promoid   = $xss->clean_input($_POST['promoid']);
$partyid   = $xss->clean_input($_REQUEST['partyid']);
$custband  = $xss->clean_input($_REQUEST['custband']);
$score   = $xss->clean_input($_POST['score']);
$user_token   = $xss->clean_input($_POST['user_token']);

if (empty($custband)) {
	if (!empty($_SESSION['custband']))
		$custband = $_SESSION['custband'];
} //empty($custband)
else
	$_SESSION['custband'] = $custband;
if (empty($partyid)) {
	if (!empty($_SESSION['partyid']))
		$partyid = $_SESSION['partyid'];
} //empty($partyid)
else
	$_SESSION['partyid'] = $partyid;
if (empty($checksum)) {
	if (!empty($_SESSION['checksum']))
		$checksum = $_SESSION['checksum'];
} //empty($checksum)
else
	$_SESSION['checksum'] = $checksum;
$hash           = get_sha1($partyid, $custband, '');
$time_yesterday = time() - (24 * 60 * 60);
$date_yesterday = date('Ymd', $time_yesterday);
$hash_yesterday = get_sha1($partyid, $custband, '', $date_yesterday);
if (($hash != $checksum && $checksum != $hash_yesterday) || empty($promoid)) {
	header('Location:/' . DIR . 'all_comps.php');
	exit();
} //($hash != $checksum && $checksum != $hash_yesterday) || empty($promoid)
$comp     = $db->from($table['competition'])->where('status', 'live')->where('treat_id', $promoid)->fetch_first();
$time_now = date('Y-m-d H:i:s');

if ($time_now < $comp['start_date'] || $time_now > $comp['end_date']  || $user_token != $_SESSION['user_token']) 
{
	header('Location:/' . DIR . 'index_game.php?promoid='.$promoid);
	exit();
}

require('db_functions.php');
record_game_play($partyid,$promoid,$score);
if($score >= $comp['win_score'])
{
	$_SESSION['score'] = $score;
	$_SESSION['winhash'] = get_sha1($promoid,$score,NULL,24);
 	header('Location:/' . DIR . 'index.php?promoid='.$promoid);
	exit();	
}
else
{
	$booking_rnd      = get_rand_id(10);
	$sql                      = sprintf("INSERT INTO sky_%s (`party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`,`original_band`,`original_action_date`)  values('%s','%s','%s','%s',now(),'%s','%s','%s','%s','%s','%s',now()) ", $table['history'], $partyid, strtolower(CATEGORY), $promoid, $db->escape($comp['promotion_title']), $comp['end_date'], '', 'played', 1, $booking_rand, $custband);
	$sql .= ' ON DUPLICATE KEY UPDATE party_id = values(party_id),category_id = values(category_id),promo_id = values(promo_id),promo_title = values(promo_title),action_date = now(),promo_date = values(promo_date),location= values(location),action = values(action),volume = values(volume),action_id=values(action_id)';
	$db        = new DB_manager(HOST, DBU, DBPASS, DB_HISTORY);
	$db->debug = 0;
	$db->query($sql)->execute();
	$_SESSION['game_play_recorded'] = time();
	$db = new DB_manager(HOST, DBU, DBPASS, DB);
	$db->set_table_prefix('sky_');

	$all_rows = all_game_play($partyid,$promoid);
	$total_score=0;
	foreach($all_rows as $row)
	{
		$total_score += $row['score'];
	}
}

?><?php
require('header.php');

?>
  <div class="topbaner" style="background-color:<?= $comp['header_bg_color']; ?>">
   <div class="leftimage">
   <?php
   	unset($_SESSION['user_token']);
    if(!empty($comp['analytics_id']))
	{
		$you_parts = explode('?v=',$comp['analytics_id']);
		$you_parts = $you_parts[1];
		$you_parts = explode('&',$you_parts);
		$youtube_id = $you_parts[0];
	?><div class="video-container"><iframe src="https://www.youtube.com/embed/<?php echo $youtube_id;?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe></div>
	<?php
	}else
	{ 
	if ($overlay!='' ){?> 
			<div class="<?php echo $overlay; ?>image"></div>
	<?php } ?>
		    <img src="<?php echo CDN_URL;?><?php echo DIR;?>content/<?= $comp['image']; ?>" alt="">
	<?php } ?>
</div>
    <div class="rightparttext">
      <div class="textsection">
        <div class="bigtext">
          <?= nl2br($comp['promotion_title']); ?>
        </div>
        <div class="subheadertext">
          <?= nl2br($comp['sub_title']); ?>
        </div>
      <div class="contentpart" style="border: solid 1px #4c4c4c;"> 
          <span class="bigtext">
		  <?php 

		  if(!empty($comp['game_message']))
		  {
			echo (str_replace(array('[SCORE]','[TOTAL_SCORE]'),array($score,$total_score),$comp['game_message']));
		  }
		  else{
		  ?>
		  
		  You scored <?php echo $score;?>. </span><br>
        Sorry, you didn't win this time. You can play again in <?php echo $comp['cooling_time'];?> minutes - good luck!

		<?php } ?>		
            <br>
          <br>
		  <div>
          <form action="all_comps.php" method="post" name="frm_back">
            <div class="buttondiv bottom-button">
              <input type="submit" class="btn_big"  name="btn_back" value="All exclusive prizes" />
            </div>
          </form>
        </div>
        </div>
         
</body>
</html>
