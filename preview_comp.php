<?php
require('local_config.php');
require(ROOT . 'config/sky_connect.php');

require("Admin/com_function.php");
//check_login();
	

$already_entered = false;
$errors          = array();
require(ROOT . 'common/xss_safe.php');
require(ROOT . 'common/db/DB_manager.php');
$db = new DB_manager(HOST, DBU, DBPASS, DB);
$db->set_table_prefix('sky_');
$db->debug = 1;
$xss       = new xssSafe();

$custband  = $xss->clean_input($_REQUEST['custband']);
$promoid   = $xss->clean_input($_REQUEST['promoid']);

$comp     = $db->from($table['competition'])->where('treat_id', $promoid)->fetch_first();
$term_tags = array('[CLOSING_DATE]','[START_DATE]','[DRAW DATE]','[NUMBER_WINNERS]','[NOTIFICATION_DATE]');
	$replace_tags = array(date('H:i \o\n j F, Y',strtotime($comp['end_date'])),date('H:i \o\n j F, Y',strtotime($comp['start_date'])),date('H:i \o\n j F, Y',strtotime($comp['drawtime'])),$comp['winners'],date('d F, Y',strtotime($comp['drawtime'])+(7*60*60*24)));

$comp['terms']=str_replace($term_tags,$replace_tags, $comp['terms']);
			
$time_now = date('Y-m-d H:i:s');
if (1) {
	$verify_captcha = false;
	$ip             = $_SERVER['REMOTE_ADDR'];
	if (0) // Do not verify captcha
		{
		$rowqr = $db->from($table['competition_data'])->where('ip_address', $ip)->order_by('Registration_Date', 'desc')->fetch_first();
		if ($db->affected_rows > 0) {
			// check for the duplicate email within an hour
			$lastdt  = $rowqr['Registration_Date'];
			$timenow = date('H');
			$timereg = date('H', strtotime($lastdt));
			if ($timenow == $timereg)
				$verify_captcha = true;
			elseif ($ip == '')
				$verify_captcha = true;
		} //$db->affected_rows > 0
	} //0
	
} //$time_now >= $comp['start_date'] && $time_now <= $comp['end_date']

$message1=", answer the competition question";		
$overlay = 'competition';
$message2='';
if($comp['comingsoon'] > '2000-01-01' && $comp['comingsoon']<= $time_now  && $time_now < $comp['start_date'] )
	$overlay = 'coming-soon';
elseif(empty($comp['question']))
{
	$overlay = 'prizedraw';
	$message1 = "";
	
}	
$message2="Remember, the longer you're with us the more chances you have to win.";
if(!empty($comp['question']) && empty($comp['answer_set']))

	$message2 = "";
	
?><?php
require('header.php');
?>
  <div class="topbaner" style="background-color:<?= $comp['header_bg_color']; ?>">
    <div class="leftimage">
   <?
    if(!empty($comp['analytics_id']))
	{
		$you_parts = explode('?v=',$comp['analytics_id']);
		$you_parts = $you_parts[1];
		$you_parts = explode('&',$you_parts);
		$youtube_id = $you_parts[0];
	?><div class="video-container"><iframe src="https://www.youtube.com/embed/<?php echo $youtube_id;?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe></div>
	<?php
	}else
	{ 
	if ($overlay!='' ){?> 
			<div class="<?php echo $overlay; ?>image"></div>
	<?php } ?>
		    <img src="<?php echo CDN_URL;?><?php echo DIR;?>content/<?= $comp['image']; ?>" alt="">
	<?php } ?>
</div>
    <div class="rightparttext">
      <div class="textsection">
        <div class="bigtext">
          <?= nl2br($comp['promotion_title']); ?>
        </div>
        <div class="subheadertext">
          <?= nl2br($comp['sub_title']); ?>
        </div>
        <?php
	{
	if (count($errors) > 0) {
		echo '<div class="error" id="err_div"><ul>';
		foreach ($errors as $error)
			echo "<li>$error</li>";
		echo '</ul></div>

									';
	} //count($errors) > 0
	if ($already_entered) {
		echo '

									 <div class="termcondition ">

									<a href="javascript:void(0);" id="terms_link" class="textarrow-up" onClick="toggletc();">Terms and conditions</a>

									' . $comp['terms'] . '</div>

									 <form action="all_comps.php" method="post" name="frm_back">

									 <div class="buttondiv"><input type="submit" class="btn_big"  name="btn_back" value="Competitions" /></div>

									</form>';
	} //$already_entered
	else {
?>
        <div class="bodytext">
        Simply provide your name, phone number and email address below<?=$message1;?>, accept  the terms and conditions and select &lsquo;Enter&rsquo;. <?=$message2;?> </div>
        <?php
	}
?>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <?php
	if (($hash == $checksum || $checksum == $hash_yesterday) && !$already_entered) {
?>
    <div class="contentpart">
      <?php
		
		if (empty($email)) {
			$user  = $db->from($table['users'])->where('partyid', $partyid)->fetch_first();
			$email = $xss->clean_input($user['email']);
			$fname = $xss->clean_input($user['fname']);
			$lname = $xss->clean_input($user['lname']);
		} //empty($email)
?>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>First name</label>
          <input type="text" value="<?= stripslashes(htmlspecialchars($fname)); ?>" name="fname" class="btn_big_grey">
        </div>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Last name</label>
          <input type="text" value="<?= stripslashes(htmlspecialchars($lname)); ?>" name="lname" class="btn_big_grey">
        </div>
<? if($comp['show_address']){ ?> 
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>House name/number</label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($house_number));?>" name="house_number" class="inputbox">
          </div>
        </div>
		<div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Street</label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($street));?>" name="street" class="inputbox">
          </div>
        </div>
        
		<div class="formrow">
          <label class="fomrlabel">Address 2 </label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($address1));?>" name="address1" class="inputbox">
          </div>
        </div>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>City</label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($city));?>" name="city" class="inputbox">
          </div>
        </div>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Post Code</label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($post_code));?>" name="post_code" class="inputbox">
          </div>
		</div>
		<?php } ?>
       <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Telephone number</label>
          <div class="filedbox">
            <input type="text" value="<?= stripslashes(htmlspecialchars($telephone)); ?>" name="telephone" class="inputbox">
          </div>
        </div>
      <div class="formrow"> <a href="javascript:void(0);">*Please confirm your email address <br>
        (If you win, we’ll contact you at this address)</a> </div>
      <div class="formrow">
        <input type="email" id="email" name="email" class="btn_big_grey" value="<?php
		echo stripslashes(htmlspecialchars($email));
?>" />
      </div>
      <div class="spacerdiv">&nbsp;</div>
      <?php
      $comp['question'] = trim($comp['question']);
	  if(!empty($comp['question']))
		{
			?>
            <div class="formrow">
          <label class="fomrlabel"><?php echo $comp['question'];?><span class="grey70">*</span></label>
          <div class="filedbox">
		  <?php $a_count=0;
			if(!empty($comp['answer_set']))
			{
					$comp['answer_set'] = explode('|',$comp['answer_set']);
					foreach($comp['answer_set'] as $ans)
					{
						if($a_count)
								echo '<br>';
						$a_count++;
						?> <input type="radio" <?php if($ans==$answer) echo 'checked';?> name="answer" id="answer<?php echo $a_count;?>" value="<?php echo $ans;?>" />
						<?php 
						echo $ans;
					}
				}
				else
				{
				?>
				<input type="text" value="<?= stripslashes(htmlspecialchars($answer)); ?>" name="answer" class="inputbox">
				<?php } ?>
			  </div>
			</div>
				<?php 
			
			} ?>
      <div>
        <div class="termcondition ">
          <div class="withcheckbox">
            <?php {
?>
            <label class="control control--checkbox tearmcheckbox">
            <input type="checkbox" name="terms_agreed" value="agree"/>
            <div class="control__indicator"></div>
            </label>
            <?php
		}
?>
            <a href="javascript:void(0);" id="terms_link" class="textarrow-up" onClick="toggletc();">Click here to accept our terms and conditions</a></div>
          <?php
		echo $comp['terms'];
?></div>
      </div>
      <div class="spacerdiv">&nbsp;</div>
      <div class="clear"></div>
			<?php
			$comp['optin'] = trim($comp['optin']);
				if (!empty($comp['optin'])) {
?>
				<div class="termcondition ">
					<div class="withcheckbox">
						<div class="condition">

						<label class="control control--checkbox tearmcheckbox">
								<input type="checkbox" name="partner1" id="partner1" value="1"/>
								<div class="control__indicator"></div>
							</label>
							<?= $comp['optin']; ?>
						</div>
					</div>
				</div>
				<div class="spacerdiv">&nbsp;</div>
				<div class="clear"></div>
				
				<?php
				}
		if ($verify_captcha) {
			$ttf = rand(1, 65);
?>
      <div class="termcondition">
        <label>Image verification code</label>
        <div style="text-align: left; display: inline-block;"><img align="absmiddle" id="cimgs" src="/captcha/securimage_show.php?sid=<?= $ttf; ?>"> <a onclick="document.getElementById('cimgs').src = '/captcha/securimage_show.php?sid=' + +Math.floor(65*Math.random()); return false" href="#">Reload Image</a></div>
        <input type="text" class="button" id="ccode" name="ccode">
      </div>
      <?
		} //$verify_captcha
?>
      <div class="buttondiv">
        <?
		$user_token             = get_rand_id(10);
		$_SESSION['user_token'] = $user_token;
?>
      </div>
    </div>
  <?php
	} //($hash == $checksum || $checksum == $hash_yesterday) && !$already_entered
?>
</div>
<?php
}
?>
</body>
</html>
<script language="javascript">

function toggletc()
{
	$(".termcondition p").fadeToggle(1000);
	$("#terms_link").toggleClass("textarrow-up terms-textarrow");
}

// Sky tracking code start 
$(document).ready(function() {

var data = {
    page: {
        name: "comp_detail",
        breadcrumb: [ "skyapp", "stretch", "" ]
    }
};

	skyTags.queue.push(['set', data]);
});

</script>