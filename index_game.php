<?php
require('local_config.php');
require(ROOT . 'config/sky_connect.php');
$track_pagename = 'compdetail';
$already_entered = false;
$errors          = array();
require(ROOT . 'common/xss_safe.php');
require(ROOT . 'common/db/DB_manager.php');
$db = new DB_manager(HOST, DBU, DBPASS, DB);
$db->set_table_prefix('sky_');
$db->debug = 0;

require('db_functions.php');
$comp = '';

$xss       = new xssSafe();
$checksum  = $_REQUEST['checksum'];
$partyid   = $xss->clean_input($_REQUEST['partyid']);
$custband  = $xss->clean_input($_REQUEST['custband']);
$promoid   = $xss->clean_input($_REQUEST['promoid']);
if (empty($custband)) {
	if (!empty($_SESSION['custband']))
		$custband = $_SESSION['custband'];
} //empty($custband)
else
	$_SESSION['custband'] = $custband;
if (empty($partyid)) {
	if (!empty($_SESSION['partyid']))
		$partyid = $_SESSION['partyid'];
} //empty($partyid)
else
	$_SESSION['partyid'] = $partyid;
if (empty($checksum)) {
	if (!empty($_SESSION['checksum']))
		$checksum = $_SESSION['checksum'];
} //empty($checksum)
else
	$_SESSION['checksum'] = $checksum;
$hash           = get_sha1($partyid, $custband, '');
$time_yesterday = time() - (24 * 60 * 60);
$date_yesterday = date('Ymd', $time_yesterday);
$hash_yesterday = get_sha1($partyid, $custband, '', $date_yesterday);
if (($hash != $checksum && $checksum != $hash_yesterday) || empty($promoid)) {
	header('Location:/' . DIR . 'all_comps.php');
	exit();
} //($hash != $checksum && $checksum != $hash_yesterday) || empty($promoid)
	$comp     = $db->from($table['competition'])->where('status', 'live')->where('treat_id', $promoid)->fetch_first();
	if(!empty($comp))
	{
		$external_game = false;
		if(!empty($comp['game_file']))
		{	
			if(file_exists('jsgames/'.$comp['game_file']))
				$external_game = true;
		}
	}
	if($external_game==false)
	{
		header('Location:index.php');
		exit();
	}

$time_now = date('Y-m-d H:i:s');
if ($time_now >= $comp['start_date'] && $time_now <= $comp['end_date']) {
	
} //$time_now >= $comp['start_date'] && $time_now <= $comp['end_date']
else {
	$stime    = explode(" ", $comp['start_date']);
	$stime2   = explode('-', $stime[0]);
	$stime[0] = "$stime2[2]/$stime2[1]/$stime2[0] ";
	$etime    = explode(" ", $comp['end_date']);
	$etime2   = explode('-', $etime[0]);
	$etime[0] = "$etime2[2]/$etime2[1]/$etime2[0] ";
}

?><?php
require('header.php');
?>
  <div class="topbaner" style="background-color:<?= $comp['header_bg_color']; ?>">
   <div class="leftimage">
   <?php
	if ($overlay!='' ){?> 
			<div class="<?php echo $overlay; ?>image"></div>
	<?php } ?>
		    <img src="<?php echo CDN_URL;?><?php echo DIR;?>content/<?= $comp['image']; ?>" alt="">
</div>
    <div class="rightparttext">
      <div class="textsection">
        <div class="bigtext">
          <?= nl2br($comp['promotion_title']); ?>
        </div>
        <div class="subheadertext">
          <?= nl2br($comp['sub_title']); ?>
        </div>
        <?php
if ($time_now < $comp['start_date'] || $time_now > $comp['end_date']) {
?>
      <div class="contentpart"> 
          This  draw is coming soon.
            <br>
          <br>
        </div>
          
        <?
} //$time_now < $comp['start_date'] || $time_now > $comp['end_date']
else 
{		
	$min_time = time()-($comp['cooling_time']*60);
	$min_time = date('Y-m-d H:i',$min_time);
	$play_result = check_last_play($partyid,$promoid,$min_time);
	if($play_result['result']!=true)
	{	
$timevalue=$comp['cooling_time'];
		$timename="minutes";
		if($comp['cooling_time'] % 30 ==0)
		{		$timename="hours";
		$timevalue=$comp['cooling_time']/60;
	}
		$next_play = date($DATE_DISPLAY_FORMAT,strtotime($play_result['last_played'])+($comp['cooling_time']*60));
		$errors['played']="Please wait for $timevalue $timename from your last play time. You can play again after $next_play.";
	}
	if (count($errors) > 0) {
		echo '<div class="error" id="err_div"><ul>';
		foreach ($errors as $error)
			echo "<li>$error</li>";
		echo '</ul></div>
									';
	} //count($errors) > 0

	
?>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <?php
	if (($hash == $checksum || $checksum == $hash_yesterday) && empty($errors['played'])) {
?>
<div class="playpopup" id="overlay">
	<div id="game" style="display: inline;">
	</div>
</div>
  <form action="game_success.php" method="post" name="comp_form" id="comp_form">
    <div class="contentpart">
      <?php
		
		?>
      <div class="buttondiv">
        <?
		$user_token             = get_rand_id(10);
		$_SESSION['user_token'] = $user_token;
		  ?>
		  <input type="hidden" name="user_token" id="user_token" value="<?= $user_token; ?>">
        <input type="hidden" name="promoid" id="promoid" value="<?= $promoid; ?>">
		<input type="hidden" name="score" id="score" value="0">
        <input type="button" value="Start Game" id="gamebutton" name="caller" class="button">
      </div>
    </div>
  </form>
  <?php
	} //($hash == $checksum || $checksum == $hash_yesterday) && empty($errors['played'])
?>
</div>
<?php
}
?>
</body>
</html>
<?php	
		if($external_game && empty($errors['played']) )
		{
			echo '
			<script src="'. CDN_URL.'js/game_libV1.1.js"></script>';	
		?>

		<script language="javascript">
			$('#gamebutton').on('click', function(e)
			{ 
				$('#overlay').show();
				// Bootstrap example for loading the game and sending/receiving messages
				(async () => {
				  const gameId = "<?php echo CDN_URL;?>jsgames/<?php echo $comp['game_file'];?>";
				  const sessionId = guid();
				  try {
					const game = await loadGame(gameId, { mountNode: document.querySelector('#game') });
					game.on("ready", (data) => {

					  console.log("ready", data);
					  game.start(sessionId);
					});
					game.on("end", (data) => {
						//if(data.userId=="<?php echo $partyid;?>" )
										$('#score').val( data.score);
									$('form#comp_form').submit();
									$(".gamebutton").off("click");
									$('#overlay').hide();
					  console.log("end", data);
					});
				  } catch (err) {
					console.error(err);
				  }
				})();
			});
		</script>
<?php	
		}	
		
		?>