<?php
require('local_config.php');
require(ROOT . 'config/sky_connect.php');
$track_pagename = 'compdetail';
$already_entered = false;
$errors          = array();
require(ROOT . 'common/xss_safe.php');
require(ROOT . 'common/db/DB_manager.php');
$db = new DB_manager(HOST, DBU, DBPASS, DB);
$db->set_table_prefix('sky_');
$db->debug = 0;
$promoid = $db->escape($_REQUEST['promoid']);
$comp     = $db->from($table['competition'])->where('status', 'live')->where('treat_id', $promoid)->fetch_first();
$term_tags = array('[CLOSING_DATE]','[START_DATE]','[DRAW DATE]','[NUMBER_WINNERS]','[NOTIFICATION_DATE]');
	$replace_tags = array(date('H:i \o\n j F, Y',strtotime($comp['end_date'])),date('H:i \o\n j F, Y',strtotime($comp['start_date'])),date('H:i \o\n j F, Y',strtotime($comp['drawtime'])),$comp['winners'],date('d F, Y',strtotime($comp['drawtime'])+(7*60*60*24)));		
	$comp['terms']=str_replace($term_tags,$replace_tags, $comp['terms']);
	
 ?>
 <!DOCTYPE html>
<html lang="en-US">
<head>
<title>
<?=$comp['promotion_title'];?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="ROBOTS" CONTENT="NOARCHIVE">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!-- Responsive and mobile friendly stuff -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

 <link rel="stylesheet" href="<?php echo CDN_URL;?>/sky/booking/css/general.css?v=5" media="all" type="text/css">
<link rel="stylesheet" href="<?php echo CDN_URL;?>/sky/booking/css/Band.css" media="all" type="text/css">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93102600-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93102600-3');
</script>

</head>
<body>
 <div class="wraper">
 <div class="get-questions-text"><img src="<?php echo CDN_URL;?>/sky/booking/images/logo.png"></div>

		<div>&nbsp;</div>
			<div style="max-width:900px; margin:auto;">
			<?=$comp['terms'];?>
			</div>
		<div>&nbsp;</div>
	</div>
	<footer class="footer-distributed">

<div class="footer-left"> <img src="https://www.weticketit.com/wtilogo3.png" width="100">

  <p class="footer-links2"> <a href="#actionjava" class="toggleDiv" target_div="div_about">About</a></p>
  <div class="tearms-togletaxt" id="div_about"> 
	<p>WeTicketIt® is a full service ticketing platform under licence to Stretch Communications Ltd, designed for customer loyalty, media promotions, membership and rewards programmes. We originally ticketed for film promotions and premieres in the early 2000s, but rapidly grew into broader entertainment promotions. We provide the technical and logistical support for leading rewards programmes in the UK, USA, Europe and Australia.<br><br>Stretch Communications Ltd is an audience provider. We fulfil targeted audience requirements for 'word of mouth' and test screenings, shows and events.<br><br></p>
  </div>
<p class="footer-links2"> <a href="#actionjava" class="toggleDiv" target_div="div_privacy">Privacy policy</a></p>
<div class="tearms-togletaxt" id="div_privacy"><p>At Stretch Communications Ltd, we are committed to maintaining the trust and confidence of visitors to our web sites. In particular, we want you to know that Stretch Communications Ltd is not in the business of selling, renting or trading email lists with other companies and businesses. But just in case you don’t believe us, in this Privacy Policy, we’ve provided lots of detailed information on when and why we collect your personal information, how we use it, the specific conditions under which we may disclose it to others and how we keep it secure. Grab a cuppa and read on.<br><br>This privacy policy applies to the websites ('Sites') operated by the software / website licensor, Stretch Communications Ltd and to the processing of personal information as contemplated in this privacy policy. Any reference to 'you' or 'your' means you, the user.
Stretch Communications respects your privacy and is committed to protecting the personal information you share with us. This statement describes how we collect and use your personal information that we may collect about you through the Sites and from our communications with you. The Sites are hosted on various servers within the European Economic Area.<br><br>
Data Protection Laws<br>
We are conscious of our legal responsibilities as a 'data controller' and we support the principles of data protection reflected in The&nbsp;General Data Protection Regulation&nbsp;(GDPR) (EU) 2016/679 and applicable national implementing legislation. We shall endeavour to ensure that the personal information we obtain and use will always be held, used, transferred and otherwise processed in accordance with our legal obligations.<br><br>
Other websites<br>
There may be links from the Sites to other websites. This privacy policy only applies to the Sites and not to any other websites including any websites linked from any of the Sites. Accessing those third party websites or sources requires you to leave the Sites. We do not control those third party websites or any of the content they contain and you expressly acknowledge and agree that we are in no way responsible or liable for any of those third party websites, including, without limitation, the content, policies, failures, promotions, products, services or actions of those websites and/or any damages, losses, failures or problems caused by, related to or arising from those websites. We encourage you to review all policies, rules, terms and regulations, including the privacy policies, of each website that you visit.<br><br>
How we obtain personal information about you<br>
We may as a result of your interaction with our Sites and our communications with you hold and process personal information obtained about you. The types of personal information we collect may include:<br>
your name<br>
your phone number<br>
your email address<br>
your address<br>
your profession<br>
your date of birth<br>
your entertainment preferences<br>
your customer priority number<br>
From time to time, we may alter the specific types of personal information we collect.<br><br>
How we collect and use non-personal information<br>
Please note that when anyone visits any of the Sites, we automatically collect certain non-personal information such as the type of computer operating system (e.g., Windows or Mac OS) and browser (e.g., Safari, Chrome, Internet Explorer) being used, and the domain name of the internet service provider. We often collect information on your entertainment preferences. We sometimes use the non-personal information that we collect to improve the design and content of the Sites and to enable us to personalise your experience.<br><br>
What we do with your personal information<br>
The personal information that you provide will be available to Stretch Communications, our affiliated companies and trusted third party service providers and contractors for the following purposes:<br>
to process enquiries received via the Sites<br>
to register you for and send you newsletters<br>
to conduct market research and business development<br>
to provide you with information about products and promotions that may be of interest to you from ourselvesin connection with surveys related to our products and the Sites
any other purpose indicated at the time you voluntarily provide your personal information<br><br>
Payment Processing<br>
In any instance where we process payments through the Websites, the payment information and the processing function is handled by STRIPE in accordance with their data protection and privacy policies. We do not receive, process or store credit or debit card details, bank details or payment information of any kind, nor do we share customer details with any 3rd parties.<br><br>
Marketing<br>
We may from time to time use your personal information to send you automated email messages or marketing materials regarding our services and the services of third party suppliers, in each case with your prior consent. These email messages may contain features that help us make sure you received and were able to open the message.
You may opt out of receiving such marketing email messages at any time, free of charge, by replying to the message with 'unsubscribe' in the subject line, or by following the instructions in any marketing communication.<br><br>
To make our website better<br>
We will also process your personal data in order to provide you with a more tailored experience when you visit our website. For example, we may use your personal data to tailor our website, make sure it is displayed in the most effective way for the device you are using. We also use various third party cookies to help us improve our website (more details are set out in the sections below) but we will not share your personal data with the third party analytics and search engine providers that assist us in the improvement and optimisation of our website (see cookies and tracking software below). We will also process your personal data for the purposes of making our website more secure, and to administer our website and for internal operations, including troubleshooting, data analysis, testing, research and statistical and survey purposes.
The legal basis on which we process your personal data in these circumstances is our legitimate interest to provide you with the best possible website we can, and to ensure that our website is kept secure. <br><br>
Cookies and tracking software<br>
We use 'cookies' and other types of tracking software in order to personalise your visit to our Sites and enhance your experience by gaining a better understanding of your particular interests and customising our pages for you. A cookie is a message given to a web browser by a web server and which is then stored by the browser in a text file. Each time the browser requests a page from the server this message is sent back which enables the user's computer to be identified.<br>
We may use the information provided by cookies to analyse trends, administer the Sites, or for research and marketing purposes to help us better serve you..<br>In specific we use:<br>
Session and Browser Cookies - to keep you logged in and to prevent malicious attacks. These are accessed by our software to automatically revalidate users during the booking process. 
Google Analytics Cookies - to measure traffic, user experience, website performance and other statistical information provided by Google Analytics. These are accessed by Google and the results provided to us in the form of anonymised graphs, charts and raw data.<br> <br>
No information which in itself personally identifies you will be collected through the cookies. These cookies are not used for any purpose other than those described here. If you like, you can set your browser to notify you before you receive a cookie so you have the chance to accept it and you can also set your browser to turn off all cookies. The website www.allaboutcookies.org (run by the Interactive Marketing Bureau) contains step-by-step guidance on how cookies can be switched off by users.<br>
Please note that our Sites require the use of cookies in order to work at their best in some instances to work at all. If you do not wish these cookies to be used then please note that you may not experience our Sites working to their best effect or at all.<br><br>
Disclosing your personal information<br>
We may disclose your personal information to companies and individuals who perform business functions and services on our behalf. Such functions may include hosting the Sites, analysing data and providing other support services. All such parties will be required to keep your personal data secure and only process it in accordance with our instructions.
We may also disclose your personal information if, in our opinion, disclosure is required by law.<br>
Finally, we may disclose your personal information if you have explicitly given consent for us to do so, and then only to the third party identified at the time of consenting - and in accordance with their own privacy policy.<br><br>
International transfers of your personal information<br>
We may transfer and process any personal information you provide to us to countries outside the European Economic Area whose laws may not afford the same level of protection to your personal information. We will therefore ensure that all adequate safeguards are in place and that all applicable laws and regulations are complied with in connection with such transfer.<br><br>
The accuracy of your information<br>
While we endeavour to ensure that the information we hold about you is accurate and kept up to date, we shall assume that in the absence of evidence to the contrary, the information you provide us with is accurate. Should you inform us of inaccuracies in the information which we hold in relation to you or, if we discover that such information is inaccurate, it shall be promptly rectified by us. We do not intentionally retain any information about you which is out of date or which is no longer required.
Personal Data Security and Confidentiality<br>
We maintain adequate technical and organisational security measures to protect your personal information from loss, misuse, and unauthorised access, disclosure, alteration, or destruction.<br><br>
Access and Correction Rights<br>
By logging in to our ticketing sites, you can see the profile information we hold on you and you can update it there. You can also see your booking history. <br>
You can request access to, and have the opportunity to update and amend your personal information, and you can exercise any other rights you enjoy under applicable data protection laws, including, for example, objection to and blocking of the processing of your personal information, by contacting us at support@skyticketit.com. For security reasons, we reserve the right to take steps to authenticate your identity before providing access to your personal information. Subject to applicable law, we may charge you a small fee to access your data.<br><br>
Retention of data<br>
We will keep your personal information for as long as necessary for the purposes for which it was collected, to provide you with services and to conduct our legitimate business interests or where otherwise required by law.<br>In instances where our rules are broken by a user and we ban the user from further accessing our sites, we will retain ad infinitum such information as is necessary to identify the user in order to prevent access.<br>
Changes to the Policy<br>
We reserve the right, at our sole discretion, to modify, add or remove sections of this privacy policy at any time and any changes will be notified to you using the email address you have given us or by an announcement on the webpage available at the 'Privacy Policy' link on the Sites. Your continued use of the Sites, following the posting of changes to this privacy policy, will mean you accept these changes.<br><br>
Privacy Queries<br>
If you have any questions regarding this policy, or you wish to update your details or remove your personal data from our records, please inform us by emailing support@skyticketit.com.<br><br></p></div>
<p class="footer-links2">
<a href="#actionjava" class="toggleDiv" target_div="div_refunds">Refunds</a>
  </p>
  <div class="tearms-togletaxt" id="div_refunds"><p>Please refer to Section 8 (Refunds) and section 9 (Liability) of our Terms &amp; Conditions below.<br><br></p></div>

  <p class="footer-links2"><a href="#actionjava" class="toggleDiv" target_div="div_terms2">Terms of service</a> </p>
<div class="tearms-togletaxt" id="div_terms2">
  <p>Stretch Communications Ltd is a company registered in England and Wales under company number 4594879, with registered office at 62-70 Shorts Gardens, London WC2H 9AH (“we”, “us”, “our”). This Booking Policy is designed to ensure your satisfaction and understanding of the purchase process on weticketit.com. </p>
  <p>1. GENERAL &amp; DEFINITIONS<br>
	1.1 We ‘Offer’ (sell or give away) tickets and associated products and/or services on behalf of partners, license holders, artists, agents, producers, promoters, record labels, teams and venues. We refer to these parties who organise or provide the event and/or from whom we obtain tickets and/or associated products or services to sell or offer for free to you as our "Event Partner".<br>
	1.2 We Offer tickets as and when allocated by Event Partners. The quantity of tickets made available by us vary on an event by event basis. Tickets are generally offered through several distribution points, including online, call centres and, in some cases, box offices. Tickets for popular events may sell out quickly. Occasionally, additional tickets may be available prior to the event, however We do not always control this inventory or its availability.<br>
	1.3 For some events, tickets may be Offered as part of a “Package” (where a ticket for an event is Offered together with concessions, merchandise or other valuable benefits such as exclusive seating arrangements, accommodation, transport, dining or merchandise as an inclusive package at an inclusive price)<br>
	1.4 In this Booking Policy, we refer to any products and/or services Offered by us as “Items”.<br>
	1.5 By placing an ‘Order’ (a request for an Item), if we are able to fulfil the order, we will confirm it on screen and in email, usually in the form of a ticket or booking confirmation. Once we have confirmed that order, whether a financial transaction is required or not, we refer to this completion as a “Booking”. <br>
	1.6 To Order Item(s) from us, you must be 18 or over and have a valid credit/debit card issued in your name. To book Item(s) from us which are free, you must be 13 or over unless otherwise specified.</p>
  <p>2. CONTRACT<br>
	2.1 Any booking through our platform whether free or paid is subject to: (i) this Booking Policy; (ii) any special terms and conditions which may be displayed on our website; and (iii) the terms and conditions of the Event Partner(s) and/or event, which can be found on their respective websites. Venue terms and conditions may also be available at the venue box office.<br>
	2.2 Your contract of Booking of an Item starts once we have confirmed your Order and ends immediately after the completion of the event for which you have Ordered the Item, save that, if you have Ordered any physical product, your contract for the Booking of such product will end 14 days after the date of delivery of the product to you. All Orders are subject to payment card verification and/or other security checks and your transaction may be cancelled if it has not passed our verification process.<br>
	2.3 You agree not to obtain or attempt to obtain any Items through unauthorised use of any robot, spider or other automated device or any other illegal or unauthorised activity. We reserve the right to cancel any transaction which we reasonably suspect to have been made in breach of these provisions without any notice to you and any and all Items purchased as part of such transaction will be void.<br>
	2.4 We reserve the right to cancel Bookings which we reasonably suspect to have been made fraudulently.</p>
  <p>3. PRICES AND FEES<br>
	3.1 Orders from us may be subject to a per Item service charge and a non-refundable per order delivery fee.<br>
	3.2 Whilst we try to ensure that all prices on our website are accurate, errors may occur. If we discover an error in the price of any Item you have Ordered, we will inform you as soon as possible and give you the option of reconfirming your Order at the correct price (and credit or debit your account as applicable) or cancelling your Order. If we are unable to contact you, you agree that we may treat the order as cancelled. If you choose to cancel after you have already paid the incorrect price, you will receive a full refund from us.</p>
  <p>4. CANCELLATIONS<br>
	4.1 If you have purchased a ticket, or a Package, you are not entitled to cancel your purchase.<br>
	4.2 If you have purchased any product, you are entitled to cancel your transaction within fourteen (14) days of the date of delivery of the product (or during such longer period as may be specified in the Event Partner’s terms and conditions). You are not entitled to cancel any associated ticket purchase in such circumstances.<br>
	4.3 If you have Booked a free ticket, you may cancel the ticket via the website up until the cancellation deadline which will vary from event to event.</p>
  <p>5. DELIVERY<br>
	5.1 We aim to dispatch tickets as soon as possible. We aim to inform you of the timescales on your booking confirmation page and email. In some instances we are not able to specify the exact dates of dispatch, as the arrangements for dispatch depend on when we are in possession of the ticket stock used for a particular event. For some events, we receive ticket stock from our Event Partners close to the event date.<br>
	5.2 Please allow as much time as possible for your tickets to arrive. If your tickets have not arrived three days before the event (or, if you are travelling, three days before you leave on your journey), please contact us. Please include your reference number and the name and postcode the booking is made under.<br>
	5.3 We post tickets to the address given at point of Order. Please note that if the address in your booking does not correspond to that held by your credit card company, we may cancel your tickets.<br>
	5.4 We reserve the right to make tickets available for collection by you at the venue box office. We will notify you by telephone or email of the arrangements for collection (using the details provided by you at the time of ordering) if this becomes necessary. You may be required to provide your booking confirmation email and your photo ID to collect tickets.<br>
	5.5 Any and all products and/or services included in a Package are provided and fulfilled by our Event Partners, who are responsible for the delivery and the quality of such products and/or services. If you have any queries or complaints regarding the non-ticket element of the Package, please contact the relevant Event Partner directly. For contact details, please refer to the booking confirmation email.</p>
  <p>6. TICKETS<br>
	6.1 Any ticket you purchase from us remains the property of the Event Partner and is a personal revocable licence which may be withdrawn and admission refused at any time. If this occurs, you will be refunded the sale price of the ticket which has been withdrawn or for which access was refused (including the relevant per ticket service charge but excluding the per order handling fee).<br>
	6.2 Policies set by our Event Partners, may prohibit us from issuing replacement tickets for any lost, stolen, damaged or destroyed tickets. For example for non-seated events, allowing a possibility of both the original and replacement tickets being used, may compromise the licensed capacity of the venue. If replacement tickets are being issued, we may charge you a reasonable administration fee.<br>
	6.3 When you receive your tickets, please keep them in a safe place. We will not be responsible for any tickets that are lost or stolen. Please note that direct sunlight or heat can sometimes damage tickets.<br>
	6.4 It is your responsibility to check your tickets; mistakes cannot always be rectified.<br>
	6.5 You have a right only to a seat of a value corresponding to that stated on your ticket. We, the venue or Event Partner reserve the right to provide alternative seats (whether before or during the event) to those initially allocated to you or specified on the tickets.</p>
  <p>RESTRICTIONS<br>
	6.6 When Ordering tickets from us, you are limited to a specified number of tickets for each event. This number is included on event information page and is verified with every transaction. This policy is in effect to discourage unfair ticket ordering. Tickets may be restricted to a maximum number per person, per credit card and, for some events, a restriction may apply per household. We reserve the right to cancel tickets booked in excess of this number without prior notice.<br>
	6.7 Tickets may be Offered subject to certain restrictions on entry or use, such as restricted, obstructed or side view or a minimum age for entry. Any such restriction shall be displayed on our website or otherwise notified to you before or at the time you book the tickets. It is your responsibility to ensure that you read all notifications displayed on our website.<br>
	6.8 You may not resell or transfer your tickets if prohibited by law. In addition, Event Partners may prohibit the resale or transfer of tickets for some events. Any resale or transfer (or attempted resale or transfer) of a ticket in breach of the applicable law or any restrictions imposed by the Event Partner is grounds for seizure or cancellation of that ticket without refund or other compensation.<br>
	6.9 You may not combine a Ticket with any hospitality, travel or accommodation service and/or any other merchandise, product or service to create a package, unless formal written permission is given by us and the Event Partner.<br>
	6.10 A ticket shall not be used for advertising, promotions, contests or sweepstakes, unless formal written permission is given by the Event Partner, provided that even if such consent is obtained, use of our trade marks and other intellectual property is subject to our prior consent.</p>
  <p>7. EVENT<br>
	7.1 It is your responsibility to ascertain whether an event has been cancelled and the date and time of any rearranged event. If an event is cancelled or rescheduled, we will use reasonable endeavours to notify you of the cancellation once we have received the relevant authorisation from the Event Partner. We do not guarantee that you will be informed of such cancellation before the date of the event.<br>
	7.2 Please note that advertised start times of events are subject to change.<br>
	7.3 Tickets are sold subject to the Event Partner’s right to alter or vary the programme due to events or circumstances beyond its reasonable control without being obliged to refund monies or exchange tickets, unless such change is a material alteration as described in paragraph 8.4, in which case the provisions of this paragraph shall apply.</p>
  <p>8. REFUNDS<br>
	8.1 Occasionally, events are cancelled, rescheduled or materially altered by the team, performer or Event Partner for a variety of reasons. Contact us for exact instructions.<br>
	8.2 Cancellation: If an event is cancelled (and not rescheduled), you will be offered a refund of the sale price of your ticket(s), including the relevant per ticket service charge but excluding the per order handling fee. If an event takes place over several days and one or more day(s) is/are cancelled (but not all the days constituting the event), a partial refund only may be payable corresponding to the day(s) cancelled. If your Booking involved no financial transaction, no compensation financial or otherwise will be offered. <br>
	8.3 Rescheduling: Unless indicated otherwise in relation to a particular event, if an event is rescheduled, you will be offered seats at any rescheduled event (subject to availability) of a value corresponding with your original tickets. If you are unable to attend the rescheduled event, you will be offered a refund of the sale price of your ticket(s) including the relevant per ticket service charge but excluding the per order handling fee. You must inform us within the time specified by us if you are unable to attend the rescheduled event, otherwise we may reconfirm your booking for the rescheduled date and you will not be entitled to claim a refund. If your Booking involved no financial transaction, we may transfer your tickets to the new date or we may offer you an exclusive rebooking window. No compensation financial or otherwise will be offered. <br>
	8.4 Material alteration: If an event is materially altered, you will be offered an option to either reconfirm your order for the altered event or to claim a refund (of the sale price of your ticket(s) including the relevant per ticket booking fee but excluding per order handling fee), within such time as specified by us. Failure to inform us of your decision may result in your order being reconfirmed for the altered event and you will not be entitled to claim a refund. A ‘material alteration’ is a change which, in our and the Event Partner’s reasonable opinion, makes the Event materially different to the Event that Bookers of tickets, taken generally, could reasonably expect. The use of understudies in theatre performances and/or any changes of: (i) any supporting act; (ii) members of a band; and/or (iii) the line-up of any multi-performer or multi-talent event (such as a festival or a TV recording or Q&amp;A that forms part of a screening event or exhibition) shall not be a material alteration. If your Booking involved no financial transaction, no compensation financial or otherwise will be offered. <br>
	8.5 To claim your refund, please apply in writing to: Stretch Communications Ltd, 62-70 Shorts Gardens, London WC2H 9AH (or to such other address as may be notified to you by us). You must enclose your unused tickets and comply with any other reasonable instructions from us. For accounting purposes your unused tickets must be received within 28 days from the date of the cancelled event.<br>
	8.6 If you have Ordered from us any Item associated with an event which has been cancelled, rescheduled or materially altered (such as car parking or travel) and a refund of a ticket is due to you in accordance with this clause 8, we will also refund you the purchase price of such Item purchased from us, including the per Item service charge but excluding the per order handling charge. If your Booking involved no financial transaction, no compensation financial or otherwise will be offered. <br>
	8.7 This Booking Policy does not and shall not affect your statutory rights as a consumer. For further information about your statutory rights contact Citizens Advice or the Department for Business Innovation and Skills.<br>
	8.8 We regret that, unless paragraphs 8.2, 8.3 or 8.4 apply, tickets cannot be exchanged or refunded after purchase.</p>
  <p>9. LIABILITY<br>
	9.1 Personal arrangements including travel, accommodation or hospitality relating to the Event which have been arranged by you are at your own risk. Neither we nor the Event Partner(s) shall be liable to you for any loss of enjoyment or wasted expenditure.<br>
	9.2 Unless otherwise stated in this clause 9, our and the Event Partner(s)’ liability to you in connection with the event (including, but not limited to, for any cancellation, rescheduling or material change to the programme of the event) and the Item you have purchased shall be limited to the price paid by you for the Item, including any per item service charge but excluding any per order handling fee. If your Booking involved no financial transaction, no compensation financial or otherwise will be offered. <br>
	9.3 Neither We nor the Event Partner(s) will be liable for any loss, injury or damage to any person (including you) or property howsoever caused (including by us and/or by the Event Partner(s)): (a) in any circumstances where there is no breach of a legal duty of care owed by us or the Event Partner(s); (b) in circumstances where such loss or damage is not a reasonably foreseeable result of any such breach (save for death or personal injury resulting from our negligence); or (c) to the extent that any increase in any loss or damage results from breach by you of any of the terms of this Booking Policy and/or any terms and conditions of the Event Partner(s) or your negligence.<br>
	9.4 Nothing in this Booking Policy seeks to exclude or limit our or the Event Partner(s)’ liability for death or personal injury caused by our or the Event Partner(s)’ (as relevant) negligence, fraud or other type of liability which cannot by law be excluded or limited.</p>
  <p>10. ADMISSION AND ATTENDANCE<br>
	10.1 The venue reserves the right to refuse admission should patrons breach any terms and conditions of the event or Event Partner. The venue may on occasions have to conduct security searches to ensure the safety of the patrons.<br>
	10.2 Every effort to admit latecomers will be made at a suitable break in the event, but admission cannot always be guaranteed.<br>
	10.3 There will be no pass-outs or re-admissions of any kind.<br>
	10.4 The unauthorised use of photographic and recording equipment is prohibited. Any photos, videos and/or recordings may be destroyed or deleted. Laser pens, mobile phones, dogs (except guide dogs) and a patron’s own food and drink may also be prohibited (please check with the venue).<br>
	10.5 You and other ticket holders consent to filming and sound recording as members of the audience.<br>
	10.6 Prolonged exposure to noise may damage your hearing.<br>
	10.7 Special effects which may include, without limitation, sound, audio visual, pyrotechnic effects or lighting effects may be featured at an event.</p>
  <p>11. QUERIES AND COMPLAINTS<br>
	11.1 If you have any queries or complaints regarding your purchase, contact us, quoting your order number given to you at the conclusion of placing the order.<br>
	11.2 Because we sell Items on behalf of Event Partners, we may need to contact them for more information before responding to your complaint. Some complaints can take up to 28 days to resolve, but we will get back to you as soon as possible.<br>
	11.3 If any dispute arises, we shall use our reasonable endeavours to consult or negotiate in good faith, and attempt to reach a just and equitable settlement satisfactory to you, us and the Event Partner.<br>
	11.4 Although this does not restrict your rights to pursue court proceedings, if we are unable to settle any dispute by negotiation within 28 days, you and we may attempt to settle it by mediation. To initiate a mediation a party must give written notice to the other parties to the dispute requesting a mediation. <br>
	11.5 As an online trader, pursuant to European Union legislation, we also draw your attention to the European Commission’s Online Dispute Resolution platform&nbsp;here, where you can access further information about online dispute resolution.</p>
  <p>You can also email us at support@skyticketit.com</p>
  <p>12. MISCELLANEOUS<br>
	12.1 The Event Partner and its affiliates, successors, or assigns may enforce these terms in accordance with the provisions of the Contracts (Rights of Third Parties) Act 1999 (the “Act”). Except as provided above, this agreement does not create any right enforceable by any person who is not a party to it under the Act, but does not affect any right or remedy that a third party has which exists or is available apart from that Act.<br>
	12.2 All of these terms and conditions are governed by English Law and any disputes arising out of any transaction with TicketWeb are subject to the exclusive jurisdiction of the English Courts.</p><br><br>  </div>
  <p class="footer-company-name">Website &amp; Software © 2016 Media Promotions (Digital) Ltd. Under licence to Stretch Communications Ltd.</p>
</div>
<div class="footer-center">

  <div> <i class="fa"><img src="<?php echo CDN_URL;?>/sky/booking/images/footer-location-icon.jpg"></i>

	<p><span>62-70 Shorts Gardens</span> London WC2H 9AH</p>

  </div>

  <div> <i class="fa"><img src="<?php echo CDN_URL;?>/sky/booking/images/footer-email-icon.jpg"></i>

	<p><a href="mailto:support@skyticketit.com">support@skyticketit.com</a></p>

  </div>

</div>

<div class="footer-right">


  <div class="footer-icons"> <a href="#"><img src="<?php echo CDN_URL;?>/sky/booking/images/facebook-footer-icon.jpg"></a> <a href="#"><img src="https://1973584292.rsc.cdn77.org/sky/booking/images/twitter-facebook-icon.jpg"></a> <a href="#"><img src="https://1973584292.rsc.cdn77.org/sky/booking/images/linkdin-footer-icon.jpg"></a> <a href="#"><img src="https://1973584292.rsc.cdn77.org/sky/booking/images/github-footer-icon.jpg"></a> </div>

</div>

</footer>
</body>


</html>	
