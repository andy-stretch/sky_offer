<?php
require('local_config.php');
require(ROOT . 'config/sky_connect.php');
$track_pagename = 'compdetail';
$already_entered = false;
$errors          = array();
require(ROOT . 'common/xss_safe.php');
require(ROOT . 'common/db/DB_manager.php');
$db = new DB_manager(HOST, DBU, DBPASS, DB);
$db->set_table_prefix('sky_');
$db->debug = 0;

$comp = '';
$upload_status = '';
function check_entry($partyid)
{
	global $errors,$db,$table,$comp,$upload_status;
	$data_ids = array();
	if(!empty($_SESSION['winhash']))
		return false;
	$allentries = $db->from($table['competition_data'])->where(array(
			'partyid' => $partyid,
			'treat_id' => $comp['treat_id']
		))->order_by('id', 'desc')->fetch();
		if ($db->affected_rows > 0) {
			$rowqr = array();
			foreach($allentries as $row)
			{	
				$data_ids[$row['id']] =$row['id'];
				if(empty($rowqr))
					$rowqr = $row;
			}
			$_SESSION['rec_ids'] =$data_ids;
			// check for the duplicate email within an hour
			$lastdt      = $rowqr['Registration_Date'];
			$timenow     = date('H');
			$timereg     = date('H', strtotime($lastdt));
			$entery_date = date('Y-m-d');
			$upload_status=$rowqr['email_read'];
			if ($is_hourly_comp && $timenow == $timereg && $lastdt > $entery_date) {
				$errors['email'] = "Sorry - you have already entered once this hour. If you would like to enter again, please do so next hour.";
				return true;
			} //$is_hourly_comp && $timenow == $timereg && $lastdt > $entery_date
			else {
				$entered_date    = date('d F Y', strtotime($lastdt));
				$close_date      = date('d F Y', strtotime($comp['end_date']));
				//competition
				$comp_type="prize draw";
				if(!empty($comp['question']))
					$comp_type = "competition";
				$comp['user_answer'] = $rowqr['answer'];
				$_SESSION['booking_rand'] = $comp['booking_rand'] = $rowqr['Booking_Id'];
				$errors['email'] = "You entered this $comp_type on $entered_date. We'll let you know if you’ve won after the $comp_type closes on $close_date. Good luck!";
				if($comp['require_upload']==1 && empty($upload_status))
				$errors['email'] = "You still need to upload your file. You'll need to do so by $close_date to be entered into the $comp_type.";
				/* not need this login now
				if($comp['require_upload']==1 && empty($upload_status))
				{
					$errors['email'] ='';
					$errors['file_missing'] = "You still need to upload your file. You'll need to do so by $close_date to be entered into the $comp_type.";
				}
				*/

				return true;
			}
		} //$db->affected_rows > 0
	return false;
}

$xss       = new xssSafe();
$checksum  = $_REQUEST['checksum'];
$partyid   = $xss->clean_input($_REQUEST['partyid']);
$custband  = $xss->clean_input($_REQUEST['custband']);
$promoid   = $xss->clean_input($_REQUEST['promoid']);
if (empty($custband)) {
	if (!empty($_SESSION['custband']))
		$custband = $_SESSION['custband'];
} //empty($custband)
else
	$_SESSION['custband'] = $custband;
if (empty($partyid)) {
	if (!empty($_SESSION['partyid']))
		$partyid = $_SESSION['partyid'];
} //empty($partyid)
else
	$_SESSION['partyid'] = $partyid;

if (empty($checksum)) {
	if (!empty($_SESSION['checksum']))
		$checksum = $_SESSION['checksum'];
} //empty($checksum)
else
	$_SESSION['checksum'] = $checksum;
$hash           = get_sha1($partyid, $custband, '');
$time_yesterday = time() - (24 * 60 * 60);
$date_yesterday = date('Ymd', $time_yesterday);
$hash_yesterday = get_sha1($partyid, $custband, '', $date_yesterday);
if (($hash != $checksum && $checksum != $hash_yesterday) || empty($promoid)) {
	header('Location:/' . DIR . 'all_comps.php');
	exit();
} //($hash != $checksum && $checksum != $hash_yesterday) || empty($promoid)
elseif($custband != 'E')
{	
	if(in_array($partyid,$BAND_E_CPNs))
	{	
		$_SESSION['custband'] = $custband = 'E';
		$_SESSION['checksum'] = get_sha1($partyid, $custband, '');
	}
}
$comp     = $db->from($table['competition'])->where('status', 'live')->where('treat_id', $promoid)->fetch_first();
include('../redirect.php');
check_redirect(); // enter name of current category offer,promo,athome,athomev2

	$external_game = false;
	if(!empty($comp))
	{		
		if(!empty($comp['game_file']))
		{	
			if(file_exists('jsgames/'.$comp['game_file']))
			{
				if(!empty($_SESSION['score']))
				{	
					$winhash= get_sha1($promoid,$_SESSION['score'],NULL,24);
					if($winhash != $_SESSION['winhash'])
					{
						header('Location:index_game.php?promoid='.$comp['treat_id']);
						exit();
					}					
				}
				else
				{	
					header('Location:index_game.php?promoid='.$comp['treat_id']);
					exit();
				}
			}
		}
	}

$time_now = date('Y-m-d H:i:s');
if ($time_now >= $comp['start_date'] && $time_now <= $comp['end_date']) {
	$verify_captcha = false;
	$ip             = $_SERVER['REMOTE_ADDR'];
	if (0) // Do not verify captcha
		{
		$rowqr = $db->from($table['competition_data'])->where('ip_address', $ip)->order_by('Registration_Date', 'desc')->fetch_first();
		if ($db->affected_rows > 0) {
			// check for the duplicate email within an hour
			$lastdt  = $rowqr['Registration_Date'];
			$timenow = date('H');
			$timereg = date('H', strtotime($lastdt));
			if ($timenow == $timereg)
				$verify_captcha = true;
			elseif ($ip == '')
				$verify_captcha = true;
		} //$db->affected_rows > 0
	} //0
	$caller = $_POST['caller'];
	if (empty($caller)) {
		$already_entered = check_entry($partyid);
	} //empty($caller)
	if ($caller == 'Submit' && $_POST['user_token'] == $_SESSION['user_token']) {
		$email        = $xss->clean_input($_POST['email']);
		$fname        = $xss->clean_input($_POST['fname']);
		$lname        = $xss->clean_input($_POST['lname']);
		$answer        = $xss->clean_input($_POST['answer']);
		$terms_agreed = $xss->clean_input($_POST['terms_agreed']);
		$telephone        = $xss->clean_input($_POST['telephone']);		
		$partner1        = $xss->clean_input($_POST['partner1']);
		$house_number = $xss->clean_input(strip_tags($_POST['house_number']));
		$address1 = $xss->clean_input(strip_tags($_POST['address1']));
		$address2 = $xss->clean_input(strip_tags($_POST['address2']));
		$street = $xss->clean_input(strip_tags($_POST['street']));
		$city = $xss->clean_input(strip_tags($_POST['city']));
		$post_code = $xss->clean_input(strip_tags($_POST['post_code']));
		
		unset($_SESSION['user_token']);
		$contact_by_email = 4;
		$contact_by_sms   = 4;
		$contact_by_us    = 4;
		$booking_rnd      = get_rand_id(10);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			$errors['email'] = "Invalid email address";
		if (empty($fname))
			$errors['fname'] = "Please enter first name";
		if (empty($lname))
			$errors['lname'] = "Please enter last name";
		if(!empty($comp['question']))
			if (empty($answer) && $comp['answer_required']=='yes')
				$errors['answer'] = "Please select/enter your answer ";
		if (!checkphone($telephone))
						$errors['telephone'] = "Please enter valid UK or Ireland mobile phone number starting 447 or 3538";	

				if($comp['show_address'])
		{
			if(empty($house_number) )
				$errors['houseno']='Please enter House name/number';
			if(empty($street)  )
				$errors['street']='Please enter Street';
			if(empty($city) )
				$errors['city']='Please enter City';
			if(empty($post_code) )
				$errors['post_code']='Please enter post code';
				
		}
		
		if (empty($terms_agreed))
			$errors['lname'] = "Please tick to confirm you agree to terms and conditions";
		if (empty($partner1))
			$partner1=0;

		//check for duplicate customer id
		if ($verify_captcha) {
			include(ROOT . "captcha/securimage.php");
			$img = new Securimage();
			if (!$img->check($_POST['ccode']))
				$errors['captcha'] = "Must enter valid verification code to enter the draw";
		} //$verify_captcha
		// block the email domain if there in blocked email list
		$block_eml_array = array(
			"movenextweb.com",
			"mobiledatamail.com",
			"stickique.co.uk",
			"stickique.com",
			"awesomemail.us",
			"squeezer.us",
			"sarcasticcharm.com",
			"cupcaker.us",
			"instantmoviestream.net",
			"emailbaker.us",
			"course-manager.co.uk",
			"tangerineinternet.com",
			"doiamuseyou.com",
			"navyngrey.com",
			"seriouslyforreal.com",
			"instantmoviestream.net",
			"indigoable.net",
			"misao.me",
			"tigerweb.org.uk",
			"nutmail.info",
			"darklin.info",
			"purpleweb.info",
			"2rainmail.org.uk",
			"absoluteweb.info",
			"2rainmail.org.uk",
			"wipenet.info",
			"barchor.org.uk",
			"cannotmail.org.uk",
			"crymet.org.uk",
			"drecom01.co.uk",
			"freggnet.co.uk",
			"hoodmail.co.uk",
			"kreahnet.org.uk",
			"lonynet.oeg.uk",
			"mailbreaker.co.uk",
			"moussenetmail.co.uk",
			"mywheelbox.org.uk",
			"pluntermail.org.uk",
			"prainnet.org.uk",
			"rackernet.org.uk",
			"railosnet.co.uk",
			"rottmail.co.uk",
			"runracemail.org.uk",
			"runwaynet.org.uk",
			"sherrymail.co.uk",
			"shortsmail.co.uk",
			"stonetimenet.co.uk",
			"telph1line.org.uk",
			"threemailnet.co.uk",
			"tyermail.org.uk",
			"wonandron.co.uk",
			"wormail.co.uk",
			"freemailstore.com",
			"bestmailforyou.co.uk",
			"easybusinessemail.info",
			"yourmail4you.com"
		);
		
		$eml_arr         = @explode("@", $email);
		if (@in_array($eml_arr[1], $block_eml_array) == true)
			$errors['email'] = " <h3>Sorry, this email address is not a valid email. Please correct your email address to continue.</h3>";
		//check for duplicate email
		// $q="select sum(no_of_tickets) as cust_booked_tickets from hourly_competition1 where vEmail='$email'";
		//$total_cust_booked_tickets = $rowqr->cust_booked_tickets;
		//if($total_cust_booked_tickets>0)
		//{
		//$errors['email']="Sorry you have already applied for a ticket for this offer you cannot apply for any more";
		// }
		$already_entered = check_entry($partyid);
		if (count($errors) == 0) {
			$result       = "index_success.php";
			$booking_rand = $booking_rnd;
			$date         = date('Y-m-d'); //to insert current date into table
			$dob          = "$dob_year-$dob_month-$dob_day";
			$ip           = $_SERVER['REMOTE_ADDR'];
			if(strpos($comp['answer_set'],'&&') !==false)
				$answer = implode('|',$answer);
			$comp_data    = array(
				'partyid' => $partyid,
				'custband' => $custband,
				'Day_Phone' => $db->escape($telephone),
				'Email_Address' => $email,
				'Gender' => $gender,
				'Partner_Permission' => $partner1,
				'Tickets' => 1,
				'Booking_Id' => $booking_rand,
				'IP_Address' => $ip,
				'Registration_Date' => 'now()',
				'treat_id' => $promoid,
				'treat_id' => $promoid,
				'First_Name' => $db->escape($fname),
				'Last_Name' => $db->escape($lname),
				'answer' => $db->escape($answer),
				'Address_1'=>$db->escape($house_number.' '.$street) ,
		 		'Address_2' =>$db->escape($address1), 
				'Address_3' =>$db->escape($address2), 
				'Town'=>$db->escape($city) , 
				'Postcode'=> $db->escape($post_code)
			);
			
			if(!empty($comp['question']) && empty($comp['answer_set']) || !empty($_SESSION['winhash']))
				$BAND_ENTRIES[$custband] = 1;
			$rec_added= false;
			for ($e = 0; $e < $BAND_ENTRIES[$custband]; $e++)
			{	
				$rec_added = $db->insert($table['competition_data'], $comp_data);
				$data_ids[$rec_added]=$rec_added; 
				unset( $_SESSION['winhash']);
			}
			if(!$rec_added)
			{
				$errors['email']="Oops, sorry. We're not sure what happened, but we haven't been able to complete your application. Please try again in a few minutes.";
			}
			else
			{
			$insert_user_sql = "insert into sky_{$table['users']} ( `partyid`, `custband`, `promos`, `email`, `last_date`,fname,lname,phone)  

		values ('$partyid','$custband','$promoid','$email',now(),'$fname','$lname','$comp_data[Day_Phone]')

		 ON DUPLICATE KEY UPDATE  `partyid`=VALUES(partyid),`custband`=VALUES(custband),`promos`=CONCAT(`promos`,' ','$promoid'),`email`=VALUES(email),`last_date`=VALUES(last_date),`phone`=VALUES(phone) ";
			$db->query($insert_user_sql)->execute();
			//	$_SESSION['title']=stripslashes($title);
			$_SESSION['lname']        = stripslashes($lname);
			$_SESSION['fname']        = stripslashes($fname);
			$_SESSION['email']        = stripslashes($email);
			//	$_SESSION['requested_tickets']= stripslashes($requested_tickets);
			$_SESSION['promoid']      = stripslashes($promoid);
			$_SESSION['custband']     = stripslashes($custband);
			$_SESSION['booking_rand'] = stripslashes($booking_rand);
			$_SESSION['user_answer'] = $comp_data['answer'];
			$_SESSION['rec_ids'] = $data_ids;
			$_SESSION['checksum2']    = sha1($custband . $booking_rand . $sha_salt . date('Ymd'));
			$history_action = 'requested';
			if($comp['require_upload']==1)
				$history_action = 'engaged';	
			$sql                      = sprintf("INSERT INTO sky_%s (`party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`,`original_band`,`original_action_date`)  values('%s','%s','%s','%s',now(),'%s','%s','%s','%s','%s','%s',now()) ", $table['history'], $partyid, strtolower(CATEGORY), $promoid, $db->escape($comp['promotion_title']), $comp['end_date'], '', $history_action, $BAND_ENTRIES[$custband], $booking_rand, $custband);
			$sql .= ' ON DUPLICATE KEY UPDATE party_id = values(party_id),category_id = values(category_id),promo_id = values(promo_id),promo_title = values(promo_title),action_date = now(),promo_date = values(promo_date),location= values(location),action = values(action),volume = values(volume),action_id=values(action_id)';
			unset($db);
			$db        = new DB_manager(HOST, DBU, DBPASS, DB_HISTORY);
			$db->debug = 0;
			$db->query($sql)->execute();
			$anchor='';
			if($comp['require_upload']==1) 
				$anchor='#C_UPLOAD';
			header('Location:/'.DIR.'index_success.php'.$anchor);		
			exit();
			}
		} //count($errors) == 0
	} //$caller == 'Submit' && $_POST['user_token'] == $_SESSION['user_token']
} //$time_now >= $comp['start_date'] && $time_now <= $comp['end_date']
else {
	$stime    = explode(" ", $comp['start_date']);
	$stime2   = explode('-', $stime[0]);
	$stime[0] = "$stime2[2]/$stime2[1]/$stime2[0] ";
	$etime    = explode(" ", $comp['end_date']);
	$etime2   = explode('-', $etime[0]);
	$etime[0] = "$etime2[2]/$etime2[1]/$etime2[0] ";
}
//a) if no question set, then use prizedraw-bandA.png
//b) if question set, then use competition-bandA.png
if($_SERVER['REMOTE_ADDR']=='82.11.207.224')
{
//	echo "$comp[comingsoon] > '2000-01-01' && $comp[comingsoon]<= $time_now  && $time_now < $comp[start_date]";
}
$message1=", answer the competition question";		
$overlay = 'competition';
$uploadmessage="";		
if($comp['require_upload']==1)
   $uploadmessage="Then follow the instructions to upload your file.";		
$message2='';
if($comp['comingsoon'] > '2000-01-01' && $comp['comingsoon']<= $time_now  && $time_now < $comp['start_date'] )
	$overlay = 'coming-soon';
elseif(empty($comp['question']))
{
	$overlay = 'prizedraw';
	$message1 = "";
}	
$message2="Remember, the longer you're with us the more chances you have to win.";
if(!empty($comp['question']) && empty($comp['answer_set']))
	$message2 = "";

?><?php
require('header.php');
?>
  <div class="topbaner" style="background-color:<?= $comp['header_bg_color']; ?>">
   <div class="leftimage">
   <?
    if(!empty($comp['analytics_id']))
	{
		$you_parts = explode('?v=',$comp['analytics_id']);
		$you_parts = $you_parts[1];
		$you_parts = explode('&',$you_parts);
		$youtube_id = $you_parts[0];
	?><div class="video-container"><iframe src="https://www.youtube.com/embed/<?php echo $youtube_id;?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe></div>
	<?php
	}else
	{ 
	if ($overlay!='' ){?> 
			<div class="<?php echo $overlay; ?>image"></div>
	<?php } ?>
		    <img src="<?php echo CDN_URL;?><?php echo DIR;?>content/<?= $comp['image']; ?>" alt="">
	<?php } ?>
</div>
    <div class="rightparttext">
      <div class="textsection">
        <div class="bigtext">
          <?= nl2br($comp['promotion_title']); ?>
        </div>
        <div class="subheadertext">
          <?= nl2br($comp['sub_title']); ?>
        </div>
        <?php
if ($time_now < $comp['start_date'] || $time_now > $comp['end_date']) {
?>
      <div class="contentpart"> 
          This  draw is coming soon.
            <br>
          <br>
        </div>
          <!--<div class="contentpart"> <br>
          <br>
          <br>
          This promotion opens on
          <?= date('d F, Y',strtotime($comp['start_date'])); ?>
          at
          <?= $stime[1]; ?>
          and closes on
          <?= date('d F, Y',strtotime($comp['end_date'])); ?>
          at
          <?= $etime[1]; ?>
          <br>
          <br>
          <br>
        </div>-->
        <?
} //$time_now < $comp['start_date'] || $time_now > $comp['end_date']
else {
	$term_tags = array('[CLOSING_DATE]','[START_DATE]','[DRAW DATE]','[NUMBER_WINNERS]','[NOTIFICATION_DATE]');
	$replace_tags = array(date('H:i \o\n j F, Y',strtotime($comp['end_date'])),date('H:i \o\n j F, Y',strtotime($comp['start_date'])),date('H:i \o\n j F, Y',strtotime($comp['drawtime'])),$comp['winners'],date('d F, Y',strtotime($comp['drawtime'])+(7*60*60*24)));		
	$comp['terms']=str_replace($term_tags,$replace_tags, $comp['terms']);
			
	if (count($errors) > 0) {
		echo '<div class="error" id="err_div"><ul>';
		foreach ($errors as $error)
			echo "<li>$error</li>";
		echo '</ul></div>

									';
	if($already_entered)
	{
		  if($comp['require_upload']==1 && empty($upload_status)) {
		  
		  	$filename= $comp['booking_rand'].'-'.$custband.'-'.round(microtime(true));
			$foldername= $comp['upload_dir'].'/'.preg_replace("/[^A-Za-z0-9- ]/",'',substr($comp['user_answer'],0,50));
				 
				?>
		  	<div class="hidden success">Your file is uploaded successfully </div>
		  	<div class="bigtext upload_text">  Upload your file</div>

				<div class="card-body upload_text" id="upload-widget"><a name="C_UPLOAD" id="C_UPLOAD"></a>
					<div id="widget">
					</div>
				</div>
                        

                <script>
                
               var myWidget = cloudinary.createUploadWidget({
                      cloudName: 'dnd68seoj', 
                      uploadPreset: 'wo1uwnop',
                      inlineContainer: "#widget",
                      publicId: '<?php echo $filename; ?>',
                      folder: '<?php echo $foldername; ?>',
                      
                         sources: [
                            "local"
                        ],
                        googleApiKey: "<image_search_google_api_key>",
                        showAdvancedOptions: false,
                     	show_powered_by: false,
                        cropping: false,
                        multiple: false,
                        defaultSource: "local",
                        
                    },
                      
                      (error, result) => { 
                        if (!error && result && result.event === "success") { 
							register_upload();
                        }
                      }
                    )
                 myWidget.open();
                
                </script>  
		  <?php }
		  
	}
	} //count($errors) > 0
	if ($already_entered) {
		echo '

									 <div class="termcondition ">
									<a href="javascript:void(0);" id="terms_link" class="textarrow-up" onClick="toggletc();">Terms and conditions</a>
									' .  $comp['terms']. '</div>
									 <form action="all_comps.php" method="post" name="frm_back">
									 <div class="buttondiv"><input type="submit" class="btn_big"  name="btn_back" value="All exclusive prizes" /></div>
									</form>';
	} //$already_entered
	else {
?>
        <div class="bodytext">Simply provide your name, phone number and email address below<?php echo $message1;?>, accept  the terms and conditions and select &lsquo;Enter&rsquo;. <?php echo $uploadmessage;?><?php echo $message2;?></div>
        <?php
	}
?>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <?php
	if (($hash == $checksum || $checksum == $hash_yesterday) && !$already_entered) {
?>
  <form action="index.php" method="post" name="frm1" id="frm1">
    <div class="contentpart">
      <?php
		if (0) {
?>
      <div class="leftform">
        <p><strong>Please enter your details here and then click submit</strong></p>
        <div class="formrow">
          <label class="fomrlabel">Title <span class="red">*</span></label>
          <div class="filedbox">
            <select name=title>
              <option value='' <?php
			if (empty($caller) || $title == '')
				echo 'selected';
?>>---</option>
              <option value='Mr'<?php
			if ($title == 'Mr')
				echo 'selected';
?>>Mr</option>
              <option value='Mrs' <?php
			if ($title == 'Mrs')
				echo 'selected';
?>>Mrs</option>
              <option value='Miss' <?php
			if ($title == 'Miss')
				echo 'selected';
?>>Miss</option>
              <option value='Ms' <?php
			if ($title == 'Ms')
				echo 'selected';
?>>Ms</option>
              <option value='Other' <?php
			if ($title == 'Other')
				echo 'selected';
?>>Other</option>
            </select>
          </div>
        </div>
        <?php
		} //0
		if (empty($email)) {
			$user  = $db->from($table['users'])->where('partyid', $partyid)->fetch_first();
			$email = $xss->clean_input($user['email']);
			$fname = $xss->clean_input($user['fname']);
			$lname = $xss->clean_input($user['lname']);
			$telephone = $xss->clean_input($user['phone']);
		} //empty($email)
?>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>First name</label>
          <input type="text" value="<?= stripslashes(htmlspecialchars($fname)); ?>" name="fname" class="btn_big_grey">
        </div>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Last name</label>
          <input type="text" value="<?= stripslashes(htmlspecialchars($lname)); ?>" name="lname" class="btn_big_grey">
        </div>
		  	<? if($comp['show_address']){ ?> 
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>House name/number</label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($house_number));?>" name="house_number" class="inputbox">
          </div>
        </div>
		<div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Street</label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($street));?>" name="street" class="inputbox">
          </div>
        </div>
        
		<div class="formrow">
          <label class="fomrlabel">Address 2 </label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($address1));?>" name="address1" class="inputbox">
          </div>
        </div>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>City</label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($city));?>" name="city" class="inputbox">
          </div>
        </div>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Post Code</label>
          <div class="filedbox">
            <input type="text" value="<?=stripslashes(htmlspecialchars($post_code));?>" name="post_code" class="inputbox">
          </div>
		</div>
		<?php } ?>
        <?
		if (0) {
			/*
			
			<div class="formrow">
			
			<label class="fomrlabel">Date of Birth <font color=red> *</font></label>
			
			<div class="filedbox">
			
			<?
			
			//dob day
			
			print "<select class='small-select' name=dob_day>";
			
			if(empty($caller) || $dob_day =='') print "<option value='' selected>Day</option>";
			
			else print "<option value=''>Day</option>";
			
			for($i=1; $i<=31; $i++)
			
			{
			
			if($dob_day == $i) printf ("<option value=%02d selected>%02d</option>",$i,$i);
			
			else printf ("<option value=%02d>%02d</option>",$i,$i);
			
			}
			
			print "</select>";
			
			
			
			//dob month
			
			$months = array('1' => "JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
			
			print "<select class='small-select' name=dob_month>";
			
			if(empty($caller) || $dob_month =='') print "<option value='' selected>Month</option>";
			
			else print "<option value=''>Month</option>";
			
			foreach($months as $key=>$month)
			
			{
			
			if($key == $dob_month) printf ("<option value=%02d selected>$month</option>",$key);
			
			else printf ("<option value=%02d>$month</option>",$key);
			
			}
			
			print "</select>";
			
			
			
			//dob year
			
			print "<select class='small-select' name=dob_year>";
			
			if(empty($caller) || $dob_year =='')
			
			print "<option value=''  selected>Year</option>";
			
			else  print "<option value=''>Year</option>";
			
			for($i=1900; $i<=2004; $i++)
			
			{
			
			if($dob_year == $i) print "<option value=$i selected>$i</option>";
			
			else print "<option value=$i>$i</option>";
			
			}
			
			print "</select>";
			
			?>
			
			</div>
			
			</div>
			
			*/
?>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Email</label>
          <div class="filedbox">
            <input type="text" value="<?= stripslashes(htmlspecialchars($email)); ?>" name="email" class="inputbox">
          </div>
        </div>
        <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Confirm email</label>
          <div class="filedbox">
            <input type="text" value="<?= stripslashes(htmlspecialchars($confirm_email)); ?>" name="confirm_email" class="inputbox">
          </div>
        </div>
        <?php
			if ($show_address) {
?>
        <div class="formrow">
          <label class="fomrlabel">House name/number <span class="red">*</span></label>
          <div class="filedbox">
            <input type="text" value="<?= stripslashes(htmlspecialchars($house_number)); ?>" name="house_number" class="inputbox">
          </div>
        </div>
        <div class="formrow">
          <label class="fomrlabel">Street <span class="red">*</span></label>
          <div class="filedbox">
            <input type="text" value="<?= stripslashes(htmlspecialchars($street)); ?>" name="street" class="inputbox">
          </div>
        </div>
        <div class="formrow">
          <label class="fomrlabel">Address 2 <span class="red">*</span></label>
          <div class="filedbox">
            <input type="text" value="<?= stripslashes(htmlspecialchars($address1)); ?>" name="address1" class="inputbox">
          </div>
        </div>
        <div class="formrow">
          <label class="fomrlabel">City <span class="red">*</span></label>
          <div class="filedbox">
            <input type="text" value="<?= stripslashes(htmlspecialchars($city)); ?>" name="city" class="inputbox">
          </div>
        </div>
        <div class="formrow">
          <label class="fomrlabel">Post Code <span class="red">*</span></label>
          <div class="filedbox">
            <input type="text" value="<?= stripslashes(htmlspecialchars($post_code)); ?>" name="post_code" class="inputbox">
          </div>
        </div>
        
        <?php
			} //$show_address
			
			if (0) {
				/*
				
				<div class="formrow">
				
				<label class="fomrlabel">Your Story <span class="red">*</span></label>
				
				<div class="filedbox">
				
				<textarea name="story" rows="5" class="story"><?=stripslashes(htmlspecialchars($story));?>
				
				</textarea>
				
				Maximum 500 words (<span id="wcount">500</span> words left)
				
				</div>
				
				</div>
				
				*/
?>
        <div>&nbsp;</div>
        <div class="condition">
          <p><strong>Condition of Entry</strong></p>
          <p>By entering the promotion, you agree to receive information on offers and promotions from Times Newspapers Limited, publishers of The Times and The Sunday Times, but you'll be able to opt out at any time. Your information will be used in accordance with our <a target="_blank" href="http://www.newsprivacy.co.uk/single/">Privacy Policy.</a></p>
				</div>	
					<?php
			}
	?>
      </div>
      <div class="righttextpart"></div>
      <?php
		} //0
?>
      	<div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Mobile phone number</label><br>
Inc. country code eg. <strong>353</strong>8012345678 or <strong>44</strong>7012345678          </div>
          <div class="filedbox">
            <input type="text" value="<?= stripslashes(htmlspecialchars($telephone)); ?>" name="telephone" class="inputbox">
        </div>      <div class="formrow">
          <label class="fomrlabel"><span class="grey70">*</span>Email address</label><br>
        (If you win, we’ll contact you at this address)</div>
      <div class="formrow">
        <input type="email" id="email" name="email" class="btn_big_grey" value="<?php
		echo stripslashes(htmlspecialchars($email));
?>" />
      </div>
      <?php
      $comp['question'] = trim($comp['question']);
			if(!empty($comp['question']))
			{
			?>
            <div class="formrow">
          <label class="fomrlabel"><?php echo $comp['question'];?><span class="grey70">*</span></label>
          <div class="filedbox">
		  <?php $a_count=0;
				if(!empty($comp['answer_set']))
				{
					$chkbox='';
					if(strpos($comp['answer_set'],'&&') !==false || strstr($comp['answer_set'],'|')===false)
					{
						$chkbox = '[]';
						$comp['answer_set'] = explode('&&',$comp['answer_set']);
					}
					elseif(strpos($comp['answer_set'],'|')!==false)
						$comp['answer_set'] = explode('|',$comp['answer_set']);		
					
					foreach($comp['answer_set'] as $ans)
					{
						$ans=trim($ans);
						if($a_count)
							echo ' <br> ';
						$a_count++;
						?> <input type="<?php echo (empty($chkbox)?'radio':'checkbox');?>" <?php if($ans==$answer || in_array($ans,$answer)) echo 'checked';?> name="answer<?php echo $chkbox;?>" id="answer<?php echo $a_count;?>" value="<?php echo $ans;?>" />
					<?php 
						echo $ans;
					}
				}
				else
				{
			?>
            <input type="text" value="<?= stripslashes(htmlspecialchars($answer)); ?>" name="answer" class="inputbox">
          	<?php } ?>
          </div>
        </div>
			<?php 
			
			}
			?>
      <div class="spacerdiv">&nbsp;</div>
      <div>
        <div class="termcondition ">
          <div class="withcheckbox">
            <?php {
?>
            <label class="control control--checkbox tearmcheckbox">
            <input type="checkbox" name="terms_agreed" value="agree"/>
            <div class="control__indicator"></div>
            </label>
            <?php
		}
?>
            <a href="javascript:void(0);" id="terms_link" class="textarrow-up" onClick="toggletc();">Click here to accept our terms and conditions</a></div>
          <?php
		echo $comp['terms'];
?></div>
      </div>
      <div class="spacerdiv">&nbsp;</div>
			<div class="clear"></div>
			<?php
			$comp['optin'] = trim($comp['optin']);
				if (!empty($comp['optin'])) {
?>
				<div class="termcondition ">
					<div class="withcheckbox">
						<div class="condition">

						<label class="control control--checkbox tearmcheckbox">
								<input type="checkbox" <?php if ($_POST['partner1'] == 1)	echo ' checked';?>  name="partner1" id="partner1" value="1"/>
								<div class="control__indicator"></div>
							</label>
							<?= $comp['optin']; ?>
						</div>
					</div>
				</div>
				<div class="spacerdiv">&nbsp;</div>
				<div class="clear"></div>
				
				<?php
				} //$optin

		/// COA full div
		/*
		
		<div>
		
		<p><strong>Condition of Entry</strong></p>
		
		<p>By entering the promotion, you agree to receive information on offers and promotions from Times Newspapers Limited, publishers of The Times and The Sunday Times, but you'll be able to opt out at any time. Your information will be used in accordance with our <a target="_blank" href="http://www.newsprivacy.co.uk/single/">Privacy Policy.</a></p>
		
		<p>We'll pass your details to Carluccio`s for them to send you promotions, offers and information that may be of interest to you unless you
		
		<label>tick here
		
		<input type="checkbox" value="4" name="partner1" id="partner1"  <? if($_POST['partner1']==4) echo ' checked'; ?> >
		
		</label>
		
		</p>
		
		<p>Your information will be used in accordance with the Carluccio`s&nbsp;<a target="_blank" href="http://www.carluccios.com/privacy">Privacy Policy</a>.</p>
		
		<div class="termcondition">
		
		<?=$terms;?>
		
		
		
		</div>
		
		</div>
		
		
		
		*/
?>
      <?
		if ($verify_captcha) {
			$ttf = rand(1, 65);
?>
      <div class="termcondition">
        <label>Image verification code</label>
        <div style="text-align: left; display: inline-block;"><img align="absmiddle" id="cimgs" src="/captcha/securimage_show.php?sid=<?= $ttf; ?>"> <a onclick="document.getElementById('cimgs').src = '/captcha/securimage_show.php?sid=' + +Math.floor(65*Math.random()); return false" href="#">Reload Image</a></div>
        <input type="text" class="button" id="ccode" name="ccode">
      </div>
      <?
		} //$verify_captcha
?>
      <div class="buttondiv">
        <?
		$user_token             = get_rand_id(10);
		$_SESSION['user_token'] = $user_token;
?>
        <input type="hidden" name="user_token" id="user_token" value="<?= $user_token; ?>">
        <input type="hidden" name="promoid" id="promoid" value="<?= $promoid; ?>">
        <input type="submit" value="Enter" name="caller1" class="button">
        <input type="hidden" value="Submit" name="caller" class="Submit">
      </div>
    </div>
  </form>
  <?php
	} //($hash == $checksum || $checksum == $hash_yesterday) && !$already_entered
?>
</div>
<?php
}
?>
</body>
</html>
<script language="javascript">
function scrollToAnchor(aid){
    var aTag = $("a[name='"+ aid +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}
<?php 
//	  if($comp['require_upload']==1) 
//		echo "scrollToAnchor('C_UPLOAD');";?>
function toggletc()

{

	$(".termcondition p").fadeToggle(1000);

	$("#terms_link").toggleClass("textarrow-up terms-textarrow");

}

<?
if(0)
if (count($errors) > 0 && $caller == 'Submit') {
?>
$( "#err_div" ).fadeOut(20000);
<?
} //count($errors) > 0 && $caller == 'Submit'
?>
// Sky tracking code start 
$(document).ready(function() {
var data = {
    page: {
        name: "comp_detail",
        breadcrumb: [ "skyapp", "stretch", "" ]
    }
};

	skyTags.queue.push(['set', data]);
});


</script>
<?php require('upload_handler.php');?>
