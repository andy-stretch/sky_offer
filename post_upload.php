<?php
$output = array();
require('local_config.php');
require(ROOT . 'config/sky_connect.php');
$track_pagename = 'compdetail';
$already_entered = false;
$errors          = array();
require(ROOT . 'common/xss_safe.php');
require(ROOT . 'common/db/DB_manager.php');
$db = new DB_manager(HOST, DBU, DBPASS, DB);
$db->set_table_prefix('sky_');
$db->debug = 1;
$comp = '';

$xss       = new xssSafe();
//$promoid = $_SESSION['promoid'];
//$promoid   = $xss->clean_input($_REQUEST['promoid']);
$custband = $_SESSION['custband'];
$partyid = $_SESSION['partyid'];
$checksum = $_SESSION['checksum'];

$image_file_name   = $xss->clean_input($_POST['file_name']);
$promoid  = $xss->clean_input($_POST['promoid']);
$user_token = $xss->clean_input($_POST['user_token']);

$hash           = get_sha1($partyid, $custband, '');
$time_yesterday = time() - (24 * 60 * 60);
$date_yesterday = date('Ymd', $time_yesterday);
$hash_yesterday = get_sha1($partyid, $custband, '', $date_yesterday);
if (($hash != $checksum && $checksum != $hash_yesterday) || empty($promoid)) {
	//checksum failed
	$output['debug']='checksum failed';
	$output['result']='redirect';
} //($hash != $checksum && $checksum != $hash_yesterday) || empty($promoid)
$comp     = $db->from($table['competition'])->where('status', 'live')->where('treat_id', $promoid)->fetch_first();
$time_now = date('Y-m-d H:i:s');

if ($time_now < $comp['start_date'] || $time_now > $comp['end_date']  || $user_token != $_SESSION['user_token']) 
{
	$output['debug']='not live or end';
	// promo not live or closed or user token not verified
	$output['result']='redirect';
}
else
{
	$booking_rand=$_SESSION['booking_rand'];
	$user_info = $db->from($table['competition_data'])->where(array(
		'partyid' => $partyid,
		'treat_id' => $promoid,
		'Booking_Id'=>$booking_rand
	))->order_by('id', 'desc')->fetch_first();
	
	if ($db->affected_rows > 0) 
	{
		$data = array('email_read'=>$image_file_name);
		$db->where('Booking_Id',$booking_rand);
		$db->where('partyid',$partyid);
		$db->where('treat_id',$promoid);
		//$db->limit(1);
		$db->update( $table['competition_data'],$data);
		
		$sql                      = sprintf("INSERT INTO sky_%s (`party_id`, `category_id`, `promo_id`, `promo_title`, `action_date`, `promo_date`, `location`, `action`, `volume`, `action_id`,`original_band`,`original_action_date`)  values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s') ", $table['history'], $partyid, strtolower(CATEGORY), $promoid, $db->escape($comp['promotion_title']),$time_now,$comp['end_date'], '', 'requested', $BAND_ENTRIES[$custband], $booking_rand, $custband,$time_now);
		$sql .= ' ON DUPLICATE KEY UPDATE party_id = values(party_id),category_id = values(category_id),promo_id = values(promo_id),promo_title = values(promo_title),action_date = now(),promo_date = values(promo_date),location= values(location),action = values(action),volume = values(volume),action_id=values(action_id)';
		$db        = new DB_manager(HOST, DBU, DBPASS, DB_HISTORY);
		$db->debug = 0;
		$db->query($sql)->execute();
		$output['result']='success';
	
		$comp['email_content']=htmlspecialchars_decode($comp['email_content']);
		$term_tags = array('[CLOSING_DATE]','[START_DATE]','[DRAW DATE]','[NUMBER_WINNERS]','[NOTIFICATION_DATE]');
	$replace_tags = array(date('H:i \o\n j F, Y',strtotime($comp['end_date'])),date('H:i \o\n j F, Y',strtotime($comp['start_date'])),date('H:i \o\n j F, Y',strtotime($comp['drawtime'])),$comp['winners'],date('d F, Y',strtotime($comp['drawtime'])+(7*60*60*24)));		
	$comp['terms']=str_replace($term_tags,$replace_tags, $comp['terms']);
		$band_color = $BAND_COLORS[$custband];

		$email_constants = array('[YOUTUBE]','[BOOKING_ID]','[FILM_NAME]','[IMAGE]','[USER_EMAIL]','[UNIQUE_DATE]','[UNIQUE_TIME]','[BRAND]','[TERMS]','[FNAME]','[LNAME],[ACTION_ID]');

		$email_constants_value = array($comp['analytics_id'],$booking_rand,
		$comp['promotion_title'],
		SITE_URL.$comp['image'],
		$email,
		date('d-m-Y',strtotime($comp['start_date'])),
		date('H:ia',strtotime($comp['start_date'])),
		$comp['brand'],
		$comp['terms'],
		$user_info['First_Name'],
		$user_info['Last_Name'],
		$booking_rand
		);

		$band_entries_text = $BAND_ENTRIES_DISPLAY[$custband];
		if(!empty($comp['question']) && empty($comp['answer_set']))
		   $band_entries_text = $BAND_ENTRIES_DISPLAY_COMP[$custband];
		$comp_type="prize draw";
		   if(!empty($comp['question']))
  		$comp_type = "competition";

		$confirm_page_text = "Thank you for entering our ".$comp_type.": &quot;".$comp['promotion_title']."&quot;.".$band_entries_text."<br><br>We'll let you know by email if you're a winner.";
		
		$email_content=  str_replace($email_constants,$email_constants_value,$comp['email_content']);
		
		require('email/header.php');
		require('email/footer.php');
		$email_signoff="
		<br><br>
		Your Sky VIP Team";
		$from="noreply-admin@skyticketit.com";
		$etype = 'prize draw';
		if(!empty($comp['question']) )
			$etype = 'competition';
		$subject = "You've entered Sky VIP's $etype";

		$email_header = str_replace('[ETYPE]',$etype,$email_header);

		// Sending Email using API
		$email_content =  $email_header.$confirm_page_text.$email_content.$email_signoff.$email_footer;
		$from_name = 'Sky VIP';
		require(ROOT . 'common/Send_Email_API.php');
		$post = array(
		'to' => $user_info['Email_Address'],  // if we send via CSV file we do not needs this line
		'from' => $from,
		'fromName' => $from_name,
		'subject' => $subject,
		'bodyHtml' => $email_content,
		'bodyText' => strip_tags($email_content),
		'channel' =>  str_replace('/','_', rtrim (DIR, '/')),      // sky/offer/ will produce sky_offer
		'isTransactional' => true
		);
		// if we send via CSV file we do not needs to send to in above field
		$result = send_email_api($post,$csv_file_name=NULL); 
	}
	else // we do not find user's record, we will send user back to promo page
	{
		$output['result']='redirect';
		$output['debug']='User entry not found'; 
	}
}
echo json_encode($output);
